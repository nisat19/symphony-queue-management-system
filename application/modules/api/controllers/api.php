<?php

set_time_limit(300);
date_default_timezone_set('Asia/Dhaka');

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class api extends Front_Controller {

//--------------------------------------------------------------------
    private $sym_rest;
    private $token;
    private $tokenid;
    private $email;

    public function __construct() {
        parent::__construct();

        $this->sym_rest = new REST_Controller();
        $this->token = $this->sym_rest->get('token');
        $this->load->helper('date');

        $this->load->model('users/User_model', 'user_model');
        $this->load->model('token/token_model');
        $this->load->model('ticket/ticket_model');
        $this->load->model('counters/counters_model');
        $this->load->model('branches/branches_model');
        $this->load->model('ticket_meta/ticket_meta_model');
        $this->load->model('counter_join/counter_join_model');

        if ($this->token) {
            $tokenrow = $this->db
                    ->select('id,user_id,expire_on')
                    ->from('token')
                    ->where('token', $this->token)
                    ->where('status <> 0')
                    ->get()
                    ->result();
            if (isset($tokenrow) && !empty($tokenrow)) {
                $token = $tokenrow[0];
                if ($token->expire_on > date('Y-m-d H:i:s')) {
                    if ($token->id) {
                        $this->tokenid = $token->id;
                    }
                    $this->db->_reset_select();
                    if ($token->user_id) {
                        $userrow = $this->db
                                ->select('email')
                                ->where('id', $token->user_id)
                                ->get('users')
                                ->result();
                        $user = $userrow[0];
                        $this->email = $user->email;
                    }
                    $this->db->_reset_select();
                } else {
                    $this->token_model->update($token->id, array('status' => 0));
                }
            }
        }
    }

//    public function __construct() {
//        parent::__construct();
//        $this->sym_rest = new REST_Controller();
//    }
//--------------------------------------------------------------------

    public function index() {

//        Template::render();
        $this->sym_rest->response(array('Msg' => 'Request is missing id parameter', 'Success' => TRUE), 200);
    }

    private function user() {
        try {
            if ($this->tokenid) {
                $token_data = $this->token_model->find($this->tokenid);
                $user = $this->user_model->find($token_data->user_id);
                if ($user) {
                    logit('Requested by: ' . $this->email . ' on ' . date('d-m-Y h:i:s'), 'debug');
                    return $user;
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } catch (ErrorException $ex) {
            logit('Caused by: ' . $ex->getMessage() . ' on ' . date('d-m-Y h:i:s'), 'debug');
            return FALSE;
        }
    }

    public function login() {
        try {
//If not logged in do login
            if (!$this->sym_rest->post('id')) {
                $this->sym_rest->response(array('Msg' => 'Request is missing id parameter', 'Success' => TRUE), 403);
            }
            if (!$this->sym_rest->post('pass')) {
                $this->sym_rest->response(array('Msg' => 'Request is missing password parameter', 'Success' => TRUE), 403);
            }
            $user = $this->sym_rest->post('id', TRUE);
            $pass = $this->sym_rest->post('pass', TRUE);

            if ($this->user_login($user, $pass)) {
//Assign Session ID to $token
//                $this->load->model('token/token_model');
                $tr = $this->token_model->find($this->tokenid);
                $this->token = $tr->token;
//Return token
                $this->sym_rest->response(array('auth_key' => $this->token, 'user_id' => $this->user()->id, 'logged_in' => TRUE, 'Success' => TRUE), 200);
            } else {
                $this->sym_rest->response(array('auth_key' => '', 'logged_in' => FALSE, 'Msg' => 'Username or Password does not match', 'Success' => TRUE), 403);
            }
        } catch (ErrorException $ex) {
            $this->sym_rest->response(array('Msg' => $ex->getMessage(), 'Success' => TRUE, 404));
        }
    }

//User Login
//Core Login
    private function user_login($login, $password) {
        if (empty($login) || empty($password)) {
            return FALSE;
        }

        $this->load->model('users/User_model', 'user_model');

// Grab the user from the db
        $selects = 'id, email, username, users.role_id, password_hash, users.role_id, users.deleted, users.active, banned, ban_message, password_iterations, force_password_reset';

        if ($this->settings_lib->item('auth.do_login_redirect')) {
            $selects .= ', login_destination';
        }

        $user = $this->user_model->select($selects)->find_by($this->settings_lib->item('auth.login_type'), $login);


// check to see if a value of FALSE came back, meaning that the username or email or password doesn't exist.
        if ($user == FALSE) {
            return FALSE;
        }

// check if the account has been activated.
        $activation_type = $this->settings_lib->item('auth.user_activation_method');
        if ($user->active == 0 && $activation_type > 0) { // in case we go to a unix timestamp later, this will still work.
            if ($activation_type == 1) {
                return FALSE;
            } elseif ($activation_type == 2) {
                return FALSE;
            }

            return FALSE;
        }

// check if the account has been soft deleted.
        if ($user->deleted >= 1) { // in case we go to a unix timestamp later, this will still work.
            return FALSE;
        }

// Load the password hash library
        if (!class_exists('PasswordHash')) {
            require(dirname(__FILE__) . '/../libraries/PasswordHash.php');
        }
        $hasher = new PasswordHash($user->password_iterations, false);

// Try password
        if ($hasher->CheckPassword($password, $user->password_hash)) {

// We've successfully validated the login, so setup the token
//            $this->load->model('token/token_model');
            $this->tokenid = $this->token_model->insert(
                    array(
                        'token' => md5($user->username . now()),
                        'user_id' => $user->id,
                        'expire_on' => date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s") . '+ 2 hour'))
                    )
            );

// Save the login info
            $data = array(
                'last_login' => date('Y-m-d H:i:s', time()),
                'last_ip' => $_SERVER['REMOTE_ADDR'],
            );
            $this->user_model->update($user->id, $data);

// Clear the cached result of user() (and hence is_logged_in(), user_id() etc).
// Doesn't fix `$this->current_user` in controller (for this page load)...
            unset($this->user);

            $trigger_data = array('user_id' => $user->id, 'role_id' => $user->role_id);
            Events::trigger('after_login', $trigger_data);

            return TRUE;
        }

// Bad password
        else {
            return FALSE;
        }

        return FALSE;
    }

//Core Login
    public function session() {
        if (!$this->sym_rest->get('token')) {
            $this->sym_rest->response(array('Msg' => 'Request is missing a token', 'Success' => TRUE), 403);
        }
        $q = $this->db->query(
                        "SELECT token.token
                FROM token
                WHERE token.token = '$this->token'
                AND token.expire_on > CURDATE()
                AND token.`status` <> 0"
                )->result();
        if ($q[0]->token) {
//var_dump($this->session->userdata);
//var_dump($this->auth->is_logged_in());
//var_dump($this->current_user);
//var_dump($this->auth->user());
//var_dump($this->user_model->is_company_admin());
//return FALSE;
            $this->sym_rest->response(array('Msg' => 'Wrong place, Wrong time :)', 'Success' => TRUE), 403);
        } else {
            $this->sym_rest->response(array('Msg' => 'Invalid Token', 'Success' => TRUE), 403);
        }

//--------------------------------------------------------------------
    }

    public function getTicket($branch_id) {
        $coun = 0;
        try {
//            if (!$this->sym_rest->get('token')) {
//                $this->sym_rest->response(array('Msg' => 'Request is missing a token', 'Success' => TRUE), 403);
//            }
            $category_id = $this->sym_rest->post('category_id');
            $ins_id = $this->ticket_model->insert(
                    array(
                        'lso' => $this->sym_rest->post('lso'),
                        'customer_name' => $this->sym_rest->post('cname'),
                        'customer_address' => $this->sym_rest->post('caddress'),
//                        'model_id' => $this->sym_rest->post('model_id'),
                        'category_id' => $category_id,
                        'branch_id' => $branch_id
                    )
            );

            if ($ins_id) {
                $coun = $this->getCounterId($branch_id, $category_id);
                $data = array('counter_id' => $coun);
                $upd_ticket = $this->ticket_model->update($ins_id, $data);
                $counterData = $this->counters_model->find_by('id', $coun);
                if ($upd_ticket) {
                    $this->sym_rest->response(array('success' => TRUE, 'ticket_id' => $ins_id, 'counter_id' => $counterData->title), 200);
                } else {
                    $this->sym_rest->response(array('success' => FALSE), 403);
                }
            } else {
                $this->sym_rest->response(array('success' => FALSE), 403);
            }
        } catch (ErrorException $ex) {
            $this->sym_rest->response(array('Msg' => $ex->getMessage(), 'Success' => FALSE, 404));
        }
    }

    public function getCounterId($branchId, $categoryId) {
        $branch_target = 0;
        $value_t = array();
        $value_s = array();
        $value_o = array();
        $value_target = array();
        $value_average = array();
        $value_cload = array();
        // Empirical Value
        $w1 = .5; // Weight value for remaining target
        $w2 = .1; // Weight value for remaining serving
        $w3 = .4; // Weight value for remaining pending
        $value_p = array();
//                $serving_counter = count($this->counters_model->where('branch_id', 1)->find_by('status', 'serving'));

        $branch_target = $this->branches_model->find_by('id', $branchId);

        /* Get All counter by Condition */


        $counterList = $this->db
                        ->query("SELECT
                                        bf_counter.id,
                                        bf_counter.title,
                                        bf_counter.description,
                                        bf_counter.branch_id,
                                        bf_counter.`status`,
                                        bf_counter.created_on,
                                        bf_counter.modified_on
                                FROM
                                        bf_counter
                                LEFT JOIN bf_counter_join ON bf_counter.id = bf_counter_join.counter_id
                                WHERE
                                        bf_counter.branch_id = '" . $branchId . "'
                                AND bf_counter_join.category_id = '" . $categoryId . "'
                                AND bf_counter.`status` = 'serving'")->result();

        if (!empty($counterList)) {
            $no_of_counter = $counterList;
        } else {
            $condition = array(
                'status' => 'serving',
                'branch_id' => $branchId
            );
            $no_of_counter = $this->counters_model->find_all_by($condition);
        }


        if (is_array($no_of_counter)) {
            foreach ($no_of_counter as $serv) {
                if (isset($serv->id) && is_numeric($serv->id) && $serv->id > 0) {
                    $already_served = $this->db
                                    ->query("SELECT {$branch_target->target} - count(id) as num FROM bf_ticket_meta WHERE DATE(created_on) = DATE(NOW()) AND counter_id= '{$serv->id}' AND status = 'resolved'")->row();
                    if ($already_served) {
                        $value_t[] = (int) $already_served->num;
                    }

                    $avg_value = $this->db
                                    ->query("SELECT AVG(TIME_TO_SEC(TIMEDIFF(finished_on, progressed_on))) as avg_time FROM bf_ticket_meta WHERE DATE(created_on) = DATE(NOW()) AND counter_id = '{$serv->id}' AND `status` = 'resolved'")->row();

                    if ($avg_value) {
                        $value_s[] = (int) $avg_value->avg_time;
                    }

                    $current_load = $this->db
                                    ->query("SELECT COUNT(id) as cload from bf_ticket WHERE DATE(created_on) = DATE(NOW()) AND counter_id = '{$serv->id}' AND `status` = 'pending'")->row();
                    if ($current_load) {
                        $value_o[] = (int) $current_load->cload;
                    }
                }
            }
            if ($value_t) {
                foreach ($value_t as $t) {
                    if ($t > 0)
                        $value_target[] = ($t / max($value_t)) * $w1;
                    else
                        $value_target[] = 0;
                }
            }
            if ($value_s) {
                foreach ($value_s as $s) {
                    if ($s > 0)
                        $value_average[] = ($s / max($value_s)) * $w2;
                    else
                        $value_average[] = 0;
                }
            }
            if ($value_o) {
                foreach ($value_o as $o) {
                    if ($o > 0)
                        $value_cload[] = ($o / max($value_o)) * $w3;
                    else
                        $value_cload[] = 0;
                }
            }
        }
        $i = 0;
        foreach ($no_of_counter as $value) {
            $value_p[$value->id] = ($value_target[$i] + $value_average[$i]) - $value_cload[$i];
            $i++;
        }
        $coun = max($value_p);
        $res = array_search($coun, $value_p);
        return $res;
    }

}
