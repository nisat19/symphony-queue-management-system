<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(
	'description'	=> 'Helps to Manage API requests',
	'name'		=> 'API',
	'version'		=> '0.0.1',
	'author'		=> 'admin'
);