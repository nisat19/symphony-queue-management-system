<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['api_manage']			= 'Manage API';
$lang['api_edit']				= 'Edit';
$lang['api_true']				= 'True';
$lang['api_false']				= 'False';
$lang['api_create']			= 'Create';
$lang['api_list']				= 'List';
$lang['api_new']				= 'New';
$lang['api_edit_text']			= 'Edit this to suit your needs';
$lang['api_no_records']		= 'There aren\'t any api in the system.';
$lang['api_create_new']		= 'Create a new API.';
$lang['api_create_success']	= 'API successfully created.';
$lang['api_create_failure']	= 'There was a problem creating the api: ';
$lang['api_create_new_button']	= 'Create New API';
$lang['api_invalid_id']		= 'Invalid API ID.';
$lang['api_edit_success']		= 'API successfully saved.';
$lang['api_edit_failure']		= 'There was a problem saving the api: ';
$lang['api_delete_success']	= 'record(s) successfully deleted.';
$lang['api_delete_failure']	= 'We could not delete the record: ';
$lang['api_delete_error']		= 'You have not selected any records to delete.';
$lang['api_actions']			= 'Actions';
$lang['api_cancel']			= 'Cancel';
$lang['api_delete_record']		= 'Delete this API';
$lang['api_delete_confirm']	= 'Are you sure you want to delete this api?';
$lang['api_edit_heading']		= 'Edit API';

// Create/Edit Buttons
$lang['api_action_edit']		= 'Save API';
$lang['api_action_create']		= 'Create API';

// Activities
$lang['api_act_create_record']	= 'Created record with ID';
$lang['api_act_edit_record']	= 'Updated record with ID';
$lang['api_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['api_column_created']	= 'Created';
$lang['api_column_deleted']	= 'Deleted';
$lang['api_column_modified']	= 'Modified';
