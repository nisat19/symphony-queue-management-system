<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * content controller
 */
class content extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Token.Content.View');
		$this->load->model('token_model', null, true);
		$this->lang->load('token');
		
			Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
			Assets::add_js('jquery-ui-1.8.13.min.js');
			Assets::add_css('jquery-ui-timepicker.css');
			Assets::add_js('jquery-ui-timepicker-addon.js');
		Template::set_block('sub_nav', 'content/_sub_nav');

		Assets::add_module_js('token', 'token.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	 */
	public function index()
	{

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->token_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('token_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('token_delete_failure') . $this->token_model->error, 'error');
				}
			}
		}

		$records = $this->token_model->find_all();

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Token');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Creates a Token object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Token.Content.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_token())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('token_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'token');

				Template::set_message(lang('token_create_success'), 'success');
				redirect(SITE_AREA .'/content/token');
			}
			else
			{
				Template::set_message(lang('token_create_failure') . $this->token_model->error, 'error');
			}
		}
		Assets::add_module_js('token', 'token.js');

		Template::set('toolbar_title', lang('token_create') . ' Token');
		Template::render();
	}

	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_token($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['token']        = $this->input->post('token_token');
		$data['user_id']        = $this->input->post('token_user_id');
		$data['created_on']        = $this->input->post('token_created_on') ? $this->input->post('token_created_on') : '0000-00-00 00:00:00';
		$data['modified_on']        = $this->input->post('token_modified_on') ? $this->input->post('token_modified_on') : '0000-00-00 00:00:00';
		$data['expire_on']        = $this->input->post('token_expire_on') ? $this->input->post('token_expire_on') : '0000-00-00 00:00:00';
		$data['status']        = $this->input->post('token_status');

		if ($type == 'insert')
		{
			$id = $this->token_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->token_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}