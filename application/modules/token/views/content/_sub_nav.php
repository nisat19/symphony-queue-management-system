<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/content/token') ?>" id="list"><?php echo lang('token_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Token.Content.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/content/token/create') ?>" id="create_new"><?php echo lang('token_new'); ?></a>
	</li>
	<?php endif; ?>
</ul>