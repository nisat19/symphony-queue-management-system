<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($token))
{
	$token = (array) $token;
}
$id = isset($token['id']) ? $token['id'] : '';

?>
<div class="admin-box">
	<h3>Token</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('token') ? 'error' : ''; ?>">
				<?php echo form_label('Token'. lang('bf_form_label_required'), 'token_token', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='token_token' type='text' name='token_token' maxlength="32" value="<?php echo set_value('token_token', isset($token['token']) ? $token['token'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('token'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('user_id') ? 'error' : ''; ?>">
				<?php echo form_label('User Id'. lang('bf_form_label_required'), 'token_user_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='token_user_id' type='text' name='token_user_id' maxlength="20" value="<?php echo set_value('token_user_id', isset($token['user_id']) ? $token['user_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('user_id'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('created_on') ? 'error' : ''; ?>">
				<?php echo form_label('Created On', 'token_created_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='token_created_on' type='text' name='token_created_on' maxlength="1" value="<?php echo set_value('token_created_on', isset($token['created_on']) ? $token['created_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('created_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('modified_on') ? 'error' : ''; ?>">
				<?php echo form_label('Modified On', 'token_modified_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='token_modified_on' type='text' name='token_modified_on' maxlength="1" value="<?php echo set_value('token_modified_on', isset($token['modified_on']) ? $token['modified_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('modified_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('expire_on') ? 'error' : ''; ?>">
				<?php echo form_label('Expire On', 'token_expire_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='token_expire_on' type='text' name='token_expire_on' maxlength="1" value="<?php echo set_value('token_expire_on', isset($token['expire_on']) ? $token['expire_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('expire_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('status') ? 'error' : ''; ?>">
				<?php echo form_label('Status', 'token_status', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<label class='checkbox' for='token_status'>
						<input type='checkbox' id='token_status' name='token_status' value='1' <?php echo (isset($token['status']) && $token['status'] == 1) ? 'checked="checked"' : set_checkbox('token_status', 1); ?>>
						<span class='help-inline'><?php echo form_error('status'); ?></span>
					</label>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('token_action_create'); ?>"  />
				<?php echo lang('bf_or'); ?>
				<?php echo anchor(SITE_AREA .'/content/token', lang('token_cancel'), 'class="btn btn-warning"'); ?>
				
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>