<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['token_manage']			= 'Manage Token';
$lang['token_edit']				= 'Edit';
$lang['token_true']				= 'True';
$lang['token_false']				= 'False';
$lang['token_create']			= 'Create';
$lang['token_list']				= 'List';
$lang['token_new']				= 'New';
$lang['token_edit_text']			= 'Edit this to suit your needs';
$lang['token_no_records']		= 'There aren\'t any token in the system.';
$lang['token_create_new']		= 'Create a new Token.';
$lang['token_create_success']	= 'Token successfully created.';
$lang['token_create_failure']	= 'There was a problem creating the token: ';
$lang['token_create_new_button']	= 'Create New Token';
$lang['token_invalid_id']		= 'Invalid Token ID.';
$lang['token_edit_success']		= 'Token successfully saved.';
$lang['token_edit_failure']		= 'There was a problem saving the token: ';
$lang['token_delete_success']	= 'record(s) successfully deleted.';
$lang['token_delete_failure']	= 'We could not delete the record: ';
$lang['token_delete_error']		= 'You have not selected any records to delete.';
$lang['token_actions']			= 'Actions';
$lang['token_cancel']			= 'Cancel';
$lang['token_delete_record']		= 'Delete this Token';
$lang['token_delete_confirm']	= 'Are you sure you want to delete this token?';
$lang['token_edit_heading']		= 'Edit Token';

// Create/Edit Buttons
$lang['token_action_edit']		= 'Save Token';
$lang['token_action_create']		= 'Create Token';

// Activities
$lang['token_act_create_record']	= 'Created record with ID';
$lang['token_act_edit_record']	= 'Updated record with ID';
$lang['token_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['token_column_created']	= 'Created';
$lang['token_column_deleted']	= 'Deleted';
$lang['token_column_modified']	= 'Modified';
