<?php
$num_columns = 7;
$can_delete = $this->auth->has_permission('Counters.Reports.Delete');
$can_edit = $this->auth->has_permission('Counters.Reports.Edit');
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="admin-box">
    <h3>Counters</h3>
    <?php echo form_open($this->uri->uri_string()); ?>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <?php if ($can_delete && $has_records) : ?>
                    <th class="column-check"><input class="check-all" type="checkbox" /></th>
                <?php endif; ?>

                <th>Title</th>
                <th>Description</th>
                <th>Branch</th>
                <th style="text-align: center;">Executive Name</th>
                <th style="text-align: center;">Executive Email</th>
                <th style="text-align: center;">Status</th>
                <th>Created On</th>
                <th>Modified On</th>
            </tr>
        </thead>
        <?php if ($has_records) : ?>
            <tfoot>
                <?php if ($can_delete) : ?>
                    <tr>
                        <td colspan="<?php echo $num_columns; ?>">
                            <?php echo lang('bf_with_selected'); ?>
                            <input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('counters_delete_confirm'))); ?>')" />
                        </td>
                    </tr>
                <?php endif; ?>
            </tfoot>
        <?php endif; ?>
        <tbody>
            <?php
            if ($has_records) :
                foreach ($records as $record) :
                    ?>
                    <tr>
                        <?php if ($can_delete) : ?>
                            <td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td>
                        <?php endif; ?>

                        <?php if ($can_edit) : ?>
                            <td><?php echo anchor(SITE_AREA . '/reports/counters/edit/' . $record->id, '<span class="icon-pencil"></span>' . $record->title); ?></td>
                        <?php else : ?>
                            <td><?php e($record->title); ?></td>
                        <?php endif; ?>
                        <td><?php e($record->description) ?></td>
                        <td><?php e($record->branch_name) ?></td>
                        <td style="text-align: center;"><?php echo $record->display_name != "" ? $record->display_name : "n/a"; ?></td>
                        <td style="text-align: center;"><?php echo $record->email != "" ? $record->email : "n/a"; ?></td>
                        <td style="text-align: center;"><?php echo $record->status == "serving" ? "Active" : "Inactive"; ?></td>
                        <td><?php echo date("F j, Y, g:i a", strtotime($record->created_on)) ?></td>
                        <td><?php echo $record->modified_on != "" ? date("F j, Y, g:i a", strtotime($record->modified_on)) : ""; ?></td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
    <?php echo form_close(); ?>
</div>