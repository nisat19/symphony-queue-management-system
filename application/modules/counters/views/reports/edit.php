<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($counters))
{
	$counters = (array) $counters;
}
$id = isset($counters['id']) ? $counters['id'] : '';

?>
<div class="admin-box">
	<h3>Counters</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('title') ? 'error' : ''; ?>">
				<?php echo form_label('Title'. lang('bf_form_label_required'), 'counters_title', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='counters_title' type='text' name='counters_title' maxlength="150" value="<?php echo set_value('counters_title', isset($counters['title']) ? $counters['title'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('title'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description'. lang('bf_form_label_required'), 'counters_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'counters_description', 'id' => 'counters_description', 'rows' => '5', 'cols' => '80', 'value' => set_value('counters_description', isset($counters['description']) ? $counters['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('branch_id') ? 'error' : ''; ?>">
				<?php echo form_label('Branch Id'. lang('bf_form_label_required'), 'counters_branch_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='counters_branch_id' type='text' name='counters_branch_id' maxlength="11" value="<?php echo set_value('counters_branch_id', isset($counters['branch_id']) ? $counters['branch_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('branch_id'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					20 => 20,
				);

				echo form_dropdown('counters_status', $options, set_value('counters_status', isset($counters['status']) ? $counters['status'] : ''), 'Status'. lang('bf_form_label_required'));
			?>

			<div class="control-group <?php echo form_error('created_on') ? 'error' : ''; ?>">
				<?php echo form_label('Created On', 'counters_created_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='counters_created_on' type='text' name='counters_created_on' maxlength="1" value="<?php echo set_value('counters_created_on', isset($counters['created_on']) ? $counters['created_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('created_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('modified_on') ? 'error' : ''; ?>">
				<?php echo form_label('Modified On', 'counters_modified_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='counters_modified_on' type='text' name='counters_modified_on' maxlength="1" value="<?php echo set_value('counters_modified_on', isset($counters['modified_on']) ? $counters['modified_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('modified_on'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('counters_action_edit'); ?>"  />
				<?php echo lang('bf_or'); ?>
				<?php echo anchor(SITE_AREA .'/reports/counters', lang('counters_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Counters.Reports.Delete')) : ?>
				or
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('counters_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('counters_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>