<?php
$num_columns = 8;
$can_delete = $this->auth->has_permission('Counters.Content.Delete');
$can_edit = $this->auth->has_permission('Counters.Content.Edit');
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Counters</h3>
    <?php echo form_open($this->uri->uri_string()); ?>
    <table class="table table-striped table-bordered"id="tableCounter">
        <thead>
            <tr>
                <?php if ($can_delete && $has_records) : ?>
                    <th class="column-check"><input class="check-all" type="checkbox" /></th>
                <?php endif; ?>

                <th>Title</th>
                <th>Description</th>
                <th>Branch</th>
                <th style="text-align: center;">Executive Name</th>
                <th style="text-align: center;">Executive Email</th>
                <th style="text-align: center;">Status</th>
                <th>Created On</th>
                <!--<th>Modified On</th>-->
            </tr>
        </thead>
        <?php if ($has_records) : ?>
            <tfoot>
                <?php if ($can_delete) : ?>
                    <tr>
                        <td colspan="<?php echo $num_columns; ?>">
                            <?php echo lang('bf_with_selected'); ?>
                            <input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('counters_delete_confirm'))); ?>')" />
                        </td>
                    </tr>
                <?php endif; ?>
            </tfoot>
        <?php endif; ?>
        <tbody>
            <?php
            if ($has_records) :
                foreach ($records as $record) :
                    ?>
                    <tr>
                        <?php if ($can_delete) : ?>
                            <td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td>
                        <?php endif; ?>

                        <?php if ($can_edit) : ?>
                            <td><?php echo anchor(SITE_AREA . '/content/counters/edit/' . $record->id, '<span class="icon-pencil"></span>' . $record->title); ?>
                                &nbsp;<a style="font-variant: small-caps;" id="<?php echo $record->id; ?>" class="viewCat pull-right" href="#counterModal" data-toggle="modal">categories</a></td>
                        <?php else : ?>
                            <td><?php e($record->title); ?></td>
                        <?php endif; ?>
                        <td><?php e($record->description) ?></td>
                        <td><?php e($record->branch_name) ?></td>
                        <td style="text-align: center;"><?php echo $record->display_name != "" ? $record->display_name : "n/a"; ?></td>
                        <td style="text-align: center;"><?php echo $record->email != "" ? $record->email : "n/a"; ?></td>
                        <td style="text-align: center;"><?php echo $record->status == "serving" ? "Active" : "Inactive"; ?></td>
                        <td><?php echo date("F j, Y, g:i a", strtotime($record->created_on)) ?></td>
                        <!--<td><?php e($record->modified_on) ?></td>-->
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
    <?php echo form_close(); ?>
    <div id="counterModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="counterModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="counterModalLabel">Category List</h3>
        </div>
        <div class="modal-body">
            <div id="catRes"></div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
</div>