<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/content/counters') ?>" id="list"><?php echo lang('counters_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Counters.Content.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/content/counters/create') ?>" id="create_new"><?php echo lang('counters_new'); ?></a>
	</li>
	<?php endif; ?>
</ul>