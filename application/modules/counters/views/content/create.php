<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($counters)) {
    $counters = (array) $counters;
}
$id = isset($counters['id']) ? $counters['id'] : '';
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Counters</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>

        <div class="control-group <?php echo form_error('title') ? 'error' : ''; ?>">
            <?php echo form_label('Title' . lang('bf_form_label_required'), 'counters_title', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='counters_title' type='text' name='counters_title' maxlength="150" value="<?php echo set_value('counters_title', isset($counters['title']) ? $counters['title'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('title'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('description') ? 'error' : ''; ?>">
            <?php echo form_label('Description' . lang('bf_form_label_required'), 'counters_description', array('class' => 'control-label')); ?>
            <div class='controls'>
                <?php echo form_textarea(array('name' => 'counters_description', 'id' => 'counters_description', 'rows' => '5', 'cols' => '80', 'value' => set_value('counters_description', isset($counters['description']) ? $counters['description'] : ''))); ?>
                <span class='help-inline'><?php echo form_error('description'); ?></span>
            </div>
        </div>

        <?php
        $options = array();
        if (isset($branches)) {
            foreach ($branches as $store) {
                $options[$store->id] = $store->name;
            }
        }

        echo form_dropdown('counters_branch_id', $options, set_value('counters_branch_id', isset($counters['branch_id']) ? $counters['branch_id'] : ''), 'Branch Id' . lang('bf_form_label_required'));
        ?>

        <div class="control-group <?php echo form_error('categories') ? 'error' : ''; ?>">
            <?php echo form_label('Categories' . lang('bf_form_label_required'), 'counters_categories', array('class' => 'control-label')); ?>
            <div class='controls'>
                <?php
                if (isset($categories) && count($categories) > 0) {
                    foreach ($categories as $cat) {
                        ?>
                        <input type="checkbox" name="category_list[]" id="category_list" value="<?php echo $cat->id ?>" /> <?php echo $cat->name ?> <br/>
                        <?php
                    }
                }
                ?>
            </div>
        </div>

        <div class="form-actions">
            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('counters_action_create'); ?>"  />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/content/counters', lang('counters_cancel'), 'class="btn btn-warning"'); ?>

        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>