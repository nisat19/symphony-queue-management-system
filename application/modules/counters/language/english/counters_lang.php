<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['counters_manage']			= 'Manage Counters';
$lang['counters_edit']				= 'Edit';
$lang['counters_true']				= 'True';
$lang['counters_false']				= 'False';
$lang['counters_create']			= 'Create';
$lang['counters_list']				= 'List';
$lang['counters_new']				= 'New';
$lang['counters_edit_text']			= 'Edit this to suit your needs';
$lang['counters_no_records']		= 'There aren\'t any counters in the system.';
$lang['counters_create_new']		= 'Create a new Counters.';
$lang['counters_create_success']	= 'Counters successfully created.';
$lang['counters_create_failure']	= 'There was a problem creating the counters: ';
$lang['counters_create_new_button']	= 'Create New Counters';
$lang['counters_invalid_id']		= 'Invalid Counters ID.';
$lang['counters_edit_success']		= 'Counters successfully saved.';
$lang['counters_edit_failure']		= 'There was a problem saving the counters: ';
$lang['counters_delete_success']	= 'record(s) successfully deleted.';
$lang['counters_delete_failure']	= 'We could not delete the record: ';
$lang['counters_delete_error']		= 'You have not selected any records to delete.';
$lang['counters_actions']			= 'Actions';
$lang['counters_cancel']			= 'Cancel';
$lang['counters_delete_record']		= 'Delete this Counters';
$lang['counters_delete_confirm']	= 'Are you sure you want to delete this counters?';
$lang['counters_edit_heading']		= 'Edit Counters';

// Create/Edit Buttons
$lang['counters_action_edit']		= 'Save Counters';
$lang['counters_action_create']		= 'Create Counters';

// Activities
$lang['counters_act_create_record']	= 'Created record with ID';
$lang['counters_act_edit_record']	= 'Updated record with ID';
$lang['counters_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['counters_column_created']	= 'Created';
$lang['counters_column_deleted']	= 'Deleted';
$lang['counters_column_modified']	= 'Modified';
