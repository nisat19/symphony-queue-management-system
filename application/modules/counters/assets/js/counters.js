$(document).ready(function() {
    var counter_list = $('table#tableCounter').find('tbody tr').length;
    if (counter_list > 1) {
        $('table#tableCounter').dataTable({
            "bProcessing": true,
            "bDestroy": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ Per Page",
                "sInfo": "Showing _START_ to _END_ of _TOTAL_ Users"
            },
            "sPaginationType": "full_numbers",
            "aaSorting": [[1, 'asc']]
        });
    }

    $('.viewCat').on("click", function() {
        var counter_id = this.id;
        console.log("BDTCHJ " + counter_id);

        var data = 'counter_id=' + counter_id;
        $.ajax({
            type: "POST",
            url: base_url + "frontdesk/content/counters/catListBy",
            dataType: "json",
            data: data,
            success: function(data) {
                if (data.Data == true) {
                    $('#counterModal #catRes').html(data.catItem);
                }
                else {
                    $('#counterModal #catRes').html("No Category Exit");
                }
            }
        });

    });
});