<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * reports controller
 */
class reports extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Counters.Reports.View');
        $this->load->model('counters_model', null, true);
        $this->lang->load('counters');

        Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
        Assets::add_js('jquery-ui-1.8.13.min.js');
        Assets::add_css('jquery-ui-timepicker.css');
        Assets::add_js('jquery-ui-timepicker-addon.js');
        Template::set_block('sub_nav', 'reports/_sub_nav');

        Assets::add_module_js('counters', 'counters.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index() {

        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->counters_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('counters_delete_success'), 'success');
                } else {
                    Template::set_message(lang('counters_delete_failure') . $this->counters_model->error, 'error');
                }
            }
        }

        $records = $this->counters_model->getAllCounterInfo()->result();

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Counters');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Counters object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Counters.Reports.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_counters()) {
                // Log the activity
                log_activity($this->current_user->id, lang('counters_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'counters');

                Template::set_message(lang('counters_create_success'), 'success');
                redirect(SITE_AREA . '/reports/counters');
            } else {
                Template::set_message(lang('counters_create_failure') . $this->counters_model->error, 'error');
            }
        }
        Assets::add_module_js('counters', 'counters.js');

        Template::set('toolbar_title', lang('counters_create') . ' Counters');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Counters data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('counters_invalid_id'), 'error');
            redirect(SITE_AREA . '/reports/counters');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Counters.Reports.Edit');

            if ($this->save_counters('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('counters_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'counters');

                Template::set_message(lang('counters_edit_success'), 'success');
            } else {
                Template::set_message(lang('counters_edit_failure') . $this->counters_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Counters.Reports.Delete');

            if ($this->counters_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('counters_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'counters');

                Template::set_message(lang('counters_delete_success'), 'success');

                redirect(SITE_AREA . '/reports/counters');
            } else {
                Template::set_message(lang('counters_delete_failure') . $this->counters_model->error, 'error');
            }
        }
        Template::set('counters', $this->counters_model->find($id));
        Template::set('toolbar_title', lang('counters_edit') . ' Counters');
        Template::render();
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_counters($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['title'] = $this->input->post('counters_title');
        $data['description'] = $this->input->post('counters_description');
        $data['branch_id'] = $this->input->post('counters_branch_id');
        $data['status'] = $this->input->post('counters_status');
        $data['created_on'] = $this->input->post('counters_created_on');
        $data['modified_on'] = $this->input->post('counters_modified_on') ? $this->input->post('counters_modified_on') : '0000-00-00 00:00:00';

        if ($type == 'insert') {
            $id = $this->counters_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->counters_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
