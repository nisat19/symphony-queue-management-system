<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/reports/companies') ?>" id="list"><?php echo lang('companies_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Companies.Reports.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/reports/companies/create') ?>" id="create_new"><?php echo lang('companies_new'); ?></a>
	</li>
	<?php endif; ?>
</ul>