<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($companies))
{
	$companies = (array) $companies;
}
$id = isset($companies['id']) ? $companies['id'] : '';

?>
<div class="admin-box">
	<h3>Companies</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'companies_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_name' type='text' name='companies_name' maxlength="100" value="<?php echo set_value('companies_name', isset($companies['name']) ? $companies['name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('email') ? 'error' : ''; ?>">
				<?php echo form_label('Email'. lang('bf_form_label_required'), 'companies_email', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_email' type='text' name='companies_email' maxlength="50" value="<?php echo set_value('companies_email', isset($companies['email']) ? $companies['email'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('email'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('admin_id') ? 'error' : ''; ?>">
				<?php echo form_label('Admin Id'. lang('bf_form_label_required'), 'companies_admin_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_admin_id' type='text' name='companies_admin_id' maxlength="11" value="<?php echo set_value('companies_admin_id', isset($companies['admin_id']) ? $companies['admin_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('admin_id'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('is_master') ? 'error' : ''; ?>">
				<?php echo form_label('Is Master'. lang('bf_form_label_required'), 'companies_is_master', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<label class='checkbox' for='companies_is_master'>
						<input type='checkbox' id='companies_is_master' name='companies_is_master' value='1' <?php echo (isset($companies['is_master']) && $companies['is_master'] == 1) ? 'checked="checked"' : set_checkbox('companies_is_master', 1); ?>>
						<span class='help-inline'><?php echo form_error('is_master'); ?></span>
					</label>
				</div>
			</div>

			<div class="control-group <?php echo form_error('payment_account') ? 'error' : ''; ?>">
				<?php echo form_label('Payment Account'. lang('bf_form_label_required'), 'companies_payment_account', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_payment_account' type='text' name='companies_payment_account' maxlength="50" value="<?php echo set_value('companies_payment_account', isset($companies['payment_account']) ? $companies['payment_account'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('payment_account'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('status') ? 'error' : ''; ?>">
				<?php echo form_label('Status'. lang('bf_form_label_required'), 'companies_status', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<label class='checkbox' for='companies_status'>
						<input type='checkbox' id='companies_status' name='companies_status' value='1' <?php echo (isset($companies['status']) && $companies['status'] == 1) ? 'checked="checked"' : set_checkbox('companies_status', 1); ?>>
						<span class='help-inline'><?php echo form_error('status'); ?></span>
					</label>
				</div>
			</div>

			<div class="control-group <?php echo form_error('created_on') ? 'error' : ''; ?>">
				<?php echo form_label('Created On', 'companies_created_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_created_on' type='text' name='companies_created_on' maxlength="1" value="<?php echo set_value('companies_created_on', isset($companies['created_on']) ? $companies['created_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('created_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('modified_on') ? 'error' : ''; ?>">
				<?php echo form_label('Modified On', 'companies_modified_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_modified_on' type='text' name='companies_modified_on' maxlength="1" value="<?php echo set_value('companies_modified_on', isset($companies['modified_on']) ? $companies['modified_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('modified_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('deleted') ? 'error' : ''; ?>">
				<?php echo form_label('Deleted', 'companies_deleted', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<label class='checkbox' for='companies_deleted'>
						<input type='checkbox' id='companies_deleted' name='companies_deleted' value='1' <?php echo (isset($companies['deleted']) && $companies['deleted'] == 1) ? 'checked="checked"' : set_checkbox('companies_deleted', 1); ?>>
						<span class='help-inline'><?php echo form_error('deleted'); ?></span>
					</label>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('companies_action_edit'); ?>"  />
				<?php echo lang('bf_or'); ?>
				<?php echo anchor(SITE_AREA .'/reports/companies', lang('companies_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Companies.Reports.Delete')) : ?>
				or
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('companies_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('companies_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>