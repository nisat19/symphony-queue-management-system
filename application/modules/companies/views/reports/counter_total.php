<?php
$num_columns = 10;
$can_delete = $this->auth->has_permission('Companies.Reports.Delete');
$can_edit = $this->auth->has_permission('Companies.Reports.Edit');
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="admin-box">
    <h3>Counters</h3>
    <div class="clearfix" style="margin-bottom: 5px;">
        <div class="span6" style="margin-left: 0px;">
            Date Range : <input type="text" id="datefrom" name="datefrom"/> to <input type="text" id="dateto" name="dateto"/>
        </div>
        <div>
            <div id="export_ticket" class="pull-right btn btn-warning">Export to Excel</div>
            <a href="" id="export_link" class="pull-right btn btn-success" style="display: none; margin-right: 5px;">Download</a>
        </div>
    </div>
    <?php echo form_open($this->uri->uri_string()); ?>
    <table class="table table-striped table-bordered" id="avgUserTable">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Branch</th>
                <th>Counter</th>
                <th style="text-align: center">Total Active Time</th>
                <th style="text-align: center">Total Serve</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <?php echo form_close(); ?>
</div>