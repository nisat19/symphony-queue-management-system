<style>
    th.id { width: 3em; }
    th.last-login { width: 11em; }
    th.status { width: 10em; }
</style>
<div class="pull-right" id="sub-menu" style="margin-top: 10px;">
    <?php Template::block('sub_nav', ''); ?>
</div>
<div class="well shallow-well">
    <span class="filter-link-list">
        &nbsp;
    </span>
</div>

<div id="confirmModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Choose a Role</h3>
    </div>
    <div class="modal-body">
        User Role &nbsp;&nbsp;&nbsp;
        <select id="tfcounter_id" name="tfcounter_id">
            <option value="7">Executive</option>
            <option value="8">Manager</option>
            <!--<option value="7">Site Admin</option>-->
        </select>
        <input type="hidden" id="trans_id" name="trans_id" value="" />
        <div id="showrr" class="clearfix"></div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary btn_change">Change Role</button>
    </div>
</div>
<ul class="nav nav-tabs" >
    <li<?php echo $filter_type == 'all' ? ' class="active"' : ''; ?>><?php echo anchor(SITE_AREA . '/content/companies/stuff_list/', lang('us_tab_all')); ?></li>
    <?php if ($this->auth->user()->role_id == 9) { ?>
        <li<?php echo $filter_type == 'inactive' ? ' class="active"' : ''; ?>><?php echo anchor(SITE_AREA . '/content/companies/stuff_list/' . 'inactive/', lang('us_tab_inactive')); ?></li>
        <!--<li<?php echo $filter_type == 'banned' ? ' class="active"' : ''; ?>><?php echo anchor(SITE_AREA . '/content/companies/stuff_list/' . 'banned/', lang('us_tab_banned')); ?></li>-->
        <li<?php echo $filter_type == 'deleted' ? ' class="active"' : ''; ?>><?php echo anchor(SITE_AREA . '/content/companies/stuff_list/' . 'deleted/', lang('us_tab_deleted')); ?></li>
        <?php if ($this->auth->user()->role_id == 1) { ?>
            <li class="<?php echo $filter_type == 'role_id' ? 'active ' : ''; ?>dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <?php
                    echo lang('us_tab_roles');
                    echo isset($filter_role) ? ": $filter_role" : '';
                    ?>
                    <span class="caret light-caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <?php foreach ($roles as $role) : ?>
                        <li><?php echo anchor($index_url . 'role_id-' . $role->role_id, $role->role_name); ?></li>
                    <?php endforeach; ?>
                </ul>
            </li>
        <?php } ?>
    <?php } ?>
</ul>
<?php
$num_columns = 8;
$has_users = isset($users) && is_array($users) && count($users);
echo form_open($this->uri->uri_string());
?>
<table class="table table-striped table-bordered" id="userTable">
    <thead>
        <tr>
            <th class="column-check"><input class="check-all" type="checkbox" /></th>
            <!--<th class='id'><?php echo lang('bf_id'); ?></th>-->
            <th><?php echo lang('bf_username'); ?></th>
            <th><?php echo lang('bf_display_name'); ?></th>
            <th><?php echo lang('bf_email'); ?></th>
            <th><?php echo lang('us_role'); ?></th>
            <th class='last-login'><?php echo lang('us_last_login'); ?></th>
            <th class='status'><?php echo lang('us_status'); ?></th>
            <?php if ($this->auth->user()->role_id == 9) { ?>
                <th>Change Role</th>
            <?php } ?>
        </tr>
    </thead>
    <?php if ($has_users) : ?>
        <tfoot>
            <tr>
                <td colspan="<?php echo $num_columns; ?>">
                    <?php if ($this->auth->user()->role_id == 8 || $this->auth->user()->role_id == 9) { ?>
                        <?php
                        echo lang('bf_with_selected');

                        if ($filter_type == 'deleted') :
                            ?>
                            <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore'); ?>" />
                            <input type="submit" name="purge" class="btn btn-danger" value="<?php echo lang('bf_action_purge'); ?>" onclick="return confirm('<?php e(js_escape(lang('us_purge_del_confirm'))); ?>')" />
                            <?php
                        else :
                            if ($this->auth->user()->role_id != 7) {
                                ?>
                                <input type="submit" name="activate" class="btn" value="<?php echo lang('bf_action_activate'); ?>" />
                                <input type="submit" name="deactivate" class="btn" value="<?php echo lang('bf_action_deactivate'); ?>" />
                                <!--<input type="submit" name="ban" class="btn" value="<?php echo lang('bf_action_ban'); ?>" />-->
                                <input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('us_delete_account_confirm'))); ?>')" />
                            <?php } endif; ?>
                    <?php } ?>
                </td>
            </tr>
        </tfoot>
    <?php endif; ?>
    <tbody>
        <?php
        if ($has_users) :
            foreach ($users as $user) :
                ?>
                <tr>
                    <?php if ($user->role_id != 9) { ?>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $user->id; ?>" /></td>
                    <?php } else { ?>
                        <td class="column-check">#</td>
                    <?php } ?>
        <!--<td class='id'><?php echo $user->id; ?></td>-->
                    <td><?php
                        echo anchor(site_url(SITE_AREA . '/content/companies/profile/' . $user->id), $user->username);

                        if ($user->banned) :
                            ?>
                            <span class="label label-warning">Banned</span>
                        <?php endif; ?>
                    </td>
                    <td><?php echo $user->display_name; ?></td>
                    <td><?php echo $user->email ? mailto($user->email) : ''; ?></td>
                    <td><?php echo $roles[$user->role_id]->role_name; ?></td>
                    <td class='last-login'><?php echo $user->last_login != '0000-00-00 00:00:00' ? date('M j, y g:i A', strtotime($user->last_login)) : '---'; ?></td>
                    <td class='status'>
                        <?php if ($user->active) : ?>
                            <span class="label label-success"><?php echo lang('us_active'); ?></span>
                        <?php else : ?>
                            <span class="label label-warning"><?php echo lang('us_inactive'); ?></span>
                        <?php endif; ?>
                    </td>
                    <?php if ($this->auth->user()->role_id == 9) { ?>
                        <td><a id="<?php echo $user->id; ?>" class="btn btn-danger btn_modal">Change</a></td>
                    <?php } ?>
                </tr>
                <?php
            endforeach;
        else :
            ?>
            <tr>
                <td colspan="<?php echo $num_columns; ?>"><?php echo lang('us_no_users'); ?></td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>
<?php
echo form_close();

echo $this->pagination->create_links();
?>