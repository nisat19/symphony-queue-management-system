<?php
$validation_errors = validation_errors();
$controlClass = empty($controlClass) ? 'span3' : $controlClass;
if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($companies)) {
    $companies = (array) $companies;
}
$id = isset($companies['id']) ? $companies['id'] : '';
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Companies</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>

        <div class="control-group <?php echo form_error('user_email') ? 'error' : ''; ?>">
            <?php echo form_label('Email' . lang('bf_form_label_required'), 'user_email', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='user_email' type='text' class="span3" name='user_email' maxlength="100" value="<?php echo set_value('user_email', isset($companies['email']) ? $companies['email'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('user_email'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('user_display_name') ? 'error' : ''; ?>">
            <?php echo form_label('Display Name' . lang('bf_form_label_required'), 'user_display_name', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='user_display_name' type='text' class="span3" name='user_display_name' maxlength="50" value="<?php echo set_value('user_display_name', isset($companies['display_name']) ? $companies['display_name'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('user_display_name'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('user_name') ? 'error' : ''; ?>">
            <?php echo form_label('Username' . lang('bf_form_label_required'), 'user_name', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='user_name' type='text' class="span3" name='user_name' maxlength="11" value="<?php echo set_value('user_name', isset($companies['username']) ? $companies['username'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('user_name'); ?></span>
            </div>
        </div>

        <?php
        if ($companies['role_id'] != 1) {
            $options = array();
            $options[0] = "--";
            if (isset($counters) && $counters != FALSE) {
                foreach ($counters as $count) {
                    $options[$count->id] = ucfirst($count->title);
                }
            }

            echo form_dropdown('counter', $options, set_value('counter', isset($companies['counter_id']) ? $companies['counter_id'] : ''), 'Counter', 'class="span3"');
        }
        ?>

        <?php
        if ($companies['role_id'] != 1) {
            $options = array();
            $options[0] = "--";
            if (isset($branches) && $branches != FALSE) {
                foreach ($branches as $count) {
                    $options[$count->id] = ucfirst($count->name);
                }
            }

            echo form_dropdown('user_branch_id', $options, set_value('user_branch_id', isset($companies['branch_id']) ? $companies['branch_id'] : ''), 'Branch', 'class="span3"');
        }
        ?>

        <?php
        if ($companies['role_id'] != 1) {
            if ($this->auth->user()->role_id == 7) {
                $options = array(0 => '--', 7 => 'Executive');
            } else {
                $options = array(0 => '--', 7 => 'Executive', 8 => 'Manager');
            }

            echo form_dropdown('user_role_id', $options, set_value('user_role_id', isset($companies['role_id']) ? $companies['role_id'] : ''), 'User Role', 'class="span3"');
        }
        ?>

        <div class="form-actions">
            <input type="submit" name="save" class="btn btn-primary" value="Update"  />
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>