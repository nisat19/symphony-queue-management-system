<?php
$errorClass = empty($errorClass) ? ' error' : $errorClass;
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Change Password</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>

        <div class="control-group <?php echo iif(form_error('password'), $errorClass); ?>">
            <?php echo form_label('Password' . lang('bf_form_label_required'), 'password', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='password' type='password' name='password' maxlength="8" value="" />
                <span class='help-inline'><?php echo form_error('password'); ?>(Password must be at least 8 characters long.)</span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('pass_confirm') ? 'error' : ''; ?>">
            <?php echo form_label('Confirm Password' . lang('bf_form_label_required'), 'pass_confirm', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='pass_confirm' type='password' name='pass_confirm' maxlength="8" value="" />
                <span class='help-inline'><?php echo form_error('pass_confirm'); ?>(Password must be at least 8 characters long.)</span>
            </div>
        </div>

        <div class="form-actions">
            <input type="submit" name="cng_pass" class="btn btn-primary" value="<?php echo lang('companies_action_create'); ?>"  />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/content/companies', lang('companies_cancel'), 'class="btn btn-warning"'); ?>

        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>