<style type="text/css">
    .admin-box .span2{
        width: 100px;
    }
</style>
<div class="admin-box">

    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
        <ul class="nav nav-pills">
            <li>
                <a class="btn btn-mini" href="<?php echo site_url() . "frontdesk/content/companies/user_edit/" . $user_id ?>">Edit</a>
            </li>
            <?php if ($chng_pass != FALSE) { ?>
                <li>
                    <a class="btn btn-mini" href="<?php echo site_url() . "frontdesk/content/companies/changePassword" ?>">Change Password</a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <h3>Profile</h3>
    <div class="span9 offset1">
        <div class="row-fluid">
            <div class="span2"><h4>Name: </h4></div>
            <div class="span10"><?php echo $records->username; ?></div>
        </div>
        <div class="row-fluid">
            <div class="span2"><h4>Email: </h4></div>
            <div class="span10"><?php echo $records->email; ?></div>
        </div>
        <div class="row-fluid">
            <div class="span2"><h4>Display Name: </h4></div>
            <div class="span10"><?php echo $records->display_name; ?></div>
        </div>
        <div class="row-fluid">
            <div class="span2"><h4>Role: </h4></div>
            <div class="span10"><?php echo $records->role_name; ?></div>
        </div>
        <div class="row-fluid">
            <div class="span2"><h4>Counter: </h4></div>
            <div class="span10"><?php echo $records->counter_name == "" ? "n/a" : $records->counter_name; ?></div>
        </div>
        <div class="row-fluid">
            <div class="span2"><h4>Branch: </h4></div>
            <div class="span10"><?php echo $records->branch_name == "" ? "n/a" : $records->branch_name; ?></div>
        </div>
    </div>
</div>