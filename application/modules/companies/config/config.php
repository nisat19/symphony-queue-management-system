<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['module_config'] = array(
    'description' => 'Can access Users',
    'name' => 'Users',
    'version' => '0.0.1',
    'author' => 'admin',
    'weights' => array(
        'content' => 3
    )
);
