$.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"];
            var iMin = $('#datefrom').val();
            var iMax = $('#dateto').val();
            var date = new Date(iMax);
            date.setDate(date.getDate() + 1);
            iMax = monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear();

            var iVersion = aData[7] === "" ? 0 : aData[7];
            if (iMin === "" && iMax === "")
            {
                return true;
            }
            else if (iMin === "" && iVersion < iMax)
            {
                return true;
            }
            else if (iMin < iVersion && "" === iMax)
            {
                return true;
            }
            else if (iMin < iVersion && iVersion < iMax)
            {
                return true;
            }
            return false;
        }
);

$(document).ready(function() {
    var usr_list = $('table#userTable').find('tbody tr').length;
    if (usr_list > 1) {
        $('table#userTable').dataTable({
            "bProcessing": true,
            "bDestroy": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ Per Page",
                "sInfo": "Showing _START_ to _END_ of _TOTAL_ Users"
            },
            "sPaginationType": "full_numbers",
            "aaSorting": [[1, 'asc']]
        });
    }

    var avgUserTable;
    avgUserTable = $('#avgUserTable').dataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": base_url + "frontdesk/reports/companies/getCounterDetails",
        "aoColumns": [
            {
                "mDataProp": "display_name"
            },
            {
                "mDataProp": "email"
            },
            {
                "mDataProp": "role_name"
            },
            {
                "mDataProp": "name"
            },
            {
                "mDataProp": "title"
            },
            {
                "mDataProp": "time",
                "sClass": "center"
            },
            {
                "mDataProp": "total_serve",
                "sClass": "center"
            },
            {
                "mDataProp": "last_login"
            }
        ],
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sLengthMenu": "_MENU_ Per Page",
            "sSearch": "Search all columns:"
        },
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "fnInitComplete": function() {
//            var self = this;
//            var nTrs = this.fnGetNodes();
//            $('td', nTrs).click(function() {
//                self.fnFilter(this.innerHTML);
//            });
        },
        "bJQueryUI": false
    });

    $('#userTable').delegate('a.btn_modal', 'click', function(e) {
        var trans_id = this.id;
        console.log("Trans : " + trans_id);
        $('#trans_id').val(trans_id);
        $('#confirmModal').modal('show');
//        ticketTable.fnReloadAjax();
    });

    $('div#confirmModal').delegate('.btn_change', 'click', function(e) {
        var trans_id = $('#trans_id').val();
        var tfcounter_id = $('#tfcounter_id').val();
        var data = 'btn_id=' + trans_id + '&stat=' + tfcounter_id;
        $.ajax({
            type: "POST",
            url: base_url + "frontdesk/content/companies/change_user_role",
            dataType: "json",
            data: data,
            success: function(data) {
                if (data.Data == true) {
                    $('#confirmModal').modal('hide');
                    window.location.reload();
                }
                else {
//                    console.log("Error");
                    $("#showrr").html("<br/><h5>Unable to change role. Please Try again after sometimes!</h5>");
                }
            }
        });
        return false;
    });

    $("#datefrom").datepicker(
            {changeYear: true,
                altFormat: "yy-mm-dd",
                dateFormat: "MM d, yy"}
    );
    $("#dateto").datepicker(
            {changeYear: true,
                altFormat: "yy-mm-dd",
                dateFormat: "MM d, yy"}
    );
    $('#datefrom').on("change", function() {
        avgUserTable.fnDraw();
    });
    $('#dateto').on("change", function() {
        avgUserTable.fnDraw();
    });

    $('div#export_ticket').click(function() {
//        console.log($('#datefrom').val());
//        console.log($('#dateto').val());
        var data = 'datefrom=' + $('#datefrom').val() + '&dateto=' + $('#dateto').val();
        $.ajax({
            type: "POST",
            url: base_url + 'frontdesk/reports/companies/export_counter',
            processData: false,
            data: data,
            success: function(html) {
                $('#export_link').attr("href", html);
                $('#export_link').trigger("click");
            },
            failure: function(html) {
                console.log(html);
            }
        });
    });
    $('#export_link').bind('click', function() {
        window.open($(this).attr('href'), '_blank');
    });
});