<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * reports controller
 */
class reports extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Companies.Reports.View');
        $this->load->model('companies_model', null, true);
        $this->load->model('user_timelog/user_timelog_model', null, true);
        $this->lang->load('companies');

        Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
        Assets::add_js('jquery-ui-1.8.13.min.js');
        Assets::add_css('jquery-ui-timepicker.css');
        Assets::add_js('jquery-ui-timepicker-addon.js');
        Template::set_block('sub_nav', 'reports/_sub_nav');

        Assets::add_module_js('companies', 'companies.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index() {
        if ($this->auth->user()->role_id == 7) {
            redirect('frontdesk/content/companies/profile');
        } else {
            redirect('frontdesk/reports/companies/counterTotal');
        }
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->companies_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('companies_delete_success'), 'success');
                } else {
                    Template::set_message(lang('companies_delete_failure') . $this->companies_model->error, 'error');
                }
            }
        }

        $records = $this->companies_model->find_all();

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Companies');
        Template::render();
    }

    //--------------------------------------------------------------------

    public function counterTotal() {
        Assets::add_css('jquery.dataTables');
        Assets::add_js('jquery.dataTables.min');
        Assets::add_js('DT_bootstrap');
        Assets::add_js('dataTablePlugins');
        Template::set('toolbar_title', 'Manage Companies');
        Template::set_view('companies/reports/counter_total');
        Template::render();
    }

    public function getCounterDetails() {
        $records = $this->user_timelog_model->getAllUsers()->result();
        try {
            for ($index = 0; $index < count($records); $index++) {
                $record = $records[$index];
                $record->name = $record->name != "" ? $record->name : "n/a";
                $record->title = $record->title != "" ? $record->title : "n/a";
                $record->time = gmdate("H:i:s", $record->time);
                $record->last_login = date("F j, Y", strtotime($record->last_login));
            }
            echo json_encode(array('aaData' => $records));
            exit();
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
            exit();
        }
    }

    public function export_counter($param = 'Excel') {
        $datefrom = $this->input->post('datefrom');
        $dateto = $this->input->post('dateto');
        try {
            if ($datefrom != "" && $dateto != "")
                $records = $this->user_timelog_model->getAllUsers($datefrom, $dateto)->result();
            else
                $records = $this->user_timelog_model->getAllUsers()->result();
            //Load our new PHPExcel library
            $this->load->library('excel');
            //Activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //Name the worksheet
            $this->excel->getActiveSheet()->setTitle('Test Report');
            //Add Column Heading from Column A to E
            $this->excel->getActiveSheet()->setCellValue('A1', 'Name');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Email');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Role');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Branch');
            $this->excel->getActiveSheet()->setCellValue('E1', 'Counter');
            $this->excel->getActiveSheet()->setCellValue('F1', 'Total Active Time');
            $this->excel->getActiveSheet()->setCellValue('G1', 'Total Serve');
            $this->excel->getActiveSheet()->setCellValue('H1', 'Date');
            //Add Column Heading from Column A to E
            //Add Data to Cells
            $count = count($records);
            $r = 2;
            $total = 0;
            for ($i = 0; $i < $count; $i++) {
                $this->excel->getActiveSheet()->setCellValue("A$r", $records[$i]->display_name != "" ? $records[$i]->display_name : "n/a");
                $this->excel->getActiveSheet()->setCellValue("B$r", $records[$i]->email != "" ? $records[$i]->email : "n/a");
                $this->excel->getActiveSheet()->setCellValue("C$r", $records[$i]->role_name != "" ? $records[$i]->role_name : "n/a");
                $this->excel->getActiveSheet()->setCellValue("D$r", $records[$i]->name != "" ? $records[$i]->name : "n/a");
                $this->excel->getActiveSheet()->setCellValue("E$r", $records[$i]->title != "" ? $records[$i]->title : "n/a");
                $this->excel->getActiveSheet()->setCellValue("F$r", gmdate("H:i:s", $records[$i]->time));
                $this->excel->getActiveSheet()->setCellValue("G$r", $records[$i]->total_serve != "" ? $records[$i]->total_serve : "0");
                $this->excel->getActiveSheet()->setCellValue("H$r", $records[$i]->last_login != "" ? date("F j, Y", strtotime($records[$i]->last_login)) : "n/a");
//                $total = $total + $records[$i]->total_price;
                $r++;
//                if ($r > $count) {
//                    $this->excel->getActiveSheet()->setCellValue("D$r", 'Total Amount');
//                    $this->excel->getActiveSheet()->setCellValue("E$r", $total);
//                }
            }
            //Add Data to Cells
            //Change the font size
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(16);
            //Make the font become bold
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(FALSE);
            //Merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //Set aligment to center for cells (A1 to E1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $datetime = uniqid();
            $filename = "Report_$datetime.xls"; //Save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //Tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //If you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//            $filename = "Report_$datetime.pdf"; //Save our workbook as this file name
//            header('Content-Type: application/pdf');
//            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
            //Force user to download the Excel file without writing it to server's HD
            //$objWriter->save('php://output');
            $this->config->load('download');
            $path = $this->config->item('dload_dir');
            $objWriter->save($path . $filename);
            echo base_url('downloads/' . $filename);
            exit;
        } catch (PHPExcel_Exception $e) {
            echo $e->getTraceAsString();
            exit;
        }
    }

    /**
     * Creates a Companies object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Companies.Reports.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_companies()) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message(lang('companies_create_success'), 'success');
                redirect(SITE_AREA . '/reports/companies');
            } else {
                Template::set_message(lang('companies_create_failure') . $this->companies_model->error, 'error');
            }
        }
        Assets::add_module_js('companies', 'companies.js');

        Template::set('toolbar_title', lang('companies_create') . ' Companies');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Companies data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('companies_invalid_id'), 'error');
            redirect(SITE_AREA . '/reports/companies');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Companies.Reports.Edit');

            if ($this->save_companies('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message(lang('companies_edit_success'), 'success');
            } else {
                Template::set_message(lang('companies_edit_failure') . $this->companies_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Companies.Reports.Delete');

            if ($this->companies_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message(lang('companies_delete_success'), 'success');

                redirect(SITE_AREA . '/reports/companies');
            } else {
                Template::set_message(lang('companies_delete_failure') . $this->companies_model->error, 'error');
            }
        }
        Template::set('companies', $this->companies_model->find($id));
        Template::set('toolbar_title', lang('companies_edit') . ' Companies');
        Template::render();
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_companies($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['name'] = $this->input->post('companies_name');
        $data['email'] = $this->input->post('companies_email');
        $data['admin_id'] = $this->input->post('companies_admin_id');
        $data['is_master'] = $this->input->post('companies_is_master');
        $data['payment_account'] = $this->input->post('companies_payment_account');
        $data['status'] = $this->input->post('companies_status');
        $data['created_on'] = $this->input->post('companies_created_on') ? $this->input->post('companies_created_on') : '0000-00-00 00:00:00';
        $data['modified_on'] = $this->input->post('companies_modified_on') ? $this->input->post('companies_modified_on') : '0000-00-00 00:00:00';
        $data['deleted'] = $this->input->post('companies_deleted');

        if ($type == 'insert') {
            $id = $this->companies_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->companies_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
