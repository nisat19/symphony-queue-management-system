<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * content controller
 */
class content extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Companies.Content.View');
        $this->load->model('companies_model', null, true);
        $this->load->model('roles/role_model');
        $this->load->model('counters/counters_model');
        $this->load->model('branches/branches_model');
        $this->lang->load('companies');
        $this->lang->load('users/users');

        Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
        Assets::add_js('jquery-ui-1.8.13.min.js');
        Assets::add_css('jquery-ui-timepicker.css');
        Assets::add_js('jquery-ui-timepicker-addon.js');
        Template::set_block('sub_nav', 'content/_sub_nav');

        Assets::add_module_js('companies', 'companies.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index() {
        if ($this->auth->user()->role_id == 7) {
            redirect('frontdesk/content/companies/profile');
        } else {
            redirect('frontdesk/content/companies/stuff_list');
        }
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->companies_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('companies_delete_success'), 'success');
                } else {
                    Template::set_message(lang('companies_delete_failure') . $this->companies_model->error, 'error');
                }
            }
        }

        $records = $this->companies_model->find_all();
//        Template::set_theme('symphony', 'default');
        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Companies');
        Template::render();
    }

    public function profile($id = NULL) {
        $chng_pass = FALSE;
        if ($id == NULL) {
            $chng_pass = TRUE;
            $id = $this->auth->user()->id;
        }

        $records = $this->db
                        ->select('bf_users.username, bf_users.email, bf_users.display_name, bf_roles.role_name, bf_users.counter_id, bf_users.branch_id, bf_counter.title as counter_name, bf_branches.`name` as branch_name')
                        ->join('bf_roles', 'bf_roles.role_id = bf_users.role_id', 'inner')
                        ->join('bf_counter', 'bf_counter.id = bf_users.counter_id', 'left')
                        ->join('bf_branches', 'bf_branches.id = bf_users.branch_id', 'left')
                        ->where('bf_users.id', $id)
                        ->get('bf_users')->row();
        Template::set('records', $records);
        Template::set('user_id', $id);
        Template::set('chng_pass', $chng_pass);
        Template::set('toolbar_title', 'Manage User');
        Template::set_view('companies/content/profile');
        Template::render();
    }

    //--------------------------------------------------------------------

    public function stuff_list($filter = 'all', $offset = 0) {
        $this->auth->restrict('Companies.Content.View');

        // Fetch roles we might want to filter on
        $roles = $this->role_model->select('role_id, role_name')->where('deleted', 0)->find_all();
        $ordered_roles = array();
        foreach ($roles as $role) {
            $ordered_roles[$role->role_id] = $role;
        }
        Template::set('roles', $ordered_roles);

        // Do we have any actions?
        if (isset($_POST['activate']))
            $action = '_activate';
        if (isset($_POST['deactivate']))
            $action = '_deactivate';
        if (isset($_POST['ban']))
            $action = '_ban';
        if (isset($_POST['delete']))
            $action = '_delete';
        if (isset($_POST['purge']))
            $action = '_purge';
        if (isset($_POST['restore']))
            $action = '_restore';

        if (isset($action)) {
            $checked = $this->input->post('checked');

            if (!empty($checked)) {
                foreach ($checked as $user_id) {
                    $this->$action($user_id);
                }
            } else {
                Template::set_message(lang('us_empty_id'), 'error');
            }
        }

        // Actions done, now display the view
        $where = array('users.deleted' => 0, 'users.role_id !=' => 1);

        // Filters
        if (preg_match('{first_letter-([A-Z])}', $filter, $matches)) {
            $filter_type = 'first_letter';
            $first_letter = $matches[1];
        } elseif (preg_match('{role_id-([0-9]*)}', $filter, $matches)) {
            $filter_type = 'role_id';
            $role_id = (int) $matches[1];
        } else {
            $filter_type = $filter;
        }

        switch ($filter_type) {
            case 'inactive':
                $where['users.active'] = 0;
                break;

            case 'banned':
                $where['users.banned'] = 1;
                break;

            case 'deleted':
                $where['users.deleted'] = 1;
                break;

            case 'role_id':
                $where['users.role_id'] = $role_id;

                foreach ($roles as $role) {
                    if ($role->role_id == $role_id) {
                        Template::set('filter_role', $role->role_name);
                        break;
                    }
                }
                break;

            case 'first_letter':
                $where['SUBSTRING( LOWER(username), 1, 1)='] = $first_letter;
                break;

            case 'all':
                // Nothing to do
                break;

            default:
                show_404("users/index/$filter/");
        }

        if ($this->auth->user()->role_id == 8) {
            $where = array('users.branch_id' => $this->auth->user()->branch_id, 'users.counter_id !=' => 0);
        }

        // Fetch the users to display
        $this->user_model->limit($this->limit, $offset)->where($where);
        $this->user_model->select('users.id, users.role_id, username, display_name, email, last_login, banned, active, users.deleted, role_name');
        Template::set('users', $this->user_model->find_all());

        // Pagination
        $this->load->library('pagination');

        $this->user_model->where($where);
        $total_users = $this->user_model->count_all();

        $this->pager['base_url'] = site_url(SITE_AREA . "/settings/users/index/$filter/");
        $this->pager['total_rows'] = $total_users;
        $this->pager['per_page'] = $this->limit;
        $this->pager['uri_segment'] = 6;

        $this->pagination->initialize($this->pager);

        Template::set('index_url', site_url(SITE_AREA . '/settings/users/index/') . '/');
        Template::set('filter_type', $filter_type);

        Assets::add_css('jquery.dataTables');
        Assets::add_js('jquery.dataTables.min');
        Assets::add_js('DT_bootstrap');
        Assets::add_js('dataTablePlugins');

        Template::set('toolbar_title', lang('us_user_management'));
        Template::set_view('companies/content/users/stuff_list');
        Template::render();
    }

    private function _activate($user_id) {
        $this->user_status($user_id, 1, 0);
    }

    private function _deactivate($user_id) {
        $this->user_status($user_id, 0, 0);
    }

    private function user_status($user_id = false, $status = 1, $supress_email = 0) {
        $supress_email = (isset($supress_email) && $supress_email == 1 ? true : false);

        if ($user_id !== false && $user_id != -1) {
            $result = false;
            $type = '';
            if ($status == 1) {
                $result = $this->user_model->admin_activation($user_id);
                $type = lang('bf_action_activate');
            } else {
                $result = $this->user_model->admin_deactivation($user_id);
                $type = lang('bf_action_deactivate');
            }

            $user = $this->user_model->find($user_id);
            $log_name = $this->settings_lib->item('auth.use_own_names') ? $this->current_user->username : ($this->settings_lib->item('auth.use_usernames') ? $user->username : $user->email);

            log_activity($this->current_user->id, lang('us_log_status_change') . ': ' . $log_name . ' : ' . $type . "ed", 'users');

            if ($result) {
                $message = lang('us_active_status_changed');
                if ($status == 1 && !$supress_email) {
                    // Now send the email
                    $this->load->library('emailer/emailer');

                    $site_title = $this->settings_lib->item('site.title');

                    $data = array
                        (
                        'to' => $this->user_model->find($user_id)->email,
                        'subject' => lang('us_account_active'),
                        'message' => $this->load->view('_emails/activated', array('link' => site_url(), 'title' => $site_title), true)
                    );

//                    if ($this->emailer->send($data)) {
//                        $message = lang('us_active_email_sent');
//                    } else {
//                        $message = lang('us_err_no_email') . $this->emailer->error;
//                    }
                }
                Template::set_message($message, 'success');
            } else {
                Template::set_message(lang('us_err_status_error') . $this->user_model->error, 'error');
            }//end if
        } else {
            Template::set_message(lang('us_err_no_id'), 'error');
        }//end if
    }

    private function _delete($id) {
        $user = $this->user_model->find($id);

        if (isset($user) && has_permission('Permissions.' . $user->role_name . '.Manage') && $user->id != $this->current_user->id) {
            if ($this->user_model->delete($id)) {

                $user = $this->user_model->find($id);
                $log_name = (isset($user->display_name) && !empty($user->display_name)) ? $user->display_name : ($this->settings_lib->item('auth.use_usernames') ? $user->username : $user->email);
                log_activity($this->current_user->id, lang('us_log_delete') . ': ' . $log_name, 'users');
                Template::set_message(lang('us_action_deleted'), 'success');
            } else {
                Template::set_message(lang('us_action_not_deleted') . $this->user_model->error, 'error');
            }
        } else {
            if ($user->id == $this->current_user->id) {
                Template::set_message(lang('us_self_delete'), 'error');
            } else {
                Template::set_message(sprintf(lang('us_unauthorized'), $user->role_name), 'error');
            }
        }//end if
    }

    private function _purge($id) {
        $this->user_model->delete($id, TRUE);
        Template::set_message(lang('us_action_purged'), 'success');

        // Purge any user meta for this user, also.
        $this->db->where('user_id', $id)->delete('user_meta');

        // Any modules needing to save data?
        Events::trigger('purge_user', $id);
    }

    private function _ban($user_id, $ban_message = '') {
        $data = array(
            'banned' => 1,
            'ban_message' => $ban_message
        );

        $this->user_model->update($user_id, $data);
    }

    private function _restore($id) {
        if ($this->user_model->update($id, array('users.deleted' => 0))) {
            Template::set_message(lang('us_user_restored_success'), 'success');
        } else {
            Template::set_message(lang('us_user_restored_error') . $this->user_model->error, 'error');
        }
    }

    public function register() {
        date_default_timezone_set('Asia/Dhaka');
        // Are users even allowed to register?
//        if (!$this->settings_lib->item('auth.allow_register')) {
//            Template::set_message(lang('us_register_disabled'), 'error');
//            Template::redirect('/');
//        }

        $this->load->model('roles/role_model');
        $this->load->helper('date');

        $this->load->config('address');
        $this->load->helper('address');

        $this->load->config('user_meta');
        $meta_fields = config_item('user_meta_fields');
        Template::set('meta_fields', $meta_fields);

        if (isset($_POST['register'])) {
            // Validate input
            $this->form_validation->set_rules('email', 'lang:bf_email', 'required|trim|valid_email|max_length[120]|unique[users.email]');

            $username_required = '';
            if ($this->settings_lib->item('auth.login_type') == 'username' ||
                    $this->settings_lib->item('auth.use_usernames')) {
                $username_required = 'required|';
            }
            $this->form_validation->set_rules('username', 'lang:bf_username', $username_required . 'trim|max_length[30]|unique[users.username]');

            $this->form_validation->set_rules('password', 'lang:bf_password', 'required|max_length[120]|valid_password');
            $this->form_validation->set_rules('pass_confirm', 'lang:bf_password_confirm', 'required|matches[password]');

            $this->form_validation->set_rules('language', 'lang:bf_language', 'required|trim');
            $this->form_validation->set_rules('timezones', 'lang:bf_timezone', 'required|trim|max_length[4]');
            $this->form_validation->set_rules('display_name', 'lang:bf_display_name', 'trim|max_length[255]');


            $meta_data = array();
            foreach ($meta_fields as $field) {
                if ((!isset($field['admin_only']) || $field['admin_only'] === FALSE || (isset($field['admin_only']) && $field['admin_only'] === TRUE && isset($this->current_user) && $this->current_user->role_id == 1)) && (!isset($field['frontend']) || $field['frontend'] === TRUE)) {
                    $this->form_validation->set_rules($field['name'], $field['label'], $field['rules']);

                    $meta_data[$field['name']] = $this->input->post($field['name']);
                }
            }

            if ($this->form_validation->run() !== FALSE) {
                // Time to save the user...
                $data = array(
                    'email' => $this->input->post('email'),
                    'password' => $this->input->post('password'),
                    'language' => $this->input->post('language'),
                    'timezone' => $this->input->post('timezones'),
                    'display_name' => $this->input->post('display_name'),
                    'company_id' => 1,
                    'role_id' => $this->input->post('user_role_id'),
                    'branch_id' => $this->input->post('user_branch_id'),
                    'counter_id' => $this->input->post('counter'),
                );

                if (isset($_POST['username'])) {
                    $data['username'] = $this->input->post('username');
                }

                // User activation method
                $activation_method = $this->settings_lib->item('auth.user_activation_method');

                // No activation method
                if ($activation_method == 0) {
                    // Activate the user automatically
                    $data['active'] = 1;
                }

                if ($user_id = $this->user_model->insert($data)) {
                    // now add the meta is there is meta data
                    $this->user_model->save_meta_for($user_id, $meta_data);

                    /*
                     * USER ACTIVATIONS ENHANCEMENT
                     */

                    // Prepare user messaging vars
                    $subject = '';
                    $email_mess = '';
//                    $message = lang('us_email_thank_you');
                    $message = "User created successfully. ";
                    $type = 'success';
                    $site_title = $this->settings_lib->item('site.title');
                    $error = false;

                    switch ($activation_method) {
                        case 0:
                            // No activation required. Activate the user and send confirmation email
                            $subject = str_replace('[SITE_TITLE]', $this->settings_lib->item('site.title'), lang('us_account_reg_complete'));
                            $email_mess = $this->load->view('_emails/activated', array('title' => $site_title, 'link' => site_url()), true);
                            //$message .= lang('us_account_active_login');
                            break;
                        case 1:
                            // Email Activiation.
                            // Create the link to activate membership
                            // Run the account deactivate to assure everything is set correctly
                            $activation_code = $this->user_model->deactivate($user_id);
                            $activate_link = site_url('activate/' . $user_id);
                            $subject = lang('us_email_subj_activate');

                            $email_message_data = array(
                                'title' => $site_title,
                                'code' => $activation_code,
                                'link' => $activate_link
                            );
                            $email_mess = $this->load->view('_emails/activate', $email_message_data, true);
                            //$message .= lang('us_check_activate_email');
                            break;
                        case 2:
                            // Admin Activation
                            // Clear hash but leave user inactive
                            $subject = lang('us_email_subj_pending');
                            $email_mess = $this->load->view('_emails/pending', array('title' => $site_title), true);
                            //$message .= lang('us_admin_approval_pending');
                            break;
                    }//end switch
                    // Now send the email
                    $this->load->library('emailer/emailer');
                    $data = array(
                        'to' => $_POST['email'],
                        'subject' => $subject,
                        'message' => $email_mess
                    );

//                    if (!$this->emailer->send($data)) {
//                        $message .= lang('us_err_no_email') . $this->emailer->error;
//                        $error = true;
//                    }

                    if ($error) {
                        $type = 'error';
                    } else {
                        $type = 'success';
                    }

                    Template::set_message($message, $type);

                    // Log the Activity

                    log_activity($user_id, lang('us_log_register'), 'users');
                    redirect(SITE_AREA . '/content/companies/stuff_list');
                } else {
                    Template::set_message(lang('us_registration_fail'), 'error');
                    redirect(SITE_AREA . '/content/companies/register');
                }//end if
            }//end if
        }//end if

        $settings = $this->settings_lib->find_all();
        if ($settings['auth.password_show_labels'] == 1) {
            Assets::add_module_js('users', 'password_strength.js');
            Assets::add_module_js('users', 'jquery.strength.js');
            Assets::add_js($this->load->view('users_js', array('settings' => $settings), true), 'inline');
        }

        // Generate password hint messages.
        $this->user_model->password_hints();

        Template::set('languages', unserialize($this->settings_lib->item('site.languages')));
        if ($this->auth->user()->role_id == 9) {
            $branches = $this->branches_model->find_all();
        } else {
            $branches = $this->branches_model->where('id', $this->auth->user()->branch_id)->find_all();
        }
        Template::set('branches', $branches);
        if ($this->auth->user()->role_id == 9) {
            $counters = $this->counters_model->find_all();
        } else {
            $counters = $this->counters_model->where('branch_id', $this->auth->user()->branch_id)->find_all();
        }
        Template::set('counters', $counters);
        Template::set_view('companies/content/users/register');
        Template::set('page_title', 'Register');
        Template::render();
    }

    public function changePassword() {
        if (isset($_POST['cng_pass'])) {
            $this->form_validation->set_rules('password', 'lang:bf_password', 'required|max_length[8]|valid_password');
            $this->form_validation->set_rules('pass_confirm', 'lang:bf_password_confirm', 'required|matches[password]');

            if ($this->form_validation->run() !== FALSE) {
                $userpass = $this->input->post('password');
                $data = array(
                    'password' => $userpass
                );

                if ($this->user_model->update($this->auth->user()->id, $data)) {
                    Template::set_message('Password changed successfully', 'success');
                    redirect(SITE_AREA . '/content/companies/profile');
                } else {
                    Template::set_message('Unable to change password', 'error');
                    redirect(SITE_AREA . '/content/companies/changePassword');
                }
            } else {
                Template::set_message('Unable to change password', 'error');
                redirect(SITE_AREA . '/content/companies/changePassword');
            }
        }
        Template::set_view('companies/content/change_password');
        Template::set('page_title', 'Change Password');
        Template::render();
    }

    public function user_edit($user_id = NULL) {
        $id = $this->uri->segment(5);
        if (empty($id)) {
            Template::set_message(lang('companies_invalid_id'), 'error');
            redirect(SITE_AREA . '/content/companies/index');
        }

        if (isset($_POST['save'])) {
//            $this->auth->restrict('Companies.Content.Edit');

            if ($this->save_users_info('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message("User update Successfully", 'success');
                redirect(SITE_AREA . '/content/companies/index');
            } else {
                Template::set_message("Error occure while updating", 'error');
            }
        }
        if ($this->auth->user()->role_id == 9) {
            $branches = $this->branches_model->find_all();
        } else {
            $branches = $this->branches_model->where('id', $this->auth->user()->branch_id)->find_all();
        }
        Template::set('branches', $branches);
        if ($this->auth->user()->role_id == 9) {
            $counters = $this->counters_model->find_all();
        } else {
            $counters = $this->counters_model->where('branch_id', $this->auth->user()->branch_id)->find_all();
        }
        Template::set('counters', $counters);
        Template::set('companies', $this->user_model->find($id));
        Template::set('toolbar_title', lang('companies_edit') . ' Users');
        Template::set_view('companies/content/edit_user');
        Template::render();
    }

//end register()
    //--------------------------------------------------------------------

    private function save_user($id = 0, $meta_fields = array()) {

        if ($id == 0) {
            $id = $this->current_user->id; /* ( $this->input->post('id') > 0 ) ? $this->input->post('id') :  */
        }

        $_POST['id'] = $id;

// Simple check to make the posted id is equal to the current user's id, minor security check
        if ($_POST['id'] != $this->current_user->id) {
            $this->form_validation->set_message('email', 'lang:us_invalid_userid');
            return FALSE;
        }

// Setting the payload for Events system.
        $payload = array('user_id' => $id, 'data' => $this->input->post());


        $this->form_validation->set_rules('email', 'lang:bf_email', 'required|trim|valid_email|max_length[120]|unique[users.email,users.id]');
        $this->form_validation->set_rules('password', 'lang:bf_password', 'max_length[120]|valid_password');

        // check if a value has been entered for the password - if so then the pass_confirm is required
        // if you don't set it as "required" the pass_confirm field could be left blank and the form validation would still pass
        $extra_rules = !empty($_POST['password']) ? 'required|' : '';
        $this->form_validation->set_rules('pass_confirm', 'lang:bf_password_confirm', '' . $extra_rules . 'matches[password]');

        $username_required = '';
        if ($this->settings_lib->item('auth.login_type') == 'username' ||
                $this->settings_lib->item('auth.use_usernames')) {
            $username_required = 'required|';
        }
        $this->form_validation->set_rules('username', 'lang:bf_username', $username . 'required|trim|max_length[30]|unique[users.username,users.id]');

        $this->form_validation->set_rules('language', 'lang:bf_language', 'required|trim');
        $this->form_validation->set_rules('timezones', 'lang:bf_timezone', 'required|trim|max_length[4]');
        $this->form_validation->set_rules('display_name', 'lang:bf_display_name', 'trim|max_length[255]');

        // Added Event "before_user_validation" to run before the form validation
        Events::trigger('before_user_validation', $payload);


        foreach ($meta_fields as $field) {
            if ((!isset($field['admin_only']) || $field['admin_only'] === FALSE || (isset($field['admin_only']) && $field['admin_only'] === TRUE && isset($this->current_user) && $this->current_user->role_id == 1)) && (!isset($field['frontend']) || $field['frontend'] === TRUE)) {
                $this->form_validation->set_rules($field['name'], $field['label'], $field['rules']);
            }
        }


        if ($this->form_validation->run() === FALSE) {
            return FALSE;
        }

        // Compile our core user elements to save.
        $data = array(
            'email' => $this->input->post('email'),
            'language' => $this->input->post('language'),
            'timezone' => $this->input->post('timezones'),
        );

        // If empty, the password will be left unchanged.
        if ($this->input->post('password') !== '') {
            $data['password'] = $this->input->post('password');
        }

        if ($this->input->post('display_name') !== '') {
            $data['display_name'] = $this->input->post('display_name');
        }

        if (isset($_POST['username'])) {
            $data['username'] = $this->input->post('username');
        }

        // Any modules needing to save data?
        // Event to run after saving a user
        Events::trigger('save_user', $payload);

        return $this->user_model->update($id, $data);
    }

//end save_user()

    /**
     * Creates a Companies object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Companies.Content.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_companies()) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message(lang('companies_create_success'), 'success');
                redirect(SITE_AREA . '/content/companies');
            } else {
                Template::set_message(lang('companies_create_failure') . $this->companies_model->error, 'error');
            }
        }
        Assets::add_module_js('companies', 'companies.js');

        Template::set('toolbar_title', lang('companies_create') . ' Companies');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Companies data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('companies_invalid_id'), 'error');
            redirect(SITE_AREA . '/content/companies');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Companies.Content.Edit');

            if ($this->save_companies('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message(lang('companies_edit_success'), 'success');
            } else {
                Template::set_message(lang('companies_edit_failure') . $this->companies_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Companies.Content.Delete');

            if ($this->companies_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message(lang('companies_delete_success'), 'success');

                redirect(SITE_AREA . '/content/companies');
            } else {
                Template::set_message(lang('companies_delete_failure') . $this->companies_model->error, 'error');
            }
        }
        Template::set('companies', $this->companies_model->find($id));
        Template::set('toolbar_title', lang('companies_edit') . ' Companies');
        Template::render();
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_companies($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['name'] = $this->input->post('companies_name');
        $data['email'] = $this->input->post('companies_email');
        $data['admin_id'] = $this->input->post('companies_admin_id');
        $data['is_master'] = $this->input->post('companies_is_master');
        $data['payment_account'] = $this->input->post('companies_payment_account');
        $data['status'] = $this->input->post('companies_status');
        $data['created_on'] = $this->input->post('companies_created_on') ? $this->input->post('companies_created_on') : '0000-00-00 00:00:00';
        $data ['modified_on'] = $this->input->post('companies_modified_on') ? $this->input->post('companies_modified_on') : '0000-00-00 00:00:00';
        $data['deleted'] = $this->input->
                post('companies_deleted');

        if ($type == 'insert') {
            $id = $this->companies_model->insert($data);
            if (is_numeric
                            ($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->companies_model->update($id, $data);
        } return $return;
    }

    private function save_users_info($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        $data = array();
        $data['email'] = $this->input->post('user_email');
        $data['display_name'] = $this->input->post('user_display_name');
        $data['username'] = $this->input->post('user_name');
        $data['counter_id'] = $this->input->post('counter');
        $data['branch_id'] = $this->input->post('user_branch_id');
        $data['role_id'] = $this->input->post('user_role_id');

        $return = $this->user_model->update($id, $data);
        return $return;
    }

    public function change_user_role() {
        $btn_id = $this->input->post('btn_id');
        $stat = $this->input->post('stat');
        $cond = array('role_id' => $stat);
        $res = $this->user_model->update_where('id', $btn_id, $cond);
        if ($res) {
            echo json_encode(array('Data' => TRUE));
            exit();
        } else {
            echo json_encode(array('Data' => FALSE));
            exit();
        }
    }

//--------------------------------------------------------------------
}
