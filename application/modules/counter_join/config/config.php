<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['module_config'] = array(
    'description' => 'Allow to access counter_join',
    'name' => 'Counter_join',
    'version' => '0.0.1',
    'author' => 'admin'
);
