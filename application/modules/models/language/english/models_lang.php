<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['models_manage']			= 'Manage Models';
$lang['models_edit']				= 'Edit';
$lang['models_true']				= 'True';
$lang['models_false']				= 'False';
$lang['models_create']			= 'Create';
$lang['models_list']				= 'List';
$lang['models_new']				= 'New';
$lang['models_edit_text']			= 'Edit this to suit your needs';
$lang['models_no_records']		= 'There aren\'t any models in the system.';
$lang['models_create_new']		= 'Create a new Models.';
$lang['models_create_success']	= 'Models successfully created.';
$lang['models_create_failure']	= 'There was a problem creating the models: ';
$lang['models_create_new_button']	= 'Create New Models';
$lang['models_invalid_id']		= 'Invalid Models ID.';
$lang['models_edit_success']		= 'Models successfully saved.';
$lang['models_edit_failure']		= 'There was a problem saving the models: ';
$lang['models_delete_success']	= 'record(s) successfully deleted.';
$lang['models_delete_failure']	= 'We could not delete the record: ';
$lang['models_delete_error']		= 'You have not selected any records to delete.';
$lang['models_actions']			= 'Actions';
$lang['models_cancel']			= 'Cancel';
$lang['models_delete_record']		= 'Delete this Models';
$lang['models_delete_confirm']	= 'Are you sure you want to delete this models?';
$lang['models_edit_heading']		= 'Edit Models';

// Create/Edit Buttons
$lang['models_action_edit']		= 'Save Models';
$lang['models_action_create']		= 'Create Models';

// Activities
$lang['models_act_create_record']	= 'Created record with ID';
$lang['models_act_edit_record']	= 'Updated record with ID';
$lang['models_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['models_column_created']	= 'Created';
$lang['models_column_deleted']	= 'Deleted';
$lang['models_column_modified']	= 'Modified';
