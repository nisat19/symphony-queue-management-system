<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * content controller
 */
class content extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Models.Content.View');
        $this->load->model('models_model', null, true);
        $this->lang->load('models');

        Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
        Assets::add_js('jquery-ui-1.8.13.min.js');
        Assets::add_css('jquery-ui-timepicker.css');
        Assets::add_js('jquery-ui-timepicker-addon.js');
        Template::set_block('sub_nav', 'content/_sub_nav');

        Assets::add_module_js('models', 'models.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index() {

        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->models_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('models_delete_success'), 'success');
                } else {
                    Template::set_message(lang('models_delete_failure') . $this->models_model->error, 'error');
                }
            }
        }

        $records = $this->models_model->find_all();

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Models');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Models object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Models.Content.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_models()) {
                // Log the activity
                log_activity($this->current_user->id, lang('models_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'models');

                Template::set_message(lang('models_create_success'), 'success');
                redirect(SITE_AREA . '/content/models');
            } else {
                Template::set_message(lang('models_create_failure') . $this->models_model->error, 'error');
            }
        }
        Assets::add_module_js('models', 'models.js');

        Template::set('toolbar_title', lang('models_create') . ' Models');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Models data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('models_invalid_id'), 'error');
            redirect(SITE_AREA . '/content/models');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Models.Content.Edit');

            if ($this->save_models('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('models_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'models');

                Template::set_message(lang('models_edit_success'), 'success');
                redirect(SITE_AREA . '/content/models');
            } else {
                Template::set_message(lang('models_edit_failure') . $this->models_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Models.Content.Delete');

            if ($this->models_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('models_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'models');

                Template::set_message(lang('models_delete_success'), 'success');

                redirect(SITE_AREA . '/content/models');
            } else {
                Template::set_message(lang('models_delete_failure') . $this->models_model->error, 'error');
            }
        }
        Template::set('models', $this->models_model->find($id));
        Template::set('toolbar_title', lang('models_edit') . ' Models');
        Template::render();
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_models($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['name'] = $this->input->post('models_name');
        $data['description'] = $this->input->post('models_description');

        if ($type == 'insert') {
            $id = $this->models_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->models_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
