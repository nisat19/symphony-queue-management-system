<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/content/models') ?>" id="list"><?php echo lang('models_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Models.Content.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/content/models/create') ?>" id="create_new"><?php echo lang('models_new'); ?></a>
	</li>
	<?php endif; ?>
</ul>