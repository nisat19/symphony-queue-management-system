<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($models)) {
    $models = (array) $models;
}
$id = isset($models['id']) ? $models['id'] : '';
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Models</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>

        <div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
            <?php echo form_label('Name' . lang('bf_form_label_required'), 'models_name', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='models_name' type='text' name='models_name' maxlength="255" value="<?php echo set_value('models_name', isset($models['name']) ? $models['name'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('name'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('description') ? 'error' : ''; ?>">
            <?php echo form_label('Description' . lang('bf_form_label_required'), 'models_description', array('class' => 'control-label')); ?>
            <div class='controls'>
                <?php echo form_textarea(array('name' => 'models_description', 'id' => 'models_description', 'rows' => '5', 'cols' => '80', 'value' => set_value('models_description', isset($models['description']) ? $models['description'] : ''))); ?>
                <span class='help-inline'><?php echo form_error('description'); ?></span>
            </div>
        </div>

        <div class="form-actions">
            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('models_action_create'); ?>"  />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/content/models', lang('models_cancel'), 'class="btn btn-warning"'); ?>

        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>