<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * mainsite controller
 */
class mainsite extends Front_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->lang->load('mainsite');
        $this->load->model('counters/counters_model');
        $this->load->model('ticket/ticket_model');
        $this->load->model('ticket_meta/ticket_meta_model');
        $this->load->model('users/user_model');

        Assets::add_module_css('mainsite', 'style.css');
        Assets::add_module_js('mainsite', 'mainsite.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index($branchId = 100000) {
        $result1_array = array();
        $result2_array = array();

        $no_of_counter = $this->counters_model->where('status', 'serving')->find_all_by('branch_id', $branchId);
        if ($no_of_counter != FALSE) {
            foreach ($no_of_counter as $no) {
                $res1 = $this->ticket_model
                        ->where('status', 'serving')
                        ->where('DATE(bf_ticket.created_on) = date(NOW())')
                        ->order_by('id', 'desc')
                        ->limit(1)
                        ->find_by('counter_id', $no->id);
                if ($res1) {
                    $result1_array[] = $res1->id;
                } else {
                    $result1_array[] = 'n/a';
                }
//            $cond2 = array('seen' => 1);
//            $this->ticket_meta_model->update_where('counter_id', $no->id, $cond2);

                $res2 = $this->ticket_model
                        ->where('status', 'pending')
                        ->where('DATE(bf_ticket.created_on) = date(NOW())')
                        ->limit(10)
                        ->find_all_by('counter_id', $no->id);
                if ($res2) {
                    $result2_array[] = $res2;
                } else {
                    $result2_array[] = 'n/a';
                }
//            $cond3 = array('seen' => 1);
//            $this->ticket_model->update_where('counter_id', $no->id, $cond3);
            }
            $no_of_counter = $no_of_counter;
        } else {
            $no_of_counter = 0;
        }

        Template::set('no_of_counter', $no_of_counter);
        Template::set('result1_array', $result1_array);
        Template::set('result2_array', $result2_array);
        Template::render();
    }

    public function ticket() {
        Template::set_view('mainsite/ticket');
        Template::render();
    }

    public function checkTicket() {
        $res2 = $this->ticket_model->order_by('id', 'desc')->limit(1)->find_all_by('seen', 0);
        echo json_encode($res2);
        exit();
    }

    public function changeStatus($id) {
        $cond3 = array('seen' => 1);
        if ($this->ticket_model->update_where('id', $id, $cond3))
            echo TRUE;
        exit();
    }

    //--------------------------------------------------------------------
}
