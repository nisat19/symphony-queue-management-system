<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['mainsite_manage']			= 'Manage mainsite';
$lang['mainsite_edit']				= 'Edit';
$lang['mainsite_true']				= 'True';
$lang['mainsite_false']				= 'False';
$lang['mainsite_create']			= 'Create';
$lang['mainsite_list']				= 'List';
$lang['mainsite_new']				= 'New';
$lang['mainsite_edit_text']			= 'Edit this to suit your needs';
$lang['mainsite_no_records']		= 'There aren\'t any mainsite in the system.';
$lang['mainsite_create_new']		= 'Create a new mainsite.';
$lang['mainsite_create_success']	= 'mainsite successfully created.';
$lang['mainsite_create_failure']	= 'There was a problem creating the mainsite: ';
$lang['mainsite_create_new_button']	= 'Create New mainsite';
$lang['mainsite_invalid_id']		= 'Invalid mainsite ID.';
$lang['mainsite_edit_success']		= 'mainsite successfully saved.';
$lang['mainsite_edit_failure']		= 'There was a problem saving the mainsite: ';
$lang['mainsite_delete_success']	= 'record(s) successfully deleted.';
$lang['mainsite_delete_failure']	= 'We could not delete the record: ';
$lang['mainsite_delete_error']		= 'You have not selected any records to delete.';
$lang['mainsite_actions']			= 'Actions';
$lang['mainsite_cancel']			= 'Cancel';
$lang['mainsite_delete_record']		= 'Delete this mainsite';
$lang['mainsite_delete_confirm']	= 'Are you sure you want to delete this mainsite?';
$lang['mainsite_edit_heading']		= 'Edit mainsite';

// Create/Edit Buttons
$lang['mainsite_action_edit']		= 'Save mainsite';
$lang['mainsite_action_create']		= 'Create mainsite';

// Activities
$lang['mainsite_act_create_record']	= 'Created record with ID';
$lang['mainsite_act_edit_record']	= 'Updated record with ID';
$lang['mainsite_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['mainsite_column_created']	= 'Created';
$lang['mainsite_column_deleted']	= 'Deleted';
$lang['mainsite_column_modified']	= 'Modified';
