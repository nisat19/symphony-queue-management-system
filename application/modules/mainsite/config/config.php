<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['module_config'] = array(
    'description' => 'User can see this page without login',
    'name' => 'mainsite',
    'version' => '0.0.1',
    'author' => 'admin'
);
