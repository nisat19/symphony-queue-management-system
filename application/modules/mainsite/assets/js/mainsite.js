$(document).ready(function() {
    $("#save_submit").click(function() {
        var formData = $(".ticket_form").serialize();
        //console.log(formData);
        $.ajax({
            type: "POST",
            url: base_url + "api/getTicket/1",
            dataType: "json",
            data: formData,
            success: function(msg) {
                console.log(msg.success);
                if (msg.success == true) {
                    $('.alert-success').show("slow");
                    $('.alert-error').hide("slow");
                } else {
                    $('.alert-success').hide("slow");
                    $('.alert-error').show("slow");
                }
            }
        });
        return false;
    });

    function randomBetween(min, max) {
        if (min < 0) {
            return min + Math.random() * (Math.abs(min) + max);
        } else {
            return min + Math.random() * max;
        }
    }

    setInterval(function() {
//        $.getJSON(base_url + 'mainsite/checkTicket', function(data) {
//            var tdValue = '';
//            var len = data.length;
//            if (len > 0) {
//                for (var i = 0; i < len; i++) {
//                    if (data[i].status === 'pending') {
//                        $("#checkTable tbody tr[id=" + data[i].counter_id + "]" + ' td:nth-child(3)').each(function() {
//                            tdValue = $(this).text();
//                            $(this).text(tdValue + ',' + data[i].id);
//                            $.getJSON(base_url + 'mainsite/changeStatus/' + data[i].id, function(data) {
//                                console.log("Success");
//                            });
//                        });
//                    } else if (data[i].status === 'serving') {
//                        $("#checkTable tbody tr[id=" + data[i].counter_id + "]" + ' td:nth-child(2)').each(function() {
//                            $(this).text(data[i].id);
//                        });
//                        $("#checkTable tbody tr[id=" + data[i].counter_id + "]" + ' td:nth-child(3)').each(function() {
//                            tdValue = $(this).text().replace(data[i].id + ',', '');
//                            $(this).text(tdValue);
//                        });
//                    }
//
//                }
//            } else {
//                $("#checkTable tbody tr td:nth-child(2)").each(function() {
//                    $(this).text("n/a");
//                });
//            }
//        });
        window.location.reload();
    }, randomBetween(10000, 15000));
});