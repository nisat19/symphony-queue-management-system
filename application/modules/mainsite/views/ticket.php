<div class="admin-box">
    <h3>Ticket</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal ticket_form"'); ?>
    <fieldset>

        <div class="control-group">
            <label class="control-label">LSO</label>
            <div class='controls'>
                <input id='lso' type='text' name='lso' maxlength="100" value="" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Customer Name</label>
            <div class='controls'>
                <input id='cname' type='text' name='cname' maxlength="100" value="" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Customer Address</label>
            <div class='controls'>
                <input id='caddress' type='text' name='caddress' maxlength="100" value="" />
            </div>
        </div>
        <?php
        // Change the values in this array to populate your dropdown as required
        $options = array(
            0 => "Select",
            1 => "Handset Servicing",
            2 => "Accessories Sales",
            3 => "Handset Delivery",
            4 => "Quick Solution",
            5 => "Information",
        );

        echo form_dropdown('category_id', $options, set_value('category_id', isset($counters['category_id']) ? $counters['category_id'] : ''), 'Category');
        ?>
        <?php
        // Change the values in this array to populate your dropdown as required
        $options = array(
            0 => "Select",
            1 => "Model One",
            2 => "Model Two",
            3 => "Model Three",
            4 => "Model Four",
            5 => "Model Five",
        );

        echo form_dropdown('model_id', $options, set_value('model_id', isset($counters['model_id']) ? $counters['model_id'] : ''), 'Model');
        ?>

        <input type="hidden" id="branch_id" name="branch_id" value="1" />
        <div class="alert alert-success" style="display: none">
            <h3>Saved Successfully</h3>
        </div>
        <div class="alert alert-error" style="display: none">
            <h3>Error occur while creating.</h3>
        </div>
        <div class="form-actions">
            <input type="submit" id="save_submit" name="save_submit" class="btn btn-primary" value="<?php echo "Submit" ?>"  />
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>