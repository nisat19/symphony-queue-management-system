<style type="text/css">
    h2{
        font-size: 24px;
        margin: 0px;
    }
    .page-header{
        margin: 0px;
    }
</style>
<div class="header1"></div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span1"></div>
        <div class="span10">
            <div class="container-fluid m-login-container">
                <div class="page-header">
                    <center><h1 style="color:#008daf">Queue Information</h1></center>
                </div>
                <div>
                    <table class="table table-bordered table-striped table-condensed" id="checkTable">
                        <thead>
                            <tr>
                                <th>Counter No</th>
                                <th style="text-align: center;">Currently servicing</th>
                                <th>Next in queue</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            if ($no_of_counter != 0) {
                                foreach ($no_of_counter as $value) {
                                    ?>
                                    <tr id="<?php echo $value->id ?>">
                                        <td><?php echo $value->title; ?></td>
                                        <td style="text-align: center;"><?php echo $result1_array[$i] != 'n/a' ? $result1_array[$i] : 'n/a'; ?></td>
                                        <td>
                                            <?php
                                            $res = array();
                                            if ($result2_array[$i] != 'n/a') {
                                                foreach ($result2_array[$i] as $val) {
                                                    $res[] = $val->id;
                                                }
                                                echo implode(", ", $res);
                                            } else {
                                                echo 'n/a';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="3" style="text-align: center;">Service currently unavailable!</td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                </center>
            </div>
        </div>
        <div class="span4"></div>
    </div>
</div>
<div class="foo" style="margin-top: -100px;"></div>