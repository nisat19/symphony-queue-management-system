<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/reports/ticket_meta') ?>" id="list"><?php echo lang('ticket_meta_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Ticket_Meta.Reports.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/reports/ticket_meta/create') ?>" id="create_new"><?php echo lang('ticket_meta_new'); ?></a>
	</li>
	<?php endif; ?>
</ul>