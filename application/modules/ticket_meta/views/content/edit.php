<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($ticket_meta))
{
	$ticket_meta = (array) $ticket_meta;
}
$id = isset($ticket_meta['id']) ? $ticket_meta['id'] : '';

?>
<div class="admin-box">
	<h3>Ticket Meta</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('user_id') ? 'error' : ''; ?>">
				<?php echo form_label('User Id'. lang('bf_form_label_required'), 'ticket_meta_user_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ticket_meta_user_id' type='text' name='ticket_meta_user_id' maxlength="11" value="<?php echo set_value('ticket_meta_user_id', isset($ticket_meta['user_id']) ? $ticket_meta['user_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('user_id'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('ticket_meta_ticket_id', $options, set_value('ticket_meta_ticket_id', isset($ticket_meta['ticket_id']) ? $ticket_meta['ticket_id'] : ''), 'Ticket Id'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('ticket_meta_counter_id', $options, set_value('ticket_meta_counter_id', isset($ticket_meta['counter_id']) ? $ticket_meta['counter_id'] : ''), 'Counter Id'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('ticket_meta_branch_id', $options, set_value('ticket_meta_branch_id', isset($ticket_meta['branch_id']) ? $ticket_meta['branch_id'] : ''), 'Branch Id'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					5 => 5,
				);

				echo form_dropdown('ticket_meta_status', $options, set_value('ticket_meta_status', isset($ticket_meta['status']) ? $ticket_meta['status'] : ''), 'Status'. lang('bf_form_label_required'));
			?>

			<div class="control-group <?php echo form_error('created_on') ? 'error' : ''; ?>">
				<?php echo form_label('Created On', 'ticket_meta_created_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ticket_meta_created_on' type='text' name='ticket_meta_created_on' maxlength="1" value="<?php echo set_value('ticket_meta_created_on', isset($ticket_meta['created_on']) ? $ticket_meta['created_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('created_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('modified_on') ? 'error' : ''; ?>">
				<?php echo form_label('Modified On', 'ticket_meta_modified_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ticket_meta_modified_on' type='text' name='ticket_meta_modified_on' maxlength="1" value="<?php echo set_value('ticket_meta_modified_on', isset($ticket_meta['modified_on']) ? $ticket_meta['modified_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('modified_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('finished_on') ? 'error' : ''; ?>">
				<?php echo form_label('Finished On', 'ticket_meta_finished_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ticket_meta_finished_on' type='text' name='ticket_meta_finished_on' maxlength="1" value="<?php echo set_value('ticket_meta_finished_on', isset($ticket_meta['finished_on']) ? $ticket_meta['finished_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('finished_on'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('ticket_meta_action_edit'); ?>"  />
				<?php echo lang('bf_or'); ?>
				<?php echo anchor(SITE_AREA .'/content/ticket_meta', lang('ticket_meta_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Ticket_Meta.Content.Delete')) : ?>
				or
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('ticket_meta_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('ticket_meta_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>