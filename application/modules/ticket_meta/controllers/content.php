<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * content controller
 */
class content extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Ticket_Meta.Content.View');
		$this->load->model('ticket_meta_model', null, true);
		$this->lang->load('ticket_meta');
		
			Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
			Assets::add_js('jquery-ui-1.8.13.min.js');
			Assets::add_css('jquery-ui-timepicker.css');
			Assets::add_js('jquery-ui-timepicker-addon.js');
		Template::set_block('sub_nav', 'content/_sub_nav');

		Assets::add_module_js('ticket_meta', 'ticket_meta.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	 */
	public function index()
	{

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->ticket_meta_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('ticket_meta_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('ticket_meta_delete_failure') . $this->ticket_meta_model->error, 'error');
				}
			}
		}

		$records = $this->ticket_meta_model->find_all();

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Ticket Meta');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Creates a Ticket Meta object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Ticket_Meta.Content.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_ticket_meta())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('ticket_meta_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'ticket_meta');

				Template::set_message(lang('ticket_meta_create_success'), 'success');
				redirect(SITE_AREA .'/content/ticket_meta');
			}
			else
			{
				Template::set_message(lang('ticket_meta_create_failure') . $this->ticket_meta_model->error, 'error');
			}
		}
		Assets::add_module_js('ticket_meta', 'ticket_meta.js');

		Template::set('toolbar_title', lang('ticket_meta_create') . ' Ticket Meta');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Ticket Meta data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('ticket_meta_invalid_id'), 'error');
			redirect(SITE_AREA .'/content/ticket_meta');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Ticket_Meta.Content.Edit');

			if ($this->save_ticket_meta('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('ticket_meta_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'ticket_meta');

				Template::set_message(lang('ticket_meta_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('ticket_meta_edit_failure') . $this->ticket_meta_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Ticket_Meta.Content.Delete');

			if ($this->ticket_meta_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('ticket_meta_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'ticket_meta');

				Template::set_message(lang('ticket_meta_delete_success'), 'success');

				redirect(SITE_AREA .'/content/ticket_meta');
			}
			else
			{
				Template::set_message(lang('ticket_meta_delete_failure') . $this->ticket_meta_model->error, 'error');
			}
		}
		Template::set('ticket_meta', $this->ticket_meta_model->find($id));
		Template::set('toolbar_title', lang('ticket_meta_edit') .' Ticket Meta');
		Template::render();
	}

	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_ticket_meta($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['user_id']        = $this->input->post('ticket_meta_user_id');
		$data['ticket_id']        = $this->input->post('ticket_meta_ticket_id');
		$data['counter_id']        = $this->input->post('ticket_meta_counter_id');
		$data['branch_id']        = $this->input->post('ticket_meta_branch_id');
		$data['status']        = $this->input->post('ticket_meta_status');
		$data['created_on']        = $this->input->post('ticket_meta_created_on');
		$data['modified_on']        = $this->input->post('ticket_meta_modified_on') ? $this->input->post('ticket_meta_modified_on') : '0000-00-00 00:00:00';
		$data['finished_on']        = $this->input->post('ticket_meta_finished_on') ? $this->input->post('ticket_meta_finished_on') : '0000-00-00 00:00:00';

		if ($type == 'insert')
		{
			$id = $this->ticket_meta_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->ticket_meta_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}