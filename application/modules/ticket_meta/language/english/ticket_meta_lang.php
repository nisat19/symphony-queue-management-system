<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['ticket_meta_manage']			= 'Manage Ticket Meta';
$lang['ticket_meta_edit']				= 'Edit';
$lang['ticket_meta_true']				= 'True';
$lang['ticket_meta_false']				= 'False';
$lang['ticket_meta_create']			= 'Create';
$lang['ticket_meta_list']				= 'List';
$lang['ticket_meta_new']				= 'New';
$lang['ticket_meta_edit_text']			= 'Edit this to suit your needs';
$lang['ticket_meta_no_records']		= 'There aren\'t any ticket_meta in the system.';
$lang['ticket_meta_create_new']		= 'Create a new Ticket Meta.';
$lang['ticket_meta_create_success']	= 'Ticket Meta successfully created.';
$lang['ticket_meta_create_failure']	= 'There was a problem creating the ticket_meta: ';
$lang['ticket_meta_create_new_button']	= 'Create New Ticket Meta';
$lang['ticket_meta_invalid_id']		= 'Invalid Ticket Meta ID.';
$lang['ticket_meta_edit_success']		= 'Ticket Meta successfully saved.';
$lang['ticket_meta_edit_failure']		= 'There was a problem saving the ticket_meta: ';
$lang['ticket_meta_delete_success']	= 'record(s) successfully deleted.';
$lang['ticket_meta_delete_failure']	= 'We could not delete the record: ';
$lang['ticket_meta_delete_error']		= 'You have not selected any records to delete.';
$lang['ticket_meta_actions']			= 'Actions';
$lang['ticket_meta_cancel']			= 'Cancel';
$lang['ticket_meta_delete_record']		= 'Delete this Ticket Meta';
$lang['ticket_meta_delete_confirm']	= 'Are you sure you want to delete this ticket_meta?';
$lang['ticket_meta_edit_heading']		= 'Edit Ticket Meta';

// Create/Edit Buttons
$lang['ticket_meta_action_edit']		= 'Save Ticket Meta';
$lang['ticket_meta_action_create']		= 'Create Ticket Meta';

// Activities
$lang['ticket_meta_act_create_record']	= 'Created record with ID';
$lang['ticket_meta_act_edit_record']	= 'Updated record with ID';
$lang['ticket_meta_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['ticket_meta_column_created']	= 'Created';
$lang['ticket_meta_column_deleted']	= 'Deleted';
$lang['ticket_meta_column_modified']	= 'Modified';
