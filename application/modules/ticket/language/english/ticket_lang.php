<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['ticket_manage']			= 'Manage Ticket';
$lang['ticket_edit']				= 'Edit';
$lang['ticket_true']				= 'True';
$lang['ticket_false']				= 'False';
$lang['ticket_create']			= 'Create';
$lang['ticket_list']				= 'List';
$lang['ticket_new']				= 'New';
$lang['ticket_edit_text']			= 'Edit this to suit your needs';
$lang['ticket_no_records']		= 'There aren\'t any ticket in the system.';
$lang['ticket_create_new']		= 'Create a new Ticket.';
$lang['ticket_create_success']	= 'Ticket successfully created.';
$lang['ticket_create_failure']	= 'There was a problem creating the ticket: ';
$lang['ticket_create_new_button']	= 'Create New Ticket';
$lang['ticket_invalid_id']		= 'Invalid Ticket ID.';
$lang['ticket_edit_success']		= 'Ticket successfully saved.';
$lang['ticket_edit_failure']		= 'There was a problem saving the ticket: ';
$lang['ticket_delete_success']	= 'record(s) successfully deleted.';
$lang['ticket_delete_failure']	= 'We could not delete the record: ';
$lang['ticket_delete_error']		= 'You have not selected any records to delete.';
$lang['ticket_actions']			= 'Actions';
$lang['ticket_cancel']			= 'Cancel';
$lang['ticket_delete_record']		= 'Delete this Ticket';
$lang['ticket_delete_confirm']	= 'Are you sure you want to delete this ticket?';
$lang['ticket_edit_heading']		= 'Edit Ticket';

// Create/Edit Buttons
$lang['ticket_action_edit']		= 'Save Ticket';
$lang['ticket_action_create']		= 'Create Ticket';

// Activities
$lang['ticket_act_create_record']	= 'Created record with ID';
$lang['ticket_act_edit_record']	= 'Updated record with ID';
$lang['ticket_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['ticket_column_created']	= 'Created';
$lang['ticket_column_deleted']	= 'Deleted';
$lang['ticket_column_modified']	= 'Modified';
