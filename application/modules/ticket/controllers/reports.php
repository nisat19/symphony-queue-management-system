<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * reports controller
 */
class reports extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Ticket.Reports.View');
        $this->load->model('ticket_model', null, true);
        $this->lang->load('ticket');

        Assets::clear_cache();
        Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
        Assets::add_css('jquery.dataTables');
        Assets::add_js('jquery.dataTables.min');
        Assets::add_js('DT_bootstrap');
        Assets::add_js('dataTablePlugins');
        Assets::add_js('jquery-ui-1.8.13.min.js');
        Assets::add_css('jquery-ui-timepicker.css');
        Assets::add_js('jquery-ui-timepicker-addon.js');
        Template::set_block('sub_nav', 'reports/_sub_nav');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index() {

        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->ticket_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('ticket_delete_success'), 'success');
                } else {
                    Template::set_message(lang('ticket_delete_failure') . $this->ticket_model->error, 'error');
                }
            }
        }

        Assets::clear_cache();
        Assets::add_module_js('ticket', 'ticket_report.js');
        $records = $this->ticket_model->find_all();
        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Ticket');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Ticket object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Ticket.Reports.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_ticket()) {
                // Log the activity
                log_activity($this->current_user->id, lang('ticket_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'ticket');

                Template::set_message(lang('ticket_create_success'), 'success');
                redirect(SITE_AREA . '/reports/ticket');
            } else {
                Template::set_message(lang('ticket_create_failure') . $this->ticket_model->error, 'error');
            }
        }
        Assets::clear_cache();
        Assets::add_module_js('ticket', 'ticket.js');

        Template::set('toolbar_title', lang('ticket_create') . ' Ticket');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Ticket data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('ticket_invalid_id'), 'error');
            redirect(SITE_AREA . '/reports/ticket');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Ticket.Reports.Edit');

            if ($this->save_ticket('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('ticket_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'ticket');

                Template::set_message(lang('ticket_edit_success'), 'success');
            } else {
                Template::set_message(lang('ticket_edit_failure') . $this->ticket_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Ticket.Reports.Delete');

            if ($this->ticket_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('ticket_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'ticket');

                Template::set_message(lang('ticket_delete_success'), 'success');

                redirect(SITE_AREA . '/reports/ticket');
            } else {
                Template::set_message(lang('ticket_delete_failure') . $this->ticket_model->error, 'error');
            }
        }
        Template::set('ticket', $this->ticket_model->find($id));
        Template::set('toolbar_title', lang('ticket_edit') . ' Ticket');
        Template::render();
    }

    public function serviceTypeList() {

        Assets::clear_cache();
        Assets::add_module_js('ticket', 'service_type.js');
        $records = $this->ticket_model->find_all();
        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Ticket');
        Template::set_view('ticket/reports/service_type');
        Template::render();
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_ticket($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['lso'] = $this->input->post('ticket_lso');
        $data['customer_name'] = $this->input->post('ticket_customer_name');
        $data['customer_address'] = $this->input->post('ticket_customer_address');
        $data['model_id'] = $this->input->post('ticket_model_id');
        $data['service_type'] = $this->input->post('ticket_service_type');
        $data['counter_id'] = $this->input->post('ticket_counter_id');
        $data['branch_id'] = $this->input->post('ticket_branch_id');
        $data['category_id'] = $this->input->post('ticket_category_id');
        $data['created_on'] = $this->input->post('ticket_created_on');
        $data['modified_on'] = $this->input->post('ticket_modified_on') ? $this->input->post('ticket_modified_on') : '0000-00-00 00:00:00';

        if ($type == 'insert') {
            $id = $this->ticket_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->ticket_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
    //Get Average time
    public function service_type() {
        $sql = "SELECT
                bf_ticket.id as id,
                bf_ticket.lso,
                bf_ticket.customer_name,
                bf_ticket.customer_address,
                bf_ticket.model_id,
                bf_models.name as model_name,
                bf_ticket.service_type,
                bf_ticket.counter_id,
                bf_counter.title as counter_name,
                bf_ticket.branch_id,
                bf_branches.name as branche_name,
                bf_ticket.status as status,
                bf_ticket.category_id,
                bf_ticket.comments,
                bf_categories.name as category_name,
                bf_ticket.created_on,
                ROUND(AVG(
			TIME_TO_SEC(
				TIMEDIFF(
					bf_ticket.finished_on,
					bf_ticket.modified_on
				)
			)
		),
		1
                ) AS avg_time

                FROM bf_ticket
                LEFT JOIN bf_models ON bf_models.id = bf_ticket.category_id
                INNER JOIN bf_counter ON bf_counter.id = bf_ticket.counter_id
                INNER JOIN bf_branches ON bf_branches.id = bf_ticket.branch_id
                INNER JOIN bf_categories ON bf_categories.id = bf_ticket.category_id ";
        if ($this->auth->user()->role_id == 7) {
            $sql .="WHERE bf_ticket.counter_id = {$this->auth->user()->counter_id} ";
        }
        $sql .= "GROUP BY bf_categories.id,
                bf_counter.id
                ORDER BY counter_id, category_id";
        $records = $this->db
                ->query($sql)
                ->result();
//        var_dump($records);
//        exit();
        try {
            $sl = 0;
            for ($index = 0; $index < count($records); $index++) {
                $record = $records[$index];
                $record->sl = ++$sl;
                if (strtolower($record->status) == 'pending') {
                    $record->status_s = "<strong style='color:#B94A48'>" . strtoupper($record->status) . "</strong>";
                } elseif (strtolower($record->status) == 'serving') {
                    $record->status_s = "<strong style='color:#3A87AD'>" . strtoupper($record->status) . "</strong>";
                } else {
                    $record->status_s = "<strong style='color:#468847'>" . strtoupper($record->status) . "</strong>";
                }
                $record->created_on = date("F j, Y, g:i a", strtotime($record->created_on));
                $record->avg_time = $record->avg_time != "" ? $record->avg_time : '00:00:00';
//                $avg_time = explode('.', $record->avg_time);
                $record->avg_time = $record->avg_time . " sec";
                $record->actionLink = '';
                if (strtolower($record->status) == 'pending' && $this->auth->user()->role_id == 7) {
                    $record->actionLink .= '<a class="upd_serving btn btn-primary" id="' . $record->id . '">Serve</a>';
                }
                if (strtolower($record->status) == 'serving' && $this->auth->user()->role_id == 7) {
                    $record->actionLink .= '<a class="upd_finished btn btn-info" id="' . $record->id . '">Finish</a>';
                }
                $record->actionLink .= '<div class="btn-group pull-right">
                                <a class="btn dropdown-toggle" data-toggle="dropdown">
                                    Action
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" style="min-width: 100px;">
                                    <li>
                                        <a class="delete_ticket" id="' . $record->id . '">Delete</a>
                                    </li>';
                if ($this->auth->user()->role_id == 8) {
                    $record->actionLink .= '<li>
                                        <a class="btn_transfer" id="' . $record->id . '">Transfer</a>
                                    </li>';
                }
                $record->actionLink .= '</ul>
                            </div>';

//                var_dump($record);
//                exit;
                $records[$index] = $record;
            }
            echo json_encode(array('aaData' => $records));
            exit();
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
            exit();
        }
    }

    public function export_2_format($param = 'Excel') {
        $sql = "SELECT
                bf_ticket.id as id,
                bf_ticket.lso,
                bf_ticket.customer_name,
                bf_ticket.customer_address,
                bf_ticket.model_id,
                bf_models.name as model_name,
                bf_ticket.service_type,
                bf_ticket.counter_id,
                bf_counter.title as counter_name,
                bf_ticket.branch_id,
                bf_branches.name as branche_name,
                bf_ticket.status as status,
                bf_ticket.category_id,
                bf_ticket.comments,
                bf_categories.name as category_name,
                bf_ticket.created_on,
                #TIME(TIMEDIFF(bf_ticket.finished_on, bf_ticket.modified_on)) as time_diff,
                #TIME(AVG(TIMEDIFF(bf_ticket.finished_on, bf_ticket.modified_on))) as avg_time
                ROUND(AVG(
			TIME_TO_SEC(
				TIMEDIFF(
					bf_ticket.finished_on,
					bf_ticket.modified_on
				)
			)
		),
		1
                ) AS avg_time
                FROM bf_ticket
                LEFT JOIN bf_models ON bf_models.id = bf_ticket.category_id
                INNER JOIN bf_counter ON bf_counter.id = bf_ticket.counter_id
                INNER JOIN bf_branches ON bf_branches.id = bf_ticket.branch_id
                INNER JOIN bf_categories ON bf_categories.id = bf_ticket.category_id ";
        if ($this->auth->user()->role_id == 7) {
            $sql .="WHERE bf_ticket.counter_id = {$this->auth->user()->counter_id} AND bf_ticket.branch_id = {$this->auth->user()->branch_id} ";
        }
        if ($this->auth->user()->role_id == 8) {
            $sql .="WHERE bf_ticket.branch_id = {$this->auth->user()->branch_id} ";
        }
//        $sql .="WHERE DATE(bf_ticket.created_on) = DATE(NOW()) ";

        $sql .= "GROUP BY bf_categories.id,
                bf_counter.id
                ORDER BY counter_id, category_id";
        try {
            $records = $this->db->query($sql)->result();
            //Load our new PHPExcel library
            $this->load->library('excel');
            //Activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //Name the worksheet
            $this->excel->getActiveSheet()->setTitle('Test Report');
            //Add Column Heading from Column A to E
            $this->excel->getActiveSheet()->setCellValue('A1', 'SL');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Counter');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Average Time');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Category');
            $this->excel->getActiveSheet()->setCellValue('E1', 'Branch');
//            $this->excel->getActiveSheet()->setCellValue('F1', 'Created On');
            //Add Column Heading from Column A to E
            //Add Data to Cells
            $count = count($records);
            $r = 2;
            $total = 0;
            $sl = 0;
            for ($i = 0; $i < $count; $i++) {
                $this->excel->getActiveSheet()->setCellValue("A$r", ++$sl);

                $avgtime = $records[$i]->avg_time != "" ? $records[$i]->avg_time : '00:00:00';
//                $avg_time = explode('.', $avgtime);
                $resavg = $avgtime . " sec";

                $this->excel->getActiveSheet()->setCellValue("B$r", $records[$i]->counter_name);
                $this->excel->getActiveSheet()->setCellValue("C$r", $resavg);
                $this->excel->getActiveSheet()->setCellValue("D$r", $records[$i]->category_name);
                $this->excel->getActiveSheet()->setCellValue("E$r", $records[$i]->branche_name);
//                $this->excel->getActiveSheet()->setCellValue("F$r", date('M-d-Y h:i:s A', strtotime($records[$i]->created_on)));
//                $total = $total + $records[$i]->total_price;
                $r++;
//                if ($r > $count) {
//                    $this->excel->getActiveSheet()->setCellValue("D$r", 'Total Amount');
//                    $this->excel->getActiveSheet()->setCellValue("E$r", $total);
//                }
            }
            //Add Data to Cells
            //Change the font size
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(20);
//            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(20);
            //Make the font become bold
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
//            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            //Merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //Set aligment to center for cells (A1 to E1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//            $this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $datetime = uniqid();
            $filename = "Report_$datetime.xls"; //Save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //Tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //If you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//            $filename = "Report_$datetime.pdf"; //Save our workbook as this file name
//            header('Content-Type: application/pdf');
//            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
            //Force user to download the Excel file without writing it to server's HD
            //$objWriter->save('php://output');
            $this->config->load('download');
            $path = $this->config->item('dload_dir');
            $objWriter->save($path . $filename);
            echo base_url('downloads/' . $filename);
            exit;
        } catch (PHPExcel_Exception $e) {
            echo $e->getTraceAsString();
            exit;
        }
    }

    public function export_ticket($param = 'Excel') {
        try {
            $records = $this->ticket_model->getAllTicketInfo()->result();
            //Load our new PHPExcel library
            $this->load->library('excel');
            //Activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //Name the worksheet
            $this->excel->getActiveSheet()->setTitle('Test Report');
            //Add Column Heading from Column A to E
            $this->excel->getActiveSheet()->setCellValue('A1', 'Ticket No');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Customer Name');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Customer Address');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Category');
            $this->excel->getActiveSheet()->setCellValue('F1', 'Branch');
            $this->excel->getActiveSheet()->setCellValue('G1', 'Counter');
            $this->excel->getActiveSheet()->setCellValue('H1', 'Status');
            $this->excel->getActiveSheet()->setCellValue('I1', 'Created On');
            //Add Column Heading from Column A to E
            //Add Data to Cells
            $count = count($records);
            $r = 2;
            $total = 0;
            for ($i = 0; $i < $count; $i++) {
                $this->excel->getActiveSheet()->setCellValue("A$r", $records[$i]->id);
                $this->excel->getActiveSheet()->setCellValue("B$r", $records[$i]->customer_name);
                $this->excel->getActiveSheet()->setCellValue("C$r", $records[$i]->customer_address);
                $this->excel->getActiveSheet()->setCellValue("D$r", $records[$i]->category_name);
                $this->excel->getActiveSheet()->setCellValue("E$r", $records[$i]->branche_name);
                $this->excel->getActiveSheet()->setCellValue("F$r", $records[$i]->counter_name);
                $this->excel->getActiveSheet()->setCellValue("G$r", $records[$i]->status);
                $this->excel->getActiveSheet()->setCellValue("H$r", date('M-d-Y h:i:s A', strtotime($records[$i]->created_on)));
//                $total = $total + $records[$i]->total_price;
                $r++;
//                if ($r > $count) {
//                    $this->excel->getActiveSheet()->setCellValue("D$r", 'Total Amount');
//                    $this->excel->getActiveSheet()->setCellValue("E$r", $total);
//                }
            }
            //Add Data to Cells
            //Change the font size
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(16);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(16);
            //Make the font become bold
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(FALSE);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(FALSE);
            //Merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //Set aligment to center for cells (A1 to E1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $datetime = uniqid();
            $filename = "Report_$datetime.xls"; //Save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //Tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //Save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //If you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//            $filename = "Report_$datetime.pdf"; //Save our workbook as this file name
//            header('Content-Type: application/pdf');
//            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
            //Force user to download the Excel file without writing it to server's HD
            //$objWriter->save('php://output');
            $this->config->load('download');
            $path = $this->config->item('dload_dir');
            $objWriter->save($path . $filename);
            echo base_url('downloads/' . $filename);
            exit;
        } catch (PHPExcel_Exception $e) {
            echo $e->getTraceAsString();
            exit;
        }
    }

}
