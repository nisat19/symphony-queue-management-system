<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * content controller
 */
class content extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Ticket.Content.View');
        $this->load->model('ticket_model', null, true);
        $this->load->model('branches/branches_model', null, true);
        $this->load->model('ticket_meta/ticket_meta_model', null, true);
        $this->load->model('counters/counters_model', null, true);
        $this->load->model('user_timelog/user_timelog_model', null, true);
        $this->lang->load('ticket');

        Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
        Assets::add_js('jquery-ui-1.8.13.min.js');
        Assets::add_css('jquery-ui-timepicker.css');
        Assets::add_js('jquery-ui-timepicker-addon.js');
        Template::set_block('sub_nav', 'content/_sub_nav');

        Assets::add_module_js('ticket', 'ticket.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index() {
        $counter_status = $this->db
                        ->query("SELECT bf_counter.id, bf_counter.`status`
                        FROM bf_counter
                        LEFT JOIN bf_users ON bf_users.counter_id = bf_counter.id
                        WHERE bf_counter.branch_id = {$this->auth->user()->branch_id} "
                                . "AND bf_users.id = {$this->auth->user()->id}")->row();

        Template::set('counter_status', $counter_status);
        $servicing = $this->ticket_model
                ->where('status', 'pending')
                ->where('DATE(bf_ticket.created_on) = date(NOW())')
                ->where('counter_id', $this->auth->user()->counter_id)
                ->find_all_by('category_id', 1);
        if ($servicing) {
            $serv = count($servicing);
        } else {
            $serv = 0;
        }
        $accSales = $this->ticket_model
                ->where('status', 'pending')
                ->where('DATE(bf_ticket.created_on) = date(NOW())')
                ->where('counter_id', $this->auth->user()->counter_id)
                ->find_all_by('category_id', 2);
        if ($accSales) {
            $sales = count($accSales);
        } else {
            $sales = 0;
        }
        $handDelivery = $this->ticket_model
                ->where('status', 'pending')
                ->where('DATE(bf_ticket.created_on) = date(NOW())')
                ->where('counter_id', $this->auth->user()->counter_id)
                ->find_all_by('category_id', 3);
        if ($handDelivery) {
            $hdel = count($handDelivery);
        } else {
            $hdel = 0;
        }
        $Qsolution = $this->ticket_model
                ->where('status', 'pending')
                ->where('DATE(bf_ticket.created_on) = date(NOW())')
                ->where('counter_id', $this->auth->user()->counter_id)
                ->find_all_by('category_id', 4);
        if ($Qsolution) {
            $qsol = count($Qsolution);
        } else {
            $qsol = 0;
        }
        $information = $this->ticket_model
                ->where('status', 'pending')
                ->where('DATE(bf_ticket.created_on) = date(NOW())')
                ->where('counter_id', $this->auth->user()->counter_id)
                ->find_all_by('category_id', 5);
        if ($information) {
            $info = count($information);
        } else {
            $info = 0;
        }
        Template::set('serv', $serv);
        Template::set('sales', $sales);
        Template::set('hdel', $hdel);
        Template::set('qsol', $qsol);
        Template::set('info', $info);

        $served_customer = $this->ticket_model
                ->where('DATE(bf_ticket.created_on) = date(NOW())')
                ->where('counter_id', $this->auth->user()->counter_id)
                ->find_all_by('status', 'finished');
        if ($served_customer) {
            $scus = count($served_customer);
        } else {
            $scus = 0;
        }
        Template::set('served_customer', $scus);

        $avg_value = $this->db
                ->query("SELECT ROUND(AVG(TIME_TO_SEC(TIMEDIFF(finished_on, modified_on))),2) as avg_time "
                        . "FROM bf_ticket WHERE DATE(created_on) = DATE(NOW()) AND `status` = 'finished' "
                        . "AND counter_id = '{$this->auth->user()->counter_id}'")
                ->row();

        if ($avg_value) {
            $avg_time = $avg_value->avg_time != "" ? $avg_value->avg_time . " sec" : "0.00 sec";
        } else {
            $avg_time = '0.00 sec';
        }
        Template::set('avg_time', $avg_time);

        $currently_serv = $this->db
                        ->query("SELECT
                    bf_ticket.id, bf_ticket.customer_name,
                    TIME(TIMEDIFF(bf_ticket.modified_on, bf_ticket.created_on)) AS w_time,
                    bf_categories.`name` AS category_name,
                    bf_models.`name` AS model_name
                    FROM bf_ticket
                    INNER JOIN bf_categories ON
                            bf_categories.id = bf_ticket.category_id
                    INNER JOIN bf_models ON
                            bf_models.id = bf_ticket.model_id
                    WHERE
                            DATE(bf_ticket.created_on) = DATE(NOW()) AND
                            bf_ticket.`status` = 'serving' AND
                            counter_id = '{$this->auth->user()->counter_id}'
                    ORDER BY bf_ticket.id DESC
                    LIMIT 1")->row();
        if ($currently_serv) {
            $curr_time = $currently_serv;
        } else {
            $curr_time = '00:00:00';
        }
        Template::set('curr_time', $curr_time);

//        $usrData = $this->user_model->find_by('id', $this->auth->user()->id);
        if ($this->auth->user()->role_id != 9 && $this->auth->user()->role_id != 1) {
            $branch_target = $this->branches_model->find_by('id', $this->auth->user()->branch_id)->target;
        } else {
            $branch_target = 70;
        }
        Template::set('branch_target', $branch_target);

        if ($this->auth->user()->role_id == 9) {
            $Allcounters = $this->counters_model->find_all();
        } else {
            $Allcounters = $this->counters_model->where('branch_id', $this->auth->user()->branch_id)->find_all();
        }
        Template::set('Allcounters', $Allcounters);

        Assets::add_css('jquery.dataTables');
        Assets::add_js('jquery.dataTables.min');
        Assets::add_js('DT_bootstrap');
        Assets::add_js('dataTablePlugins');
        $records = $this->ticket_model->find_all();
        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Ticket');
        Template::render();
    }

    public function change_status_tk() {
        $btn_id = $this->input->post('btn_id');
        $stat = $this->input->post('stat');
        $cond = array('status' => $stat, 'modified_on' => date('Y-m-d H:i:s', time()));
        $res = $this->counters_model->update_where('id', $btn_id, $cond);
        if ($res) {
            echo json_encode(array('Data' => TRUE));
            exit();
        } else {
            echo json_encode(array('Data' => FALSE));
            exit();
        }
    }

    public function ticketList() {
        $this->user_timelog_model->updateTimeLog();

        $records = $this->ticket_model->getAllTicketInfo()->result();
        try {
            $sl = 0;
            for ($index = 0; $index < count($records); $index++) {
                $record = $records[$index];
                $record->sl = ++$sl;
                if (strtolower($record->status) == 'pending') {
                    $record->status_s = "<strong style='color:#B94A48'>" . strtoupper($record->status) . "</strong>";
                } elseif (strtolower($record->status) == 'serving') {
                    $record->status_s = "<strong style='color:#3A87AD'>" . strtoupper($record->status) . "</strong>";
                } else {
                    $record->status_s = "<strong style='color:#468847'>" . strtoupper($record->status) . "</strong>";
                }
                $record->created_on = date("F j, Y, g:i a", strtotime($record->created_on));
                $record->actionLink = '';
                if (strtolower($record->status) == 'pending' && $this->auth->user()->role_id == 7) {
                    $record->actionLink .= '<a class="upd_serving btn btn-primary" id="' . $record->id . '">Serve</a>';
                }
                if (strtolower($record->status) == 'serving' && $this->auth->user()->role_id == 7) {
                    $record->actionLink .= '<a class="upd_finished btn btn-info" id="' . $record->id . '">Finish</a>';
                }
                if ($record->status != "finished" && $this->auth->user()->role_id == 8) {
                    $record->actionLink .= '<div class="btn-group pull-right">
                                <a class="btn dropdown-toggle" data-toggle="dropdown">
                                    Action
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" style="min-width: 100px;">';

                    $record->actionLink .='<li>
                                        <a class="delete_ticket" id="' . $record->id . '">Delete</a>
                                    </li>';
                    if ($record->status != "serving") {
                        $record->actionLink .= '<li>
                                        <a class="btn_transfer" id="' . $record->id . '">Transfer</a>
                                    </li>';
                    }
                    $record->actionLink .= '</ul>
                            </div>';
                }
                $records[$index] = $record;
            }
            echo json_encode(array('aaData' => $records));
            exit();
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
            exit();
        }
    }

    public function upd_serving($id) {
        $records = $this->ticket_model->find_by('id', $id);
        $cond = array('seen' => 0);
        $this->ticket_model->update($id, $cond);
        $data = array(
            'user_id' => $this->auth->user()->id,
            'ticket_id' => $id,
            'counter_id' => $records->counter_id,
            'branch_id' => $records->branch_id,
            'status' => 'progress',
            'seen' => 0,
            'progressed_on' => date('Y-m-d H:i:s', time())
        );
        $ins_id = $this->ticket_meta_model->insert($data);

        $cond2 = array('status' => 'serving', 'modified_on' => date("Y-m-d H:i:s", time()));
        $upd_id = $this->ticket_model->update($id, $cond2);
        if ($upd_id && $ins_id)
            echo TRUE;
        else
            echo FALSE;
    }

    public function tk_waiting_time() {
        $tdid = $this->input->post('tdid');
        $avg_value = $this->db
                ->query("SELECT TIMEDIFF(modified_on, created_on) as avg_time "
                        . "FROM bf_ticket WHERE status = 'serving' AND id = {$tdid}")
                ->row();
        if ($avg_value) {
            //$avg_time = gmdate("H:i:s", $avg_value->avg_time);
            $avg_time = $avg_value->avg_time;
        } else {
            $avg_time = 0;
        }
        echo json_encode(array('Data' => TRUE, 'avg_time' => $avg_time));
        exit();
    }

    public function upd_finished($id) {
        $records = $this->ticket_model->find_by('id', $id);
        $data = array(
            'user_id' => $this->auth->user()->id,
            'ticket_id' => $id,
            'counter_id' => $records->counter_id,
            'branch_id' => $records->branch_id,
            'status' => 'resolved',
            'finished_on' => date('Y-m-d H:i:s', time())
        );
        $ins_id = $this->ticket_meta_model->insert($data);

        $cond = array('status' => 'finished', 'finished_on' => date("Y-m-d H:i:s", time()));
        $upd_id = $this->ticket_model->update($id, $cond);
        if ($upd_id && $ins_id)
            echo TRUE;
        else
            echo FALSE;
    }

    public function deleteTicket($id) {
        $result = $this->ticket_model->delete($id);
        if ($result)
            echo TRUE;
        else
            echo FALSE;
    }

    public function upd_ticket_comnt() {
        $comments = $this->input->post('tkcomment');
        $id = $this->input->post('cmt_id');
        $cond = array('comments' => $comments);
        if ($this->ticket_model->update($id, $cond))
            echo json_encode(array('Data' => TRUE));
        else
            echo json_encode(array('Data' => FALSE));
    }

    public function upd_ticket_counter() {
        $tfcounter_id = $this->input->post('tfcounter_id');
        $trans_id = $this->input->post('trans_id');
        $priority_check = $this->input->post('priority_check');
        if ($priority_check == '1') {
            $counters = $this->ticket_model->checkPriority($tfcounter_id)->row();
            $cond = array('counter_id' => $tfcounter_id, 'priority' => $counters->mxp + 1);
        } else {
            $cond = array('counter_id' => $tfcounter_id, 'priority' => 0);
        }
        if ($this->ticket_model->update($trans_id, $cond))
            echo json_encode(array('Data' => TRUE));
        else
            echo json_encode(array('Data' => FALSE));
    }

    public function dashboard() {
        $delivery = $this->ticket_model->find_all_by('category_id', 3);
        if ($delivery) {
            $del = count($delivery);
        } else {
            $del = 0;
        }
        $set_prob = $this->ticket_model->find_all_by('category_id', 1);
        if ($set_prob) {
            $set = count($set_prob);
        } else {
            $set = 0;
        }
        $qeuery = $this->ticket_model->find_all_by('category_id', 5);
        if ($qeuery) {
            $que = count($qeuery);
        } else {
            $que = 0;
        }
        $solution = $this->ticket_model->find_all_by('category_id', 2);
        if ($solution) {
            $sol = count($solution);
        } else {
            $sol = 0;
        }
        $accessories = $this->ticket_model->find_all_by('category_id', 4);
        if ($accessories) {
            $acc = count($accessories);
        } else {
            $acc = 0;
        }
        Template::set('delivery', $del);
        Template::set('set_prob', $set);
        Template::set('qeuery', $que);
        Template::set('solution', $sol);
        Template::set('accessories', $acc);

        $served_customer = $this->ticket_model->find_all_by('status', 'finished');
        if ($served_customer) {
            $scus = count($served_customer);
        } else {
            $scus = 0;
        }
        Template::set('served_customer', $scus);

        $avg_value = $this->db
                        ->query("SELECT AVG(TIME_TO_SEC(TIMEDIFF(finished_on, progressed_on))) as avg_time FROM bf_ticket_meta WHERE user_id = '{$this->auth->user()->id}'")->row();
        if ($avg_value) {
            $avg_time = $avg_value->avg_time;
        } else {
            $avg_time = 0;
        }
        Template::set('avg_time', $avg_time);
        $usrData = $this->user_model->find_by('id', $this->auth->user()->id);
        $branch_target = $this->branches_model->find_by('id', $usrData->branch_id);
        Template::set('branch_target', $branch_target->target);

        Template::set_view('ticket/content/dashboard');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Ticket object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Ticket.Content.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_ticket()) {
                // Log the activity
                log_activity($this->current_user->id, lang('ticket_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'ticket');

                Template::set_message(lang('ticket_create_success'), 'success');
                redirect(SITE_AREA . '/content/ticket');
            } else {
                Template::set_message(lang('ticket_create_failure') . $this->ticket_model->error, 'error');
            }
        }
        Assets::add_module_js('ticket', 'ticket.js');

        Template::set('toolbar_title', lang('ticket_create') . ' Ticket');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Ticket data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('ticket_invalid_id'), 'error');
            redirect(SITE_AREA . '/content/ticket');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Ticket.Content.Edit');

            if ($this->save_ticket('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('ticket_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'ticket');

                Template::set_message(lang('ticket_edit_success'), 'success');
            } else {
                Template::set_message(lang('ticket_edit_failure') . $this->ticket_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Ticket.Content.Delete');

            if ($this->ticket_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('ticket_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'ticket');

                Template::set_message(lang('ticket_delete_success'), 'success');

                redirect(SITE_AREA . '/content/ticket');
            } else {
                Template::set_message(lang('ticket_delete_failure') . $this->ticket_model->error, 'error');
            }
        }
        Template::set('ticket', $this->ticket_model->find($id));
        Template::set('toolbar_title', lang('ticket_edit') . ' Ticket');
        Template::render();
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_ticket($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['lso'] = $this->input->post('ticket_lso');
        $data['customer_name'] = $this->input->post('ticket_customer_name');
        $data['customer_address'] = $this->input->post('ticket_customer_address');
        $data['model_id'] = $this->input->post('ticket_model_id');
        $data['service_type'] = $this->input->post('ticket_service_type');
        $data['counter_id'] = $this->input->post('ticket_counter_id');
        $data['branch_id'] = $this->input->post('ticket_branch_id');
        $data['category_id'] = $this->input->post('ticket_category_id');
        $data['created_on'] = $this->input->post('ticket_created_on');
        $data['modified_on'] = $this->input->post('ticket_modified_on') ? $this->input->post('ticket_modified_on') : '0000-00-00 00:00:00';

        if ($type == 'insert') {
            $id = $this->ticket_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->ticket_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
