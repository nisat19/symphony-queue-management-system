<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ticket_model extends BF_Model {

    protected $table_name = "bf_ticket";
    protected $key = "id";
    protected $soft_deletes = false;
    protected $date_format = "datetime";
    protected $log_user = FALSE;
    protected $set_created = false;
    protected $set_modified = FALSE;
    protected $created_field = "created_on";
    protected $modified_field = "modified_on";

    /*
      Customize the operations of the model without recreating the insert, update,
      etc methods by adding the method names to act as callbacks here.
     */
    protected $before_insert = array();
    protected $after_insert = array();
    protected $before_update = array();
    protected $after_update = array();
    protected $before_find = array();
    protected $after_find = array();
    protected $before_delete = array();
    protected $after_delete = array();

    /*
      For performance reasons, you may require your model to NOT return the
      id of the last inserted row as it is a bit of a slow method. This is
      primarily helpful when running big loops over data.
     */
    protected $return_insert_id = TRUE;
    // The default type of element data is returned as.
    protected $return_type = "object";
    // Items that are always removed from data arrays prior to
    // any inserts or updates.
    protected $protected_attributes = array();

    /*
      You may need to move certain rules (like required) into the
      $insert_validation_rules array and out of the standard validation array.
      That way it is only required during inserts, not updates which may only
      be updating a portion of the data.
     */
    protected $validation_rules = array(
        array(
            "field" => "ticket_lso",
            "label" => "Lso",
            "rules" => "required|trim|max_length[150]"
        ),
        array(
            "field" => "ticket_customer_name",
            "label" => "Customer Name",
            "rules" => "required|trim|max_length[50]"
        ),
        array(
            "field" => "ticket_customer_address",
            "label" => "Customer Address",
            "rules" => "required"
        ),
        array(
            "field" => "ticket_model_id",
            "label" => "Model Id",
            "rules" => "required|max_length[11]"
        ),
        array(
            "field" => "ticket_service_type",
            "label" => "Service Type",
            "rules" => "required|trim|max_length[20]"
        ),
        array(
            "field" => "ticket_counter_id",
            "label" => "Counter Id",
            "rules" => "required|max_length[11]"
        ),
        array(
            "field" => "ticket_branch_id",
            "label" => "Branch Id",
            "rules" => "required|max_length[11]"
        ),
        array(
            "field" => "ticket_category_id",
            "label" => "Category Id",
            "rules" => "required|max_length[11]"
        ),
    );
    protected $insert_validation_rules = array();
    protected $skip_validation = TRUE;

    //--------------------------------------------------------------------

    public function getAllTicketInfo() {
        $query = $this->db
                ->select('bf_ticket.id as id,
                          bf_ticket.lso,
                          bf_ticket.customer_name,
                          bf_ticket.customer_address,
                          bf_ticket.service_type,
                          bf_ticket.counter_id,
                          bf_counter.title as counter_name,
                          bf_ticket.branch_id,
                          bf_branches.name as branche_name,
                          bf_ticket.status as status,
                          bf_ticket.category_id,
                          bf_ticket.comments,
                          bf_categories.name as category_name,
                          bf_ticket.created_on')
                ->join('bf_counter', 'bf_counter.id = bf_ticket.counter_id', 'inner')
                ->join('bf_branches', 'bf_branches.id = bf_ticket.branch_id', 'inner')
                ->join('bf_categories', 'bf_categories.id = bf_ticket.category_id', 'left');
        if ($this->auth->user()->role_id == 7) {
            $query = $this->db
                    ->where('bf_ticket.counter_id', $this->auth->user()->counter_id);
        }
        if ($this->auth->user()->role_id == 7 || $this->auth->user()->role_id == 8) {
            $query = $this->db
                    ->where('bf_ticket.branch_id', $this->auth->user()->branch_id);
        }
        if ($this->auth->user()->role_id == 7) {
            $query = $this->db->where('DATE(bf_ticket.created_on) = DATE(NOW())');
        }
        $query = $this->db->order_by('bf_ticket.status', 'asc')->order_by('priority', 'desc')->get('bf_ticket');
        $result = $query;
//        print_r($this->db->last_query());
//        exit;
        return $result;
    }

    public function checkPriority($counter_id) {
        return $this->db->query("SELECT MAX(bf_ticket.priority) as mxp FROM bf_ticket WHERE bf_ticket.branch_id = '" . $this->auth->user()->branch_id . "' AND bf_ticket.counter_id = '" . $counter_id . "' AND bf_ticket.`status` = 'pending'");
    }

}
