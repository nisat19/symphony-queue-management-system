<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Dashboard</h3>
    <hr/>
    <div class="span12">
        <div class="row-fluid">
            <div class="span4 m-widget">
                <h4 style="font-size: 20px;">Waiting For</h4>
                <hr/>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th>Qty/Que</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Handset Delivery</td>
                            <td><?php echo $delivery; ?></td>
                        </tr>
                        <tr>
                            <td>Set Problem</td>
                            <td><?php echo $set_prob; ?></td>
                        </tr>
                        <tr>
                            <td>Qeuery</td>
                            <td><?php echo $qeuery; ?></td>
                        </tr>
                        <tr>
                            <td>Solution</td>
                            <td><?php echo $solution; ?></td>
                        </tr>
                        <tr>
                            <td>Accessories</td>
                            <td><?php echo $accessories; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="span8 m-widget">
                <div class="m-widget-header">
                    <h4 style="font-size: 20px;">Today's stats</h4>
                </div>
                <div class="m-widget-body">
                    <div class="row-fluid">
                        <a class="span4 m-stats-item">
                            <span class="m-stats-val">20</span>
                            Total Served Customers
                        </a>
                        <a class="span4 m-stats-item">
                            <span class="m-stats-val"><?php echo gmdate("H:i:s", $avg_time); ?></span>
                            Average Handle Time
                        </a>
                        <a class="span4 m-stats-item">
                            <span class="m-stats-val"><?php echo $branch_target; ?></span>
                            Target value
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="row-fluid">
            <div class="span4 m-widget">
                <div class="m-widget-header">
                    <h4 style="font-size: 20px;">Customer Information</h4>
                </div>
                <div class="m-widget-body">
                    <table class="table table-striped table-condensed">
                        <tbody>
                            <tr>
                                <td>Purpose</td>
                                <td>Set Deposit</td>
                            </tr>
                            <tr>
                                <td>Waiting Time</td>
                                <td>00:10:00</td>
                            </tr>
                            <tr>
                                <td>Servicing Time</td>
                                <td>00:10:00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="span8 m-widget">
                <div class="m-widget-header">
                    <h4 style="font-size: 20px;">Customer Information</h4>
                </div>
                <div class="m-widget-body">
                    <table class="table table-striped table-condensed">
                        <tbody>
                            <tr>
                                <td width="10%">SR No</td>
                                <td width="34%">Category</td>
                                <td width="22%">Waiting Time</td>
                                <td width="34%">Action</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>