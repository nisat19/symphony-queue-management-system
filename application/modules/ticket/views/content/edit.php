<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($ticket)) {
    $ticket = (array) $ticket;
}
$id = isset($ticket['id']) ? $ticket['id'] : '';
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Ticket</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>

        <div class="control-group <?php echo form_error('lso') ? 'error' : ''; ?>">
            <?php echo form_label('Lso' . lang('bf_form_label_required'), 'ticket_lso', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='ticket_lso' type='text' name='ticket_lso' maxlength="150" value="<?php echo set_value('ticket_lso', isset($ticket['lso']) ? $ticket['lso'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('lso'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('customer_name') ? 'error' : ''; ?>">
            <?php echo form_label('Customer Name' . lang('bf_form_label_required'), 'ticket_customer_name', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='ticket_customer_name' type='text' name='ticket_customer_name' maxlength="50" value="<?php echo set_value('ticket_customer_name', isset($ticket['customer_name']) ? $ticket['customer_name'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('customer_name'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('customer_address') ? 'error' : ''; ?>">
            <?php echo form_label('Customer Address' . lang('bf_form_label_required'), 'ticket_customer_address', array('class' => 'control-label')); ?>
            <div class='controls'>
                <?php echo form_textarea(array('name' => 'ticket_customer_address', 'id' => 'ticket_customer_address', 'rows' => '5', 'cols' => '80', 'value' => set_value('ticket_customer_address', isset($ticket['customer_address']) ? $ticket['customer_address'] : ''))); ?>
                <span class='help-inline'><?php echo form_error('customer_address'); ?></span>
            </div>
        </div>

        <?php
// Change the values in this array to populate your dropdown as required
        $options = array(
            11 => 11,
        );

        echo form_dropdown('ticket_model_id', $options, set_value('ticket_model_id', isset($ticket['model_id']) ? $ticket['model_id'] : ''), 'Model Id' . lang('bf_form_label_required'));
        ?>

        <div class="control-group <?php echo form_error('service_type') ? 'error' : ''; ?>">
            <?php echo form_label('Service Type' . lang('bf_form_label_required'), 'ticket_service_type', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='ticket_service_type' type='text' name='ticket_service_type' maxlength="20" value="<?php echo set_value('ticket_service_type', isset($ticket['service_type']) ? $ticket['service_type'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('service_type'); ?></span>
            </div>
        </div>

        <?php
// Change the values in this array to populate your dropdown as required
        $options = array(
            11 => 11,
        );

        echo form_dropdown('ticket_counter_id', $options, set_value('ticket_counter_id', isset($ticket['counter_id']) ? $ticket['counter_id'] : ''), 'Counter Id' . lang('bf_form_label_required'));
        ?>

        <?php
        // Change the values in this array to populate your dropdown as required
        $options = array(
            11 => 11,
        );

        echo form_dropdown('ticket_branch_id', $options, set_value('ticket_branch_id', isset($ticket['branch_id']) ? $ticket['branch_id'] : ''), 'Branch Id' . lang('bf_form_label_required'));
        ?>

        <?php
        // Change the values in this array to populate your dropdown as required
        $options = array(
            11 => 11,
        );

        echo form_dropdown('ticket_category_id', $options, set_value('ticket_category_id', isset($ticket['category_id']) ? $ticket['category_id'] : ''), 'Category Id' . lang('bf_form_label_required'));
        ?>

        <div class="control-group <?php echo form_error('created_on') ? 'error' : ''; ?>">
            <?php echo form_label('Created On', 'ticket_created_on', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='ticket_created_on' type='text' name='ticket_created_on' maxlength="1" value="<?php echo set_value('ticket_created_on', isset($ticket['created_on']) ? $ticket['created_on'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('created_on'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('modified_on') ? 'error' : ''; ?>">
            <?php echo form_label('Modified On', 'ticket_modified_on', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='ticket_modified_on' type='text' name='ticket_modified_on' maxlength="1" value="<?php echo set_value('ticket_modified_on', isset($ticket['modified_on']) ? $ticket['modified_on'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('modified_on'); ?></span>
            </div>
        </div>

        <div class="form-actions">
            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('ticket_action_edit'); ?>"  />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/content/ticket', lang('ticket_cancel'), 'class="btn btn-warning"'); ?>

            <?php if ($this->auth->has_permission('Ticket.Content.Delete')) : ?>
                or
                <button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('ticket_delete_confirm'))); ?>');
                        ">
                    <span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('ticket_delete_record'); ?>
                </button>
            <?php endif; ?>
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>