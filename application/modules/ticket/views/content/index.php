<?php
$num_columns = 11;
$can_delete = $this->auth->has_permission('Ticket.Content.Delete');
$can_edit = $this->auth->has_permission('Ticket.Content.Edit');
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Dashboard</h3>
    <hr/>
    <div class="span12">
        <?php if ($this->auth->user()->role_id == 7) { ?>
            <div class="row-fluid">
                <div class="span4 m-widget">
                    <a id="refresh_page" class="pull-right" href="javascript://"><i class="icon-refresh"></i></a>
                    <h4 style="font-size: 20px;">Waiting For</h4>
                    <hr/>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Qty/Que</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Handset Servicing</td>
                                <td><?php echo $serv; ?></td>
                            </tr>
                            <tr>
                                <td>Accessories Sales</td>
                                <td><?php echo $sales; ?></td>
                            </tr>
                            <tr>
                                <td>Handset Delivery</td>
                                <td><?php echo $hdel; ?></td>
                            </tr>
                            <tr>
                                <td>Quick Solution</td>
                                <td><?php echo $qsol; ?></td>
                            </tr>
                            <tr>
                                <td>Information</td>
                                <td><?php echo $info; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="span8">
                    <div class="m-widget">
                        <div class="span8">
                            <h4>Counter Status :
                                <span id="serv_id">
                                    <?php
                                    $act = '';
                                    if ($counter_status) {
                                        if (strtolower($counter_status->status) == 'not_serving') {
                                            $act = 'serving';
                                            $c_serving = 'Inactive';
                                        } else {
                                            $act = 'not_serving';
                                            $c_serving = 'Active';
                                        }
                                    } else {
                                        $act = 'serving';
                                        $c_serving = 'Inactive';
                                    }
                                    echo $c_serving;
                                    ?>

                                </span>
                            </h4>
                        </div>
                        <div class="span4">
                            <div class="make-switch pull-right" data-on="success" data-off="warning">
                                <a rel="<?php echo $act; ?>" id="<?php
                                if ($counter_status) {
                                    echo $counter_status->id;
                                }
                                ?>" class="btnserv btn btn-primary">Change</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="m-widget">
                        <div class="m-widget-header">
                            <h4 style="font-size: 20px;">Today's status</h4>
                        </div>
                        <div class="m-widget-body">
                            <div class="row-fluid">
                                <a class="span4 m-stats-item">
                                    <span class="m-stats-val"><?php echo $served_customer; ?></span>
                                    Total Served Customers
                                </a>
                                <a class="span4 m-stats-item">
                                    <span class="m-stats-val">
                                        <?php
                                        $res = array();
                                        if (isset($avg_time) && $avg_time != "") {
                                            //$res = explode('.', $avg_time);
                                            echo $avg_time;
                                        } else {
                                            echo '0.00 sec';
                                        }
                                        //gmdate("H:i:s", $avg_time);
                                        ?>
                                    </span>
                                    Average Handle Time
                                </a>
                                <a class="span4 m-stats-item">
                                    <span class="m-stats-val"><?php echo $branch_target; ?></span>
                                    Target value
                                </a>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="m-widget">
                        <div class="m-widget-header">
                            <h4 style="font-size: 20px;">Currently Serving</h4>
                        </div>
                        <div class="m-widget-body">
                            <table class="table table-striped table-condensed" id="current_serv">
                                <thead>
                                    <tr>
                                        <th width="10%">Ticket No</th>
                                        <th width="25%">Customer Name</th>
                                        <th width="20%">Phone Number</th>
                                        <th width="20%">Category</th>
                                        <th width="25%">Waiting Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo isset($curr_time->id) ? $curr_time->id : 'n/a' ?></td>
                                        <td><?php echo isset($curr_time->customer_name) ? $curr_time->customer_name : 'n/a' ?></td>
                                        <td><?php echo isset($curr_time->category_name) ? $curr_time->category_name : 'n/a' ?></td>
                                        <td><?php echo isset($curr_time->model_name) ? $curr_time->model_name : 'n/a' ?></td>
                                        <td><?php echo isset($curr_time->w_time) ? $curr_time->w_time : '00:00:00' ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div id="result"></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <br/>
        <div class="row-fluid">
            <?php
            echo form_open($this->uri->uri_string());
            ?>
            <table class="table table-striped table-bordered" id="ticketTable">
                <thead>
                    <tr>
                        <th>Ticket No</th>
                        <th>Lso</th>
                        <th>Customer Name</th>
                        <th>Phone Number</th>
                        <th>Category</th>
                        <th>Branch</th>
                        <th>Counter</th>
                        <th>Status</th>
                        <th>Comments</th>
                        <th>Created On</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="ticketModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Leave a Comment</h3>
    </div>
    <div class="modal-body">
        <textarea id="tkcomment" style="width: 98%;" rows="5" name="tkcomment"></textarea>
        <input type="hidden" id="tk_id" name="tk_id" value="" />
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary tkCommentBtn">Submit</button>
    </div>
</div>

<div id="confirmModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Choose a Counter</h3>
    </div>
    <div class="modal-body">
        Counter No &nbsp;&nbsp;&nbsp;
        <select id="tfcounter_id" name="tfcounter_id">
            <?php foreach ($Allcounters as $acount) { ?>
                <option value="<?php echo $acount->id; ?>"><?php echo $acount->title; ?></option>
            <?php } ?>
        </select>
        <br/>
        <br/>
        Priority &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="checkbox" value="" name="priority_check" id="priority_check" />
        <input type="hidden" id="trans_id" name="trans_id" value="" />
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary tkTransferBtn">Change Counter</button>
    </div>
</div>