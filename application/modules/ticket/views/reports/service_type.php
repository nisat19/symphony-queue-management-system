<?php
$num_columns = 11;
$can_delete = $this->auth->has_permission('Ticket.Reports.Delete');
$can_edit = $this->auth->has_permission('Ticket.Reports.Edit');
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Ticket (Average Time)</h3>
    <div class="clearfix" style="margin-bottom: 5px;">
        <!--        <div class="span6" style="margin-left: 0px;">
                    Range : <input type="text" id="datefrom" name="datefrom"/> to <input type="text" id="dateto" name="dateto"/>
                </div>-->
        <div>
            <div id="export_report" class="pull-right btn btn-warning">Export to Excel</div>
            <a href="" id="export_link" class="pull-right btn btn-success" style="display: none; margin-right: 5px;">Download</a>
        </div>
    </div>
    <?php echo form_open($this->uri->uri_string()); ?>
    <table class="table table-striped table-bordered" id="ticketService">
        <thead>
            <tr>
                <th>Ticket No</th>
                <th>Average Time</th>
                <th>Customer Name</th>
                <th>Phone Number</th>
                <th>Category</th>
                <th>Model</th>
                <th>Branch</th>
                <th>Counter</th>
                <th>Status</th>
                <th>Comments</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <?php echo form_close(); ?>
</div>