<?php
$num_columns = 11;
$can_delete = $this->auth->has_permission('Ticket.Reports.Delete');
$can_edit = $this->auth->has_permission('Ticket.Reports.Edit');
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Ticket</h3>
    <div class="clearfix" style="margin-bottom: 5px;">
        <div class="span6" style="margin-left: 0px;">
            Date Range : <input type="text" id="datefrom" name="datefrom"/> to <input type="text" id="dateto" name="dateto"/>
        </div>
        <div>
            <div id="export_ticket" class="pull-right btn btn-warning">Export to Excel</div>
            <a href="" id="export_link" class="pull-right btn btn-success" style="display: none; margin-right: 5px;">Download</a>
        </div>
        <div>
            <select class="pull-right" id="category_select" style="margin-right: 5px;">
                <option value="">All</option>
                <option value="Handset Servicing">Handset Servicing</option>
                <option value="Accessories Sales">Accessories Sales</option>
                <option value="Handset Delivery">Handset Delivery</option>
                <option value="Quick Solution">Quick Solution</option>
                <option value="Information">Information</option>
            </select>
            <h5 class="pull-right" style="margin-right: 10px;">Select Category: </h5>
        </div>
        <div>
            <select class="pull-right" id="month_select" style="margin-right: 10px;">
                <option value="">All</option>
                <option value="January">January</option>
                <option value="February">February</option>
                <option value="March">March</option>
                <option value="April">April</option>
                <option value="May">May</option>
                <option value="June">June</option>
                <option value="July">July</option>
                <option value="August">August</option>
                <option value="September">September</option>
                <option value="October">October</option>
                <option value="November">November</option>
                <option value="December">December</option>
            </select>
            <h5 class="pull-right" style="margin-right: 10px;">Select Month: </h5>
        </div>
    </div>
    <?php echo form_open($this->uri->uri_string()); ?>
    <table class="table table-striped table-bordered" id="ticketTable">
        <thead>
            <tr>
                <th>Ticket No</th>
                <th>LSO</th>
                <th>Customer Name</th>
                <th>Phone Number</th>
                <th>Category</th>
                <th>Model</th>
                <th>Branch</th>
                <th>Counter</th>
                <th>Status</th>
                <th>Comments</th>
                <th>Created On</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <?php echo form_close(); ?>
</div>