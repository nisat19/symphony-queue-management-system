<ul class="nav nav-pills">
    <li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
        <a href="<?php echo site_url(SITE_AREA . '/reports/ticket') ?>" id="list"><?php echo lang('ticket_list'); ?></a>
    </li>
    <li <?php echo $this->uri->segment(4) == 'serviceTypeList' ? 'class="active"' : '' ?>>
        <a href="<?php echo site_url(SITE_AREA . '/reports/ticket/serviceTypeList') ?>" id="list"><?php echo "Average Time"; ?></a>
    </li>
    <?php if ($this->auth->has_permission('Ticket.Reports.Create')) : ?>
        <li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
            <a href="<?php echo site_url(SITE_AREA . '/reports/ticket/create') ?>" id="create_new"><?php echo lang('ticket_new'); ?></a>
        </li>
    <?php endif; ?>
</ul>