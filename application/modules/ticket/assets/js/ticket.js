$(document).ready(function() {
    var ticketTable;
    $('#refresh_page').on("click", function() {
        location.reload();
    });

    $('.btnserv').on("click", function() {
        var btn_id = this.id;
        var stat = $(this).attr('rel');
        console.log(btn_id + " : " + stat);

        var data = 'btn_id=' + btn_id + '&stat=' + stat;
        $.ajax({
            type: "POST",
            url: base_url + "frontdesk/content/ticket/change_status_tk",
            dataType: "json",
            data: data,
            success: function(data) {
                if (data.Data == true) {
                    window.location.reload();
                }
                else {
                    console.log("Error");
                }
            }
        });

    });
    ticketTable = $('#ticketTable').dataTable({
        "bProcessing": true,
        "bDestroy": true,
        "bStateSave": true,
        "sAjaxSource": base_url + "frontdesk/content/ticket/ticketList",
        "aaSorting": [],
        "aoColumns": [
            {
                "mDataProp": "id"
            },
            {
                "mDataProp": "lso",
                "bVisible": false
            },
            {
                "mDataProp": "customer_name"
            },
            {
                "mDataProp": "customer_address"
            },
            {
                "mDataProp": "category_name"
            },
            {
                "mDataProp": "branche_name",
                "bVisible": false
            },
            {
                "mDataProp": "counter_name"
//                ,
//                "bVisible": false
            },
            {
                "mDataProp": "status_s"
            },
            {
                "mDataProp": "comments",
                "sWidth": "80px",
                "bVisible": false
            },
            {
                "mDataProp": "created_on",
                "sWidth": "80px",
                "bVisible": false
            },
            {
                "mDataProp": "actionLink",
                "sWidth": "130px"
            }
        ],
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sLengthMenu": "_MENU_ Per Page"
        }
    });
    var self;
//    $('#ticketTable').delegate('a.upd_serving', 'click', function(e) {
    $('#ticketTable').delegate('a.upd_serving', 'click', function(e) {
        self = $(this).closest('tr');
        var serv_id = this.id;
        var jsonresult = $.getJSON(base_url + "frontdesk/content/ticket/upd_serving/" + serv_id, function(data) {
            if (data) {
                ticketTable.fnReloadAjax();
                console.log("Success while updating");
            } else {
                console.log("Error while updating");
            }
        });
        $.when(jsonresult).done(function() {
//            $(this).parent().parent().trigger("click");
//            var tabledata = ticketTable.fnGetData(this);
            $("#current_serv tbody tr td:nth-child(1)").text(self[0].cells[0].innerHTML);
            $("#current_serv tbody tr td:nth-child(2)").text(self[0].cells[1].innerHTML);
            $("#current_serv tbody tr td:nth-child(3)").text(self[0].cells[2].innerHTML);
            $("#current_serv tbody tr td:nth-child(4)").text(self[0].cells[3].innerHTML);

            var data = 'tdid=' + self[0].cells[0].innerHTML;
            $.ajax({
                type: "POST",
                url: base_url + "frontdesk/content/ticket/tk_waiting_time",
                dataType: "json",
                data: data,
                success: function(data) {
                    if (data.Data == true) {
                        $("#current_serv tbody tr td:nth-child(5)").text(data.avg_time);
                    }
                    else
                        console.log("Error");
                }
            });
        });
    });

//    ticketTable.delegate('tr', 'click', function() {
//        var tabledata = ticketTable.fnGetData(this);
//        $("#current_serv tbody tr td:nth-child(1)").text(tabledata.id);
//        $("#current_serv tbody tr td:nth-child(2)").text(tabledata.customer_name);
//        $("#current_serv tbody tr td:nth-child(3)").text(tabledata.category_name);
//        $("#current_serv tbody tr td:nth-child(4)").text(tabledata.model_name);
//
//        var data = 'tdid=' + tabledata.id;
//        $.ajax({
//            type: "POST",
//            url: base_url + "frontdesk/content/ticket/tk_waiting_time",
//            dataType: "json",
//            data: data,
//            success: function(data) {
//                if (data.Data == true) {
//                    $("#current_serv tbody tr td:nth-child(5)").text(data.avg_time);
//                }
//                else
//                    console.log("Error");
//            }
//        });
//    });

    $('#ticketTable').delegate('a.upd_finished', 'click', function(e) {
        var finish_id = this.id;
        $.getJSON(base_url + "frontdesk/content/ticket/upd_finished/" + finish_id, function(data) {
            if (data) {
                $('#tk_id').val(finish_id);
                $('#tkcomment').val('');
                $('#ticketModal').modal('show');
                ticketTable.fnReloadAjax();
            } else {
                console.log("Error while updating");
            }
        });
    });

    $('#ticketTable').delegate('a.delete_ticket', 'click', function(e) {
        var del_id = this.id;
        if (confirm("Do you really want to delete this Ticket ?")) {
            e.stopPropagation();
            e.preventDefault();
            $.getJSON(base_url + "frontdesk/content/ticket/deleteTicket/" + del_id, function(data) {
                if (data) {
                    ticketTable.fnReloadAjax();
                } else {
                    console.log("Error while deleting");
                }
            });
        } else {
            e.stopPropagation();
        }
    });

    $('#ticketTable').delegate('a.btn_transfer', 'click', function(e) {
        var trans_id = this.id;
        console.log("Trans : " + trans_id);
        $('#trans_id').val(trans_id);
        $('#confirmModal').modal('show');
//        ticketTable.fnReloadAjax();
    });

    $('button.tkCommentBtn').on('click', function(e) {
        var cmt_id = $('#tk_id').val();
        var tkcomment = $('#tkcomment').val();
        var data = 'cmt_id=' + cmt_id + '&tkcomment=' + tkcomment;
        $.ajax({
            type: "POST",
            url: base_url + "frontdesk/content/ticket/upd_ticket_comnt",
            dataType: "json",
            data: data,
            success: function(data) {
                if (data.Data == true) {
                    $('#ticketModal').modal('hide');
//                    ticketTable.fnReloadAjax();
                    window.location.reload();
                }
                else
                    console.log("Error");
            }
        });
        return false;
    });

    $('button.tkTransferBtn').on('click', function(e) {
        var priority_check = false;
        var trans_id = $('#trans_id').val();
        var tfcounter_id = $('#tfcounter_id').val();
        if ($('#priority_check').prop('checked')) {
            priority_check = '1';
        } else {
            priority_check = '2';
        }
        var data = 'trans_id=' + trans_id + '&tfcounter_id=' + tfcounter_id + '&priority_check=' + priority_check;
        $.ajax({
            type: "POST",
            url: base_url + "frontdesk/content/ticket/upd_ticket_counter",
            dataType: "json",
            data: data,
            success: function(data) {
                if (data.Data == true) {
                    $('#confirmModal').modal('hide');
                    ticketTable.fnReloadAjax();
//                    window.location.reload();
                }
                else
                    console.log("Error");
            }
        });
        return false;
    });

    setInterval(function() {
        ticketTable.fnReloadAjax();
    }, 30000);
});