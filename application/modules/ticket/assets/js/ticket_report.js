$.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"];
            var iMin = $('#datefrom').val();
            var iMax = $('#dateto').val();
            var date = new Date(iMax);
            date.setDate(date.getDate() + 1);
            iMax = monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear();

            var iVersion = aData[10] === "" ? 0 : aData[10];
            if (iMin === "" && iMax === "")
            {
                return true;
            }
            else if (iMin === "" && iVersion < iMax)
            {
                return true;
            }
            else if (iMin < iVersion && "" === iMax)
            {
                return true;
            }
            else if (iMin < iVersion && iVersion < iMax)
            {
                return true;
            }
            return false;
        }
);

$(document).ready(function() {
    $('#month_select').focus();
    var ticketTable;
    var ticketService;
    $("#datefrom").datepicker(
            {changeYear: true,
                altFormat: "yy-mm-dd",
                dateFormat: "MM d, yy"}
    );

    $("#dateto").datepicker(
            {changeYear: true,
                altFormat: "yy-mm-dd",
                dateFormat: "MM d, yy"}
    );

    ticketTable = $('#ticketTable').dataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": base_url + "frontdesk/content/ticket/ticketList",
        "aoColumns": [
            {
                "mDataProp": "id"
            },
            {
                "mDataProp": "lso",
                "bVisible": false
            },
            {
                "mDataProp": "customer_name"
            },
            {
                "mDataProp": "customer_address"
            },
            {
                "mDataProp": "category_name"
            },
            {
                "mDataProp": "category_name",
                "bVisible": false
            },
            {
                "mDataProp": "branche_name"
            },
            {
                "mDataProp": "counter_name"
//                ,
//                "bVisible": false
            },
            {
                "mDataProp": "status_s"
            },
            {
                "mDataProp": "comments",
                "sWidth": "80px",
                "bVisible": false
            },
            {
                "mDataProp": "created_on",
                "sWidth": "80px",
                "bVisible": false
            }
        ],
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sLengthMenu": "_MENU_ Per Page",
            "sSearch": "Search all columns:"
        },
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "fnInitComplete": function() {
//            var self = this;
//            var nTrs = this.fnGetNodes();
//            $('td', nTrs).click(function() {
//                self.fnFilter(this.innerHTML);
//            });
        },
        "bJQueryUI": false
    });

    ticketService = $('#ticketService').dataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": base_url + "frontdesk/reports/ticket/service_type",
        "aoColumns": [
            {
                "mDataProp": "sl"
            },
            {
                "mDataProp": "avg_time"
            },
            {
                "mDataProp": "customer_name",
                "bVisible": false
            },
            {
                "mDataProp": "customer_address",
                "bVisible": false
            },
            {
                "mDataProp": "category_name"
            },
            {
                "mDataProp": "model_name",
                "bVisible": false
            },
            {
                "mDataProp": "branche_name"
            },
            {
                "mDataProp": "counter_name"
            },
            {
                "mDataProp": "status_s",
                "bVisible": false
            },
            {
                "mDataProp": "comments",
                "sWidth": "80px",
                "bVisible": false
            },
            {
                "mDataProp": "created_on",
                "sWidth": "80px",
                "bVisible": false
            }
        ],
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sLengthMenu": "_MENU_ Per Page",
            "sSearch": "Search all columns:"
        },
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "fnInitComplete": function() {
//            var self = this;
//            var nTrs = this.fnGetNodes();
//            $('td', nTrs).click(function() {
//                self.fnFilter(this.innerHTML);
//            });
        },
        "bJQueryUI": false
    });
//    $('#ticketTable_filter').hide();
    $('#category_select').on("change", function() {
        var value = $(this).val();
        ticketTable.fnFilter(value);
    });
    $('#month_select').on("change", function() {
        var value = $(this).val();
        ticketTable.fnFilter(value);
    });

    $('#datefrom').on("change", function() {
        ticketTable.fnDraw();
    });
    $('#dateto').on("change", function() {
        ticketTable.fnDraw();
    });

    $('#export_link').bind('click', function() {
        console.log('gotcha!');
        window.open($(this).attr('href'), '_blank');
    });
    $('div#export_report').click(function() {
        $.ajax({
            type: "POST",
            url: base_url + 'frontdesk/reports/ticket/export_2_format',
            processData: false,
            success: function(html) {
                $('#export_link').attr("href", html);
                $('#export_link').trigger("click");
            },
            failure: function(html) {
                console.log(html);
            }
        });
    });

    $('div#export_ticket').click(function() {
        $.ajax({
            type: "POST",
            url: base_url + 'frontdesk/reports/ticket/export_ticket',
            processData: false,
            success: function(html) {
                $('#export_link').attr("href", html);
                $('#export_link').trigger("click");
            },
            failure: function(html) {
                console.log(html);
            }
        });
    });

});
