$(document).ready(function() {
    var ticketService;

    ticketService = $('#ticketService').dataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": base_url + "frontdesk/reports/ticket/service_type",
        "aaSorting": [[8, "desc"]],
        "aoColumns": [
            {
                "mDataProp": "sl"
            },
            {
                "mDataProp": "avg_time"
            },
            {
                "mDataProp": "customer_name",
                "bVisible": false
            },
            {
                "mDataProp": "customer_address",
                "bVisible": false
            },
            {
                "mDataProp": "category_name"
            },
            {
                "mDataProp": "model_name",
                "bVisible": false
            },
            {
                "mDataProp": "branche_name"
            },
            {
                "mDataProp": "counter_name"
            },
            {
                "mDataProp": "status_s",
                "bVisible": false
            },
            {
                "mDataProp": "comments",
                "sWidth": "80px",
                "bVisible": false
            },
            {
                "mDataProp": "created_on",
                "sWidth": "80px",
                "bVisible": false
            }
        ],
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sLengthMenu": "_MENU_ Per Page",
            "sSearch": "Search all columns:"
        },
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "fnInitComplete": function() {
//            var self = this;
//            var nTrs = this.fnGetNodes();
//            $('td', nTrs).click(function() {
//                self.fnFilter(this.innerHTML);
//            });
        },
        "bJQueryUI": false
    });
    $('#export_link').bind('click', function() {
        window.open($(this).attr('href'), '_blank');
    });
    $('div#export_report').click(function() {
        $.ajax({
            type: "POST",
            url: base_url + 'frontdesk/reports/ticket/export_2_format',
            processData: false,
            success: function(html) {
                $('#export_link').attr("href", html);
                $('#export_link').trigger("click");
            },
            failure: function(html) {
                console.log(html);
            }
        });
    });

});
