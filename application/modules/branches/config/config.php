<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['module_config'] = array(
    'description' => 'Allow to access branches',
    'name' => 'Branches',
    'version' => '0.0.1',
    'author' => 'admin',
    'weights' => array(
        'content' => 5
    ),
);
