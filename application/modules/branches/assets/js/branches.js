$(document).ready(function() {
    var branch_list = $('table#tableBranch').find('tbody tr').length;
    if (branch_list > 1) {
        $('table#tableBranch').dataTable({
            "bProcessing": true,
            "bDestroy": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ Per Page",
                "sInfo": "Showing _START_ to _END_ of _TOTAL_ Users"
            },
            "sPaginationType": "full_numbers",
            "aaSorting": [[1, 'asc']]
        });
    }
});