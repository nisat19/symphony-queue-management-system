<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($branches)) {
    $branches = (array) $branches;
}
$id = isset($branches['id']) ? $branches['id'] : '';
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Branches</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>

        <div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
            <?php echo form_label('Name' . lang('bf_form_label_required'), 'branches_name', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='branches_name' type='text' name='branches_name' maxlength="150" value="<?php echo set_value('branches_name', isset($branches['name']) ? $branches['name'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('name'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('address') ? 'error' : ''; ?>">
            <?php echo form_label('Address' . lang('bf_form_label_required'), 'branches_address', array('class' => 'control-label')); ?>
            <div class='controls'>
                <?php echo form_textarea(array('name' => 'branches_address', 'id' => 'branches_address', 'rows' => '5', 'cols' => '80', 'value' => set_value('branches_address', isset($branches['address']) ? $branches['address'] : ''))); ?>
                <span class='help-inline'><?php echo form_error('address'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('phone') ? 'error' : ''; ?>">
            <?php echo form_label('Phone', 'branches_phone', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='branches_phone' type='text' name='branches_phone' maxlength="255" value="<?php echo set_value('branches_phone', isset($branches['phone']) ? $branches['phone'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('phone'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('email') ? 'error' : ''; ?>">
            <?php echo form_label('Email', 'branches_email', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='branches_email' type='text' name='branches_email' maxlength="50" value="<?php echo set_value('branches_email', isset($branches['email']) ? $branches['email'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('email'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('branches_target') ? 'error' : ''; ?>">
            <?php echo form_label('Branch Target', 'branches_target', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='branches_email' type='text' name='branches_target' maxlength="50" value="<?php echo set_value('branches_target', isset($branches['target']) ? $branches['target'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('branches_target'); ?></span>
            </div>
        </div>

        <div class="form-actions">
            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('branches_action_create'); ?>"  />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/content/branches', lang('branches_cancel'), 'class="btn btn-warning"'); ?>

        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>