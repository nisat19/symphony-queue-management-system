<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/content/branches') ?>" id="list"><?php echo lang('branches_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Branches.Content.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/content/branches/create') ?>" id="create_new"><?php echo lang('branches_new'); ?></a>
	</li>
	<?php endif; ?>
</ul>