<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($branches))
{
	$branches = (array) $branches;
}
$id = isset($branches['id']) ? $branches['id'] : '';

?>
<div class="admin-box">
	<h3>Branches</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'branches_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='branches_name' type='text' name='branches_name' maxlength="150" value="<?php echo set_value('branches_name', isset($branches['name']) ? $branches['name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('address') ? 'error' : ''; ?>">
				<?php echo form_label('Address'. lang('bf_form_label_required'), 'branches_address', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'branches_address', 'id' => 'branches_address', 'rows' => '5', 'cols' => '80', 'value' => set_value('branches_address', isset($branches['address']) ? $branches['address'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('address'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('phone') ? 'error' : ''; ?>">
				<?php echo form_label('Phone', 'branches_phone', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='branches_phone' type='text' name='branches_phone' maxlength="255" value="<?php echo set_value('branches_phone', isset($branches['phone']) ? $branches['phone'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('phone'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('email') ? 'error' : ''; ?>">
				<?php echo form_label('Email', 'branches_email', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='branches_email' type='text' name='branches_email' maxlength="50" value="<?php echo set_value('branches_email', isset($branches['email']) ? $branches['email'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('email'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('company_id') ? 'error' : ''; ?>">
				<?php echo form_label('Company Id'. lang('bf_form_label_required'), 'branches_company_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='branches_company_id' type='text' name='branches_company_id' maxlength="11" value="<?php echo set_value('branches_company_id', isset($branches['company_id']) ? $branches['company_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('company_id'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('created_on') ? 'error' : ''; ?>">
				<?php echo form_label('Created On', 'branches_created_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='branches_created_on' type='text' name='branches_created_on' maxlength="1" value="<?php echo set_value('branches_created_on', isset($branches['created_on']) ? $branches['created_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('created_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('modified_on') ? 'error' : ''; ?>">
				<?php echo form_label('Modified On', 'branches_modified_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='branches_modified_on' type='text' name='branches_modified_on' maxlength="1" value="<?php echo set_value('branches_modified_on', isset($branches['modified_on']) ? $branches['modified_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('modified_on'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('branches_action_edit'); ?>"  />
				<?php echo lang('bf_or'); ?>
				<?php echo anchor(SITE_AREA .'/reports/branches', lang('branches_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Branches.Reports.Delete')) : ?>
				or
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('branches_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('branches_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>