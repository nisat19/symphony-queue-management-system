<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['branches_manage']			= 'Manage Branches';
$lang['branches_edit']				= 'Edit';
$lang['branches_true']				= 'True';
$lang['branches_false']				= 'False';
$lang['branches_create']			= 'Create';
$lang['branches_list']				= 'List';
$lang['branches_new']				= 'New';
$lang['branches_edit_text']			= 'Edit this to suit your needs';
$lang['branches_no_records']		= 'There aren\'t any branches in the system.';
$lang['branches_create_new']		= 'Create a new Branches.';
$lang['branches_create_success']	= 'Branches successfully created.';
$lang['branches_create_failure']	= 'There was a problem creating the branches: ';
$lang['branches_create_new_button']	= 'Create New Branches';
$lang['branches_invalid_id']		= 'Invalid Branches ID.';
$lang['branches_edit_success']		= 'Branches successfully saved.';
$lang['branches_edit_failure']		= 'There was a problem saving the branches: ';
$lang['branches_delete_success']	= 'record(s) successfully deleted.';
$lang['branches_delete_failure']	= 'We could not delete the record: ';
$lang['branches_delete_error']		= 'You have not selected any records to delete.';
$lang['branches_actions']			= 'Actions';
$lang['branches_cancel']			= 'Cancel';
$lang['branches_delete_record']		= 'Delete this Branches';
$lang['branches_delete_confirm']	= 'Are you sure you want to delete this branches?';
$lang['branches_edit_heading']		= 'Edit Branches';

// Create/Edit Buttons
$lang['branches_action_edit']		= 'Save Branches';
$lang['branches_action_create']		= 'Create Branches';

// Activities
$lang['branches_act_create_record']	= 'Created record with ID';
$lang['branches_act_edit_record']	= 'Updated record with ID';
$lang['branches_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['branches_column_created']	= 'Created';
$lang['branches_column_deleted']	= 'Deleted';
$lang['branches_column_modified']	= 'Modified';
