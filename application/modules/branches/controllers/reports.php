<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * reports controller
 */
class reports extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Branches.Reports.View');
		$this->load->model('branches_model', null, true);
		$this->lang->load('branches');
		
			Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
			Assets::add_js('jquery-ui-1.8.13.min.js');
			Assets::add_css('jquery-ui-timepicker.css');
			Assets::add_js('jquery-ui-timepicker-addon.js');
		Template::set_block('sub_nav', 'reports/_sub_nav');

		Assets::add_module_js('branches', 'branches.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	 */
	public function index()
	{

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->branches_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('branches_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('branches_delete_failure') . $this->branches_model->error, 'error');
				}
			}
		}

		$records = $this->branches_model->find_all();

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Branches');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Creates a Branches object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Branches.Reports.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_branches())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('branches_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'branches');

				Template::set_message(lang('branches_create_success'), 'success');
				redirect(SITE_AREA .'/reports/branches');
			}
			else
			{
				Template::set_message(lang('branches_create_failure') . $this->branches_model->error, 'error');
			}
		}
		Assets::add_module_js('branches', 'branches.js');

		Template::set('toolbar_title', lang('branches_create') . ' Branches');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Branches data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('branches_invalid_id'), 'error');
			redirect(SITE_AREA .'/reports/branches');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Branches.Reports.Edit');

			if ($this->save_branches('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('branches_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'branches');

				Template::set_message(lang('branches_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('branches_edit_failure') . $this->branches_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Branches.Reports.Delete');

			if ($this->branches_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('branches_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'branches');

				Template::set_message(lang('branches_delete_success'), 'success');

				redirect(SITE_AREA .'/reports/branches');
			}
			else
			{
				Template::set_message(lang('branches_delete_failure') . $this->branches_model->error, 'error');
			}
		}
		Template::set('branches', $this->branches_model->find($id));
		Template::set('toolbar_title', lang('branches_edit') .' Branches');
		Template::render();
	}

	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_branches($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['name']        = $this->input->post('branches_name');
		$data['address']        = $this->input->post('branches_address');
		$data['phone']        = $this->input->post('branches_phone');
		$data['email']        = $this->input->post('branches_email');
		$data['company_id']        = $this->input->post('branches_company_id');
		$data['created_on']        = $this->input->post('branches_created_on');
		$data['modified_on']        = $this->input->post('branches_modified_on') ? $this->input->post('branches_modified_on') : '0000-00-00 00:00:00';

		if ($type == 'insert')
		{
			$id = $this->branches_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->branches_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}