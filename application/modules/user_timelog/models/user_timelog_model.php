<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_timelog_model extends BF_Model {

    protected $table_name = "bf_users_time";
    protected $key = "id";
    protected $soft_deletes = false;
    protected $date_format = "datetime";
    protected $log_user = FALSE;
    protected $set_created = true;
    protected $set_modified = true;
    protected $created_field = "created_on";
    protected $modified_field = "modified_on";

    /*
      Customize the operations of the model without recreating the insert, update,
      etc methods by adding the method names to act as callbacks here.
     */
    protected $before_insert = array();
    protected $after_insert = array();
    protected $before_update = array();
    protected $after_update = array();
    protected $before_find = array();
    protected $after_find = array();
    protected $before_delete = array();
    protected $after_delete = array();

    /*
      For performance reasons, you may require your model to NOT return the
      id of the last inserted row as it is a bit of a slow method. This is
      primarily helpful when running big loops over data.
     */
    protected $return_insert_id = TRUE;
    // The default type of element data is returned as.
    protected $return_type = "object";
    // Items that are always removed from data arrays prior to
    // any inserts or updates.
    protected $protected_attributes = array();

    /*
      You may need to move certain rules (like required) into the
      $insert_validation_rules array and out of the standard validation array.
      That way it is only required during inserts, not updates which may only
      be updating a portion of the data.
     */
    protected $validation_rules = array();
    protected $insert_validation_rules = array();
    protected $skip_validation = TRUE;

    //--------------------------------------------------------------------
    public function getUserList() {
        return $this->db->query("SELECT
                                    bf_users_time.id
                                FROM
                                    bf_users_time
                                WHERE
                                    bf_users_time.user_id = '" . $this->auth->user()->id . "'
                                AND DATE(bf_users_time.last_login) = DATE(NOW())");
    }

    public function getAllUsers($fromdate = NULL, $todate = NULL) {
        $query = $this->db
                ->select("bf_users_time.id,
                    bf_users_time.user_id,
                    bf_users.role_id,
                    bf_users.company_id,
                    bf_users.branch_id,
                    bf_users.counter_id,
                    bf_users.email,
                    bf_users.username,
                    bf_users.last_ip,
                    bf_users_time.last_login,
                    bf_users_time.time,
                    bf_users.display_name,
                    bf_users.active,
                    bf_roles.role_name,
                    bf_counter.title,
                    bf_branches.`name`,
                    bf_branches.address,
                    bf_branches.phone,
                    bf_branches.email as branch_email,
                    (
                        SELECT
                                COUNT(bf_ticket.id)
                        FROM
                                bf_ticket
                        WHERE
                                bf_ticket.counter_id = bf_counter.id
                        AND bf_ticket.`status` = 'finished'
                    ) AS total_serve")
                ->join('bf_users', 'bf_users.id = bf_users_time.user_id', 'INNER')
                ->join('bf_roles', 'bf_users.role_id = bf_roles.role_id', 'INNER')
                ->join('bf_branches', 'bf_branches.id = bf_users.branch_id', 'INNER')
                ->join('bf_counter', 'bf_counter.id = bf_users.counter_id', 'LEFT')
                ->join('bf_ticket', 'bf_counter.id = bf_ticket.counter_id', 'LEFT');
        if ($this->auth->user()->role_id == 8) {
            $query = $this->db
                    ->where('bf_users.branch_id', $this->auth->user()->branch_id);
        }
        $query = $this->db->where('bf_users.role_id', '7');
        if ($fromdate != NULL && $todate != NULL) {
            $this->db->where('bf_users_time.last_login BETWEEN "' . date('Y-m-d', strtotime($fromdate)) . '" and "' . date('Y-m-d', strtotime($todate)) . '"');
        }
        $query = $this->db->group_by('bf_ticket.counter_id');
        $query = $this->db->get('bf_users_time');
        return $query;
    }

    public function updateTimeLog() {
        return $this->db->query("UPDATE
                            bf_users_time
                        SET
                            bf_users_time.time = bf_users_time.time + 30
                        WHERE
                            bf_users_time.user_id = '" . $this->auth->user()->id . "'
                        AND DATE(bf_users_time.last_login) = DATE(NOW())");
    }

}
