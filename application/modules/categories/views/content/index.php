<?php
$num_columns = 6;
$can_delete = $this->auth->has_permission('Categories.Content.Delete');
$can_edit = $this->auth->has_permission('Categories.Content.Edit');
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Categories</h3>
    <?php echo form_open($this->uri->uri_string()); ?>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <?php if ($can_delete && $has_records) : ?>
                    <th class="column-check"><input class="check-all" type="checkbox" /></th>
                <?php endif; ?>

                <th>Name</th>
                <th>Description</th>
                <th>Created By</th>
                <th>Created On</th>
                <!--<th>Modified On</th>-->
            </tr>
        </thead>
        <?php if ($has_records) : ?>
            <tfoot>
                <?php if ($can_delete) : ?>
                    <tr>
                        <td colspan="<?php echo $num_columns; ?>">
                            <?php echo lang('bf_with_selected'); ?>
                            <input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('categories_delete_confirm'))); ?>')" />
                        </td>
                    </tr>
                <?php endif; ?>
            </tfoot>
        <?php endif; ?>
        <tbody>
            <?php
            if ($has_records) :
                foreach ($records as $record) :
                    ?>
                    <tr>
                        <?php if ($can_delete) : ?>
                            <td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td>
                            <?php endif; ?>

                        <?php if ($can_edit) : ?>
                            <td><?php echo anchor(SITE_AREA . '/content/categories/edit/' . $record->id, '<span class="icon-pencil"></span>' . $record->name); ?></td>
                        <?php else : ?>
                            <td><?php e($record->name); ?></td>
                        <?php endif; ?>
                        <td><?php e($record->description) ?></td>
                        <td><?php e($record->created_by) ?></td>
                        <td><?php e($record->created_on) ?></td>
                        <!--<td><?php e($record->modified_on) ?></td>-->
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
    <?php echo form_close(); ?>
</div>