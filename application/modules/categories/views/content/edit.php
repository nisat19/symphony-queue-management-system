<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($categories)) {
    $categories = (array) $categories;
}
$id = isset($categories['id']) ? $categories['id'] : '';
?>
<div class="admin-box">
    <div class="pull-right" id="sub-menu" style="margin-top: 10px;">
        <?php Template::block('sub_nav', ''); ?>
    </div>
    <h3>Categories</h3>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>

        <div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
            <?php echo form_label('Name' . lang('bf_form_label_required'), 'categories_name', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='categories_name' type='text' name='categories_name' maxlength="50" value="<?php echo set_value('categories_name', isset($categories['name']) ? $categories['name'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('name'); ?></span>
            </div>
        </div>

        <div class="control-group <?php echo form_error('description') ? 'error' : ''; ?>">
            <?php echo form_label('Description' . lang('bf_form_label_required'), 'categories_description', array('class' => 'control-label')); ?>
            <div class='controls'>
                <input id='categories_description' type='text' name='categories_description' maxlength="150" value="<?php echo set_value('categories_description', isset($categories['description']) ? $categories['description'] : ''); ?>" />
                <span class='help-inline'><?php echo form_error('description'); ?></span>
            </div>
        </div>

        <div class="form-actions">
            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('categories_action_edit'); ?>"  />
            <?php echo lang('bf_or'); ?>
            <?php echo anchor(SITE_AREA . '/content/categories', lang('categories_cancel'), 'class="btn btn-warning"'); ?>

            <?php if ($this->auth->has_permission('Categories.Content.Delete')) : ?>
                or
                <button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('categories_delete_confirm'))); ?>');
                        ">
                    <span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('categories_delete_record'); ?>
                </button>
            <?php endif; ?>
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>