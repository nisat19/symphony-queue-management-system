<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * reports controller
 */
class reports extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Categories.Reports.View');
        $this->load->model('categories_model', null, true);
        $this->lang->load('categories');

        Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
        Assets::add_js('jquery-ui-1.8.13.min.js');
        Assets::add_css('jquery-ui-timepicker.css');
        Assets::add_js('jquery-ui-timepicker-addon.js');
        Template::set_block('sub_nav', 'reports/_sub_nav');

        Assets::add_module_js('categories', 'categories.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index() {

        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->categories_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('categories_delete_success'), 'success');
                } else {
                    Template::set_message(lang('categories_delete_failure') . $this->categories_model->error, 'error');
                }
            }
        }

        $records = $this->categories_model
                ->select('*,bf_categories.id as id, bf_users.username as created_by')
                ->join('bf_users', 'bf_users.id = bf_categories.created_by')
                ->find_all();

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Categories');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Categories object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Categories.Reports.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_categories()) {
                // Log the activity
                log_activity($this->current_user->id, lang('categories_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'categories');

                Template::set_message(lang('categories_create_success'), 'success');
                redirect(SITE_AREA . '/reports/categories');
            } else {
                Template::set_message(lang('categories_create_failure') . $this->categories_model->error, 'error');
            }
        }
        Assets::add_module_js('categories', 'categories.js');

        Template::set('toolbar_title', lang('categories_create') . ' Categories');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Categories data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('categories_invalid_id'), 'error');
            redirect(SITE_AREA . '/reports/categories');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Categories.Reports.Edit');

            if ($this->save_categories('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('categories_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'categories');

                Template::set_message(lang('categories_edit_success'), 'success');
            } else {
                Template::set_message(lang('categories_edit_failure') . $this->categories_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Categories.Reports.Delete');

            if ($this->categories_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('categories_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'categories');

                Template::set_message(lang('categories_delete_success'), 'success');

                redirect(SITE_AREA . '/reports/categories');
            } else {
                Template::set_message(lang('categories_delete_failure') . $this->categories_model->error, 'error');
            }
        }
        Template::set('categories', $this->categories_model->find($id));
        Template::set('toolbar_title', lang('categories_edit') . ' Categories');
        Template::render();
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_categories($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['name'] = $this->input->post('categories_name');
        $data['description'] = $this->input->post('categories_description');
        $data['created_by'] = $this->input->post('categories_created_by');
        $data['created_on'] = $this->input->post('categories_created_on') ? $this->input->post('categories_created_on') : '0000-00-00 00:00:00';
        $data['modified_on'] = $this->input->post('categories_modified_on') ? $this->input->post('categories_modified_on') : '0000-00-00 00:00:00';

        if ($type == 'insert') {
            $id = $this->categories_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->categories_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
