<div class="jumbotron" text-align="center">
    <h1>Welcome to Symphony Queue Management System</h1>

    <p class="lead">&nbsp;</p>

    <?php if (isset($current_user->email)) : ?>
        <a href="<?php echo site_url(SITE_AREA . '/content/ticket') ?>" class="btn btn-large btn-success">Go to the Admin area</a>
        <br/>
        <h2>OR</h2>
        <a href="<?php echo site_url('/mainsite') ?>" class="btn btn-large btn-success">See Queue Information</a>
        <!--        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url('/mainsite/ticket') ?>" class="btn btn-large btn-primary">Ticket</a>-->
    <?php else : ?>
        <a href="<?php echo site_url(LOGIN_URL); ?>" class="btn btn-large btn-primary"><?php echo lang('bf_action_login'); ?></a>
        <br/>
        <h2>OR</h2>
        <a href="<?php echo site_url('/mainsite') ?>" class="btn btn-large btn-success">See Queue Information</a>
        <!--        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url('/mainsite/ticket') ?>" class="btn btn-large btn-primary">Ticket</a>-->
    <?php endif; ?>

    <!--<br/><br/><a href="<?php echo site_url('/docs') ?>" class="btn btn-large btn-info">Browse the Docs</a>-->
</div>