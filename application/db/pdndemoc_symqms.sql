/*
Navicat MySQL Data Transfer

Source Server         : Symphony
Source Server Version : 50535
Source Host           : pdndemo.com:3306
Source Database       : pdndemoc_symqms

Target Server Type    : MYSQL
Target Server Version : 50535
File Encoding         : 65001

Date: 2014-01-05 18:30:00
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `bf_activities`
-- ----------------------------
DROP TABLE IF EXISTS `bf_activities`;
CREATE TABLE `bf_activities` (
  `activity_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `activity` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `deleted` tinyint(12) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_activities
-- ----------------------------
INSERT INTO bf_activities VALUES ('1', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-09 07:34:33', '0');
INSERT INTO bf_activities VALUES ('2', '1', 'Created Module: Companies : 127.0.0.1', 'modulebuilder', '2013-12-09 07:44:17', '0');
INSERT INTO bf_activities VALUES ('3', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-10 05:32:10', '0');
INSERT INTO bf_activities VALUES ('4', '1', 'App settings saved from: 127.0.0.1', 'core', '2013-12-10 05:35:15', '0');
INSERT INTO bf_activities VALUES ('5', '1', 'modified user: admin', 'users', '2013-12-10 05:35:58', '0');
INSERT INTO bf_activities VALUES ('6', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-10 05:36:06', '0');
INSERT INTO bf_activities VALUES ('7', '1', 'updated their profile: admin', 'users', '2013-12-10 05:36:22', '0');
INSERT INTO bf_activities VALUES ('8', '1', 'updated their profile: admin', 'users', '2013-12-10 05:36:36', '0');
INSERT INTO bf_activities VALUES ('9', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-10 05:36:55', '0');
INSERT INTO bf_activities VALUES ('10', '1', 'App settings saved from: 127.0.0.1', 'core', '2013-12-10 05:37:50', '0');
INSERT INTO bf_activities VALUES ('11', '1', 'Created Module: API : 127.0.0.1', 'modulebuilder', '2013-12-10 06:37:41', '0');
INSERT INTO bf_activities VALUES ('12', '1', 'Migrate Type: api_ Uninstalled Version: 0 from: 127.0.0.1', 'migrations', '2013-12-10 06:38:05', '0');
INSERT INTO bf_activities VALUES ('13', '1', 'Migrate module: api Version: 0 from: 127.0.0.1', 'migrations', '2013-12-10 06:38:06', '0');
INSERT INTO bf_activities VALUES ('14', '1', 'Created Module: API : 127.0.0.1', 'modulebuilder', '2013-12-10 06:41:59', '0');
INSERT INTO bf_activities VALUES ('15', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-10 06:49:28', '0');
INSERT INTO bf_activities VALUES ('16', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-10 06:49:58', '0');
INSERT INTO bf_activities VALUES ('17', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-10 09:52:23', '0');
INSERT INTO bf_activities VALUES ('18', '1', 'App settings saved from: 127.0.0.1', 'core', '2013-12-10 11:46:23', '0');
INSERT INTO bf_activities VALUES ('19', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-10 11:47:02', '0');
INSERT INTO bf_activities VALUES ('20', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-11 04:50:51', '0');
INSERT INTO bf_activities VALUES ('21', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-12 05:19:06', '0');
INSERT INTO bf_activities VALUES ('22', '1', 'Created Module: Categories : 127.0.0.1', 'modulebuilder', '2013-12-12 05:25:39', '0');
INSERT INTO bf_activities VALUES ('23', '1', 'Created record with ID: 2 : 127.0.0.1', 'categories', '2013-12-12 05:42:52', '0');
INSERT INTO bf_activities VALUES ('24', '1', 'Updated record with ID: 2 : 127.0.0.1', 'categories', '2013-12-12 05:56:39', '0');
INSERT INTO bf_activities VALUES ('25', '1', 'Updated record with ID: 2 : 127.0.0.1', 'categories', '2013-12-12 05:56:45', '0');
INSERT INTO bf_activities VALUES ('26', '1', 'Updated record with ID: 2 : 127.0.0.1', 'categories', '2013-12-12 05:56:58', '0');
INSERT INTO bf_activities VALUES ('27', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-12 10:08:18', '0');
INSERT INTO bf_activities VALUES ('28', '1', 'Created Module: Branches : 127.0.0.1', 'modulebuilder', '2013-12-12 10:11:52', '0');
INSERT INTO bf_activities VALUES ('29', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-12 10:27:32', '0');
INSERT INTO bf_activities VALUES ('30', '1', 'Created record with ID: 1 : 127.0.0.1', 'branches', '2013-12-12 10:35:07', '0');
INSERT INTO bf_activities VALUES ('31', '1', 'Updated record with ID: 1 : 127.0.0.1', 'branches', '2013-12-12 10:54:18', '0');
INSERT INTO bf_activities VALUES ('32', '1', 'Created Module: Counters : 127.0.0.1', 'modulebuilder', '2013-12-12 11:05:29', '0');
INSERT INTO bf_activities VALUES ('33', '1', 'Created record with ID: 1 : 127.0.0.1', 'counters', '2013-12-12 11:21:32', '0');
INSERT INTO bf_activities VALUES ('34', '1', 'Updated record with ID: 1 : 127.0.0.1', 'counters', '2013-12-12 11:26:01', '0');
INSERT INTO bf_activities VALUES ('35', '1', 'Created Module: Models : 127.0.0.1', 'modulebuilder', '2013-12-12 11:31:03', '0');
INSERT INTO bf_activities VALUES ('36', '1', 'Created record with ID: 1 : 127.0.0.1', 'models', '2013-12-12 11:35:10', '0');
INSERT INTO bf_activities VALUES ('37', '1', 'Updated record with ID: 1 : 127.0.0.1', 'models', '2013-12-12 11:36:03', '0');
INSERT INTO bf_activities VALUES ('38', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-15 05:16:18', '0');
INSERT INTO bf_activities VALUES ('39', '1', 'Created Module: Tickets : 127.0.0.1', 'modulebuilder', '2013-12-15 05:22:49', '0');
INSERT INTO bf_activities VALUES ('40', '1', 'Migrate Type: tickets_ Uninstalled Version: 0 from: 127.0.0.1', 'migrations', '2013-12-15 05:27:51', '0');
INSERT INTO bf_activities VALUES ('41', '1', 'Migrate module: tickets Version: 0 from: 127.0.0.1', 'migrations', '2013-12-15 05:27:51', '0');
INSERT INTO bf_activities VALUES ('42', '1', 'Deleted Module: Tickets : 127.0.0.1', 'builder', '2013-12-15 05:28:10', '0');
INSERT INTO bf_activities VALUES ('43', '1', 'Created Module: Ticket : 127.0.0.1', 'modulebuilder', '2013-12-15 05:31:05', '0');
INSERT INTO bf_activities VALUES ('44', '1', 'Created Module: Ticket Meta : 127.0.0.1', 'modulebuilder', '2013-12-15 05:46:21', '0');
INSERT INTO bf_activities VALUES ('45', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-15 10:55:20', '0');
INSERT INTO bf_activities VALUES ('46', '1', 'Created record with ID: 1 : 127.0.0.1', 'ticket', '2013-12-15 11:19:46', '0');
INSERT INTO bf_activities VALUES ('47', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-15 12:14:01', '0');
INSERT INTO bf_activities VALUES ('48', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-17 04:26:03', '0');
INSERT INTO bf_activities VALUES ('49', '1', 'Created record with ID: 2 : 127.0.0.1', 'counters', '2013-12-17 05:02:35', '0');
INSERT INTO bf_activities VALUES ('50', '1', 'App settings saved from: 127.0.0.1', 'core', '2013-12-17 05:23:34', '0');
INSERT INTO bf_activities VALUES ('51', '1', 'Deleted Module: Token : 127.0.0.1', 'builder', '2013-12-17 06:43:57', '0');
INSERT INTO bf_activities VALUES ('52', '1', 'Created Module: Token : 127.0.0.1', 'modulebuilder', '2013-12-17 06:46:35', '0');
INSERT INTO bf_activities VALUES ('53', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-18 06:01:54', '0');
INSERT INTO bf_activities VALUES ('54', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-18 06:05:55', '0');
INSERT INTO bf_activities VALUES ('55', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-18 06:26:49', '0');
INSERT INTO bf_activities VALUES ('56', '2', 'registered a new account.', 'users', '2013-12-19 07:16:48', '0');
INSERT INTO bf_activities VALUES ('57', '3', 'registered a new account.', 'users', '2013-12-19 07:20:23', '0');
INSERT INTO bf_activities VALUES ('58', '4', 'registered a new account.', 'users', '2013-12-19 13:22:00', '0');
INSERT INTO bf_activities VALUES ('59', '1', 'FIXME (\"us_log_status_change\"): nisat : Deactivateed', 'users', '2013-12-19 09:19:43', '0');
INSERT INTO bf_activities VALUES ('60', '1', 'FIXME (\"us_log_status_change\"): nisat : Activateed', 'users', '2013-12-19 09:20:30', '0');
INSERT INTO bf_activities VALUES ('61', '1', 'FIXME (\"us_log_status_change\"): nisat : Deactivateed', 'users', '2013-12-19 09:22:57', '0');
INSERT INTO bf_activities VALUES ('62', '1', 'FIXME (\"us_log_status_change\"): nisat : Activateed', 'users', '2013-12-19 09:23:09', '0');
INSERT INTO bf_activities VALUES ('63', '1', 'deleted user: nisat1', 'users', '2013-12-19 09:25:12', '0');
INSERT INTO bf_activities VALUES ('64', '1', 'FIXME (\"us_log_status_change\"): user1 : Activateed', 'users', '2013-12-19 09:32:31', '0');
INSERT INTO bf_activities VALUES ('65', '1', 'FIXME (\"us_log_status_change\"): user1 : Deactivateed', 'users', '2013-12-19 09:32:50', '0');
INSERT INTO bf_activities VALUES ('66', '1', 'deleted user: user1', 'users', '2013-12-19 09:32:58', '0');
INSERT INTO bf_activities VALUES ('67', '1', 'FIXME (\"us_log_status_change\"): nisat : Deactivateed', 'users', '2013-12-19 09:33:01', '0');
INSERT INTO bf_activities VALUES ('68', '1', 'FIXME (\"us_log_status_change\"): nisat : Activateed', 'users', '2013-12-19 09:33:05', '0');
INSERT INTO bf_activities VALUES ('69', '1', 'modified user: admin', 'users', '2013-12-19 09:37:42', '0');
INSERT INTO bf_activities VALUES ('70', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-19 09:37:53', '0');
INSERT INTO bf_activities VALUES ('71', '1', 'modified user: symphony', 'users', '2013-12-19 09:39:23', '0');
INSERT INTO bf_activities VALUES ('72', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-19 09:39:56', '0');
INSERT INTO bf_activities VALUES ('73', '1', 'modified user: symphony', 'users', '2013-12-19 09:40:24', '0');
INSERT INTO bf_activities VALUES ('74', '2', 'logged in from: 127.0.0.1', 'users', '2013-12-19 09:40:41', '0');
INSERT INTO bf_activities VALUES ('75', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-19 09:41:26', '0');
INSERT INTO bf_activities VALUES ('76', '5', 'registered a new account.', 'users', '2013-12-19 16:03:06', '0');
INSERT INTO bf_activities VALUES ('77', '1', 'Created Module: mainsite : 127.0.0.1', 'modulebuilder', '2013-12-19 10:38:09', '0');
INSERT INTO bf_activities VALUES ('78', '2', 'logged in from: 127.0.0.1', 'users', '2013-12-19 12:12:04', '0');
INSERT INTO bf_activities VALUES ('79', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-19 13:53:54', '0');
INSERT INTO bf_activities VALUES ('80', '2', 'Created record with ID: 3 : 127.0.0.1', 'counters', '2013-12-19 14:03:19', '0');
INSERT INTO bf_activities VALUES ('81', '2', 'logged in from: 127.0.0.1', 'users', '2013-12-20 15:24:58', '0');
INSERT INTO bf_activities VALUES ('82', '2', 'logged in from: 127.0.0.1', 'users', '2013-12-20 19:06:14', '0');
INSERT INTO bf_activities VALUES ('83', '2', 'logged in from: 127.0.0.1', 'users', '2013-12-21 05:58:54', '0');
INSERT INTO bf_activities VALUES ('84', '2', 'logged in from: 127.0.0.1', 'users', '2013-12-22 04:06:24', '0');
INSERT INTO bf_activities VALUES ('85', '6', 'registered a new account.', 'users', '2013-12-22 15:29:17', '0');
INSERT INTO bf_activities VALUES ('86', '7', 'registered a new account.', 'users', '2013-12-22 15:31:08', '0');
INSERT INTO bf_activities VALUES ('87', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-22 09:34:00', '0');
INSERT INTO bf_activities VALUES ('88', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-22 09:43:42', '0');
INSERT INTO bf_activities VALUES ('89', '6', 'logged in from: 127.0.0.1', 'users', '2013-12-22 09:53:03', '0');
INSERT INTO bf_activities VALUES ('90', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-22 10:18:51', '0');
INSERT INTO bf_activities VALUES ('91', '1', 'App settings saved from: 127.0.0.1', 'core', '2013-12-22 10:25:43', '0');
INSERT INTO bf_activities VALUES ('92', '6', 'logged in from: 127.0.0.1', 'users', '2013-12-22 13:08:00', '0');
INSERT INTO bf_activities VALUES ('93', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-22 13:26:58', '0');
INSERT INTO bf_activities VALUES ('94', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-23 04:29:56', '0');
INSERT INTO bf_activities VALUES ('95', '6', 'logged in from: 127.0.0.1', 'users', '2013-12-23 04:30:28', '0');
INSERT INTO bf_activities VALUES ('96', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-23 04:33:51', '0');
INSERT INTO bf_activities VALUES ('97', '7', 'Created record with ID: 4 : 127.0.0.1', 'counters', '2013-12-23 04:40:36', '0');
INSERT INTO bf_activities VALUES ('98', '8', 'registered a new account.', 'users', '2013-12-23 10:58:09', '0');
INSERT INTO bf_activities VALUES ('99', '9', 'registered a new account.', 'users', '2013-12-23 11:01:48', '0');
INSERT INTO bf_activities VALUES ('100', '8', 'logged in from: 127.0.0.1', 'users', '2013-12-23 05:03:53', '0');
INSERT INTO bf_activities VALUES ('101', '9', 'logged in from: 127.0.0.1', 'users', '2013-12-23 05:04:28', '0');
INSERT INTO bf_activities VALUES ('102', '8', 'logged in from: 127.0.0.1', 'users', '2013-12-23 06:46:53', '0');
INSERT INTO bf_activities VALUES ('103', '9', 'logged in from: 127.0.0.1', 'users', '2013-12-23 06:52:09', '0');
INSERT INTO bf_activities VALUES ('104', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-23 06:53:12', '0');
INSERT INTO bf_activities VALUES ('105', '6', 'logged in from: 127.0.0.1', 'users', '2013-12-23 13:03:37', '0');
INSERT INTO bf_activities VALUES ('106', '9', 'logged in from: 127.0.0.1', 'users', '2013-12-23 15:19:12', '0');
INSERT INTO bf_activities VALUES ('107', '8', 'logged in from: 127.0.0.1', 'users', '2013-12-23 17:01:17', '0');
INSERT INTO bf_activities VALUES ('108', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-23 17:01:49', '0');
INSERT INTO bf_activities VALUES ('109', '6', 'logged in from: 127.0.0.1', 'users', '2013-12-24 11:31:56', '0');
INSERT INTO bf_activities VALUES ('110', '9', 'logged in from: 127.0.0.1', 'users', '2013-12-24 11:37:39', '0');
INSERT INTO bf_activities VALUES ('111', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-24 15:44:20', '0');
INSERT INTO bf_activities VALUES ('112', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-24 16:50:29', '0');
INSERT INTO bf_activities VALUES ('113', '9', 'logged in from: 127.0.0.1', 'users', '2013-12-24 17:07:20', '0');
INSERT INTO bf_activities VALUES ('114', '8', 'logged in from: 127.0.0.1', 'users', '2013-12-24 17:10:02', '0');
INSERT INTO bf_activities VALUES ('115', '6', 'logged in from: 127.0.0.1', 'users', '2013-12-24 17:39:17', '0');
INSERT INTO bf_activities VALUES ('116', '6', 'logged in from: 127.0.0.1', 'users', '2013-12-26 11:36:52', '0');
INSERT INTO bf_activities VALUES ('117', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-26 12:00:27', '0');
INSERT INTO bf_activities VALUES ('118', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-26 17:16:12', '0');
INSERT INTO bf_activities VALUES ('119', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-29 10:54:23', '0');
INSERT INTO bf_activities VALUES ('120', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-29 17:59:57', '0');
INSERT INTO bf_activities VALUES ('121', '6', 'logged in from: 127.0.0.1', 'users', '2013-12-29 18:02:07', '0');
INSERT INTO bf_activities VALUES ('122', '7', 'logged in from: 127.0.0.1', 'users', '2013-12-30 10:20:44', '0');
INSERT INTO bf_activities VALUES ('123', '1', 'logged in from: 127.0.0.1', 'users', '2013-12-30 13:38:18', '0');
INSERT INTO bf_activities VALUES ('124', '2', 'logged in from: 127.0.0.1', 'users', '2013-12-30 13:45:17', '0');
INSERT INTO bf_activities VALUES ('125', '6', 'logged in from: 127.0.0.1', 'users', '2013-12-30 16:22:18', '0');
INSERT INTO bf_activities VALUES ('126', '2', 'logged in from: 127.0.0.1', 'users', '2013-12-30 16:38:47', '0');
INSERT INTO bf_activities VALUES ('127', '6', 'logged in from: 119.148.16.250', 'users', '2013-12-30 06:20:22', '0');
INSERT INTO bf_activities VALUES ('128', '7', 'logged in from: 119.148.16.250', 'users', '2013-12-30 06:20:59', '0');
INSERT INTO bf_activities VALUES ('129', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-01 02:13:34', '0');
INSERT INTO bf_activities VALUES ('130', '7', 'logged in from: 119.148.16.250', 'users', '2014-01-01 03:20:09', '0');
INSERT INTO bf_activities VALUES ('131', '7', 'logged in from: 119.148.16.250', 'users', '2014-01-01 03:39:11', '0');
INSERT INTO bf_activities VALUES ('132', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-01 21:03:34', '0');
INSERT INTO bf_activities VALUES ('133', '2', 'Created record with ID: 129 : 119.148.16.250', 'ticket', '2014-01-01 21:04:42', '0');
INSERT INTO bf_activities VALUES ('134', '2', 'Created record with ID: 130 : 119.148.16.250', 'ticket', '2014-01-01 21:04:42', '0');
INSERT INTO bf_activities VALUES ('135', '2', 'Created record with ID: 131 : 119.148.16.250', 'ticket', '2014-01-01 21:08:46', '0');
INSERT INTO bf_activities VALUES ('136', '10', 'registered a new account.', 'users', '2014-01-02 10:19:01', '0');
INSERT INTO bf_activities VALUES ('137', '2', 'FIXME (\"us_log_status_change\"): manager2 : Deactivateed', 'users', '2014-01-01 21:19:26', '0');
INSERT INTO bf_activities VALUES ('138', '2', 'FIXME (\"us_log_status_change\"): manager2 : Activateed', 'users', '2014-01-01 21:20:00', '0');
INSERT INTO bf_activities VALUES ('139', '2', 'Created record with ID: 2 : 119.148.16.250', 'branches', '2014-01-01 21:22:19', '0');
INSERT INTO bf_activities VALUES ('140', '2', 'Created record with ID: 5 : 119.148.16.250', 'counters', '2014-01-01 21:27:13', '0');
INSERT INTO bf_activities VALUES ('141', '2', 'Created record with ID: 6 : 119.148.16.250', 'categories', '2014-01-01 21:27:57', '0');
INSERT INTO bf_activities VALUES ('142', '2', 'Created record with ID: 6 : 119.148.16.250', 'models', '2014-01-01 21:33:05', '0');
INSERT INTO bf_activities VALUES ('143', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-01 21:35:58', '0');
INSERT INTO bf_activities VALUES ('144', '2', 'FIXME (\"us_log_status_change\"): manager2 : Deactivateed', 'users', '2014-01-01 21:44:59', '0');
INSERT INTO bf_activities VALUES ('145', '7', 'logged in from: 119.148.16.250', 'users', '2014-01-01 21:45:14', '0');
INSERT INTO bf_activities VALUES ('146', '2', 'FIXME (\"us_log_status_change\"): manager2 : Activateed', 'users', '2014-01-01 21:46:59', '0');
INSERT INTO bf_activities VALUES ('147', '1', 'logged in from: 119.148.16.250', 'users', '2014-01-01 21:54:54', '0');
INSERT INTO bf_activities VALUES ('148', '2', 'deleted user: manager2', 'users', '2014-01-01 21:57:45', '0');
INSERT INTO bf_activities VALUES ('149', '11', 'registered a new account.', 'users', '2014-01-02 12:03:02', '0');
INSERT INTO bf_activities VALUES ('150', '2', 'FIXME (\"us_log_status_change\"): dmeo : Deactivateed', 'users', '2014-01-01 23:03:27', '0');
INSERT INTO bf_activities VALUES ('151', '2', 'FIXME (\"us_log_status_change\"): dmeo : Activateed', 'users', '2014-01-01 23:03:31', '0');
INSERT INTO bf_activities VALUES ('152', '2', 'deleted user: demo', 'users', '2014-01-01 23:03:37', '0');
INSERT INTO bf_activities VALUES ('153', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-01 23:08:15', '0');
INSERT INTO bf_activities VALUES ('154', '7', 'logged in from: 119.148.16.250', 'users', '2014-01-01 23:43:18', '0');
INSERT INTO bf_activities VALUES ('155', '7', 'logged in from: 119.148.16.250', 'users', '2014-01-02 02:09:07', '0');
INSERT INTO bf_activities VALUES ('156', '6', 'logged in from: 119.148.16.250', 'users', '2014-01-02 02:47:21', '0');
INSERT INTO bf_activities VALUES ('157', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-02 03:08:53', '0');
INSERT INTO bf_activities VALUES ('158', '12', 'registered a new account.', 'users', '2014-01-02 16:10:29', '0');
INSERT INTO bf_activities VALUES ('159', '13', 'registered a new account.', 'users', '2014-01-02 16:11:55', '0');
INSERT INTO bf_activities VALUES ('160', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-02 03:41:59', '0');
INSERT INTO bf_activities VALUES ('161', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-02 03:43:45', '0');
INSERT INTO bf_activities VALUES ('162', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-02 03:56:39', '0');
INSERT INTO bf_activities VALUES ('163', '6', 'logged in from: 119.148.16.250', 'users', '2014-01-02 04:03:36', '0');
INSERT INTO bf_activities VALUES ('164', '2', 'Created record with ID: 3 : 119.148.16.250', 'branches', '2014-01-02 04:16:52', '0');
INSERT INTO bf_activities VALUES ('165', '2', 'Updated record with ID: 2 : 119.148.16.250', 'counters', '2014-01-02 04:24:59', '0');
INSERT INTO bf_activities VALUES ('166', '2', 'Updated record with ID: 1 : 119.148.16.250', 'branches', '2014-01-02 04:31:01', '0');
INSERT INTO bf_activities VALUES ('167', '2', 'Updated record with ID: 1 : 119.148.16.250', 'branches', '2014-01-02 04:31:11', '0');
INSERT INTO bf_activities VALUES ('168', '2', 'Updated record with ID: 1 : 119.148.16.250', 'branches', '2014-01-02 04:31:12', '0');
INSERT INTO bf_activities VALUES ('169', '2', 'Deleted record with ID: 3 : 119.148.16.250', 'branches', '2014-01-02 04:31:22', '0');
INSERT INTO bf_activities VALUES ('170', '2', 'Created record with ID: 6 : 119.148.16.250', 'counters', '2014-01-02 04:32:00', '0');
INSERT INTO bf_activities VALUES ('171', '6', 'logged in from: 119.148.16.250', 'users', '2014-01-02 05:13:55', '0');
INSERT INTO bf_activities VALUES ('172', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-04 22:54:15', '0');
INSERT INTO bf_activities VALUES ('173', '6', 'logged in from: 119.148.16.250', 'users', '2014-01-04 23:07:39', '0');
INSERT INTO bf_activities VALUES ('174', '7', 'logged in from: 119.148.16.250', 'users', '2014-01-04 23:09:20', '0');
INSERT INTO bf_activities VALUES ('175', '6', 'logged in from: 119.148.16.250', 'users', '2014-01-04 23:12:36', '0');
INSERT INTO bf_activities VALUES ('176', '2', 'logged in from: 119.148.16.250', 'users', '2014-01-05 01:54:08', '0');
INSERT INTO bf_activities VALUES ('177', '7', 'logged in from: 119.148.16.250', 'users', '2014-01-05 02:51:57', '0');
INSERT INTO bf_activities VALUES ('178', '6', 'logged in from: 119.148.16.250', 'users', '2014-01-05 03:52:24', '0');

-- ----------------------------
-- Table structure for `bf_branches`
-- ----------------------------
DROP TABLE IF EXISTS `bf_branches`;
CREATE TABLE `bf_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `address` text,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `target` int(5) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_branches
-- ----------------------------
INSERT INTO bf_branches VALUES ('1', 'Mohakhali', 'Dhaka, Bangladesh', '+8801723111712', 'symphony@gmail.com', '1', '30', '2014-01-02 04:31:12', '2014-01-02 04:31:12');

-- ----------------------------
-- Table structure for `bf_categories`
-- ----------------------------
DROP TABLE IF EXISTS `bf_categories`;
CREATE TABLE `bf_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_categories
-- ----------------------------
INSERT INTO bf_categories VALUES ('1', 'Handset Servicing', 'Handset Servicing', '2', '2013-12-19 18:25:33', null);
INSERT INTO bf_categories VALUES ('2', 'Accessories Sales', 'Accessories Sales', '2', '2013-12-19 18:25:36', null);
INSERT INTO bf_categories VALUES ('3', 'Handset Delivery', 'Handset Delivery', '2', '2013-12-19 18:25:38', null);
INSERT INTO bf_categories VALUES ('4', 'Quick Solution', 'Quick Solution', '2', '2013-12-19 18:25:41', null);
INSERT INTO bf_categories VALUES ('5', 'Information', 'Information', '2', '2013-12-19 18:25:45', null);
INSERT INTO bf_categories VALUES ('6', 'handset test', 'handset test', '2', '2014-01-01 21:27:57', null);

-- ----------------------------
-- Table structure for `bf_companies`
-- ----------------------------
DROP TABLE IF EXISTS `bf_companies`;
CREATE TABLE `bf_companies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `address` text,
  `admin_id` int(11) unsigned DEFAULT NULL,
  `is_master` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `payment_account` varchar(50) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_companies
-- ----------------------------
INSERT INTO bf_companies VALUES ('1', 'Symphony', null, 'admin@gmail.com', null, '1', '1', null, '1', '2013-03-03 10:19:16', '2013-03-04 01:42:16', '0');

-- ----------------------------
-- Table structure for `bf_companies_meta`
-- ----------------------------
DROP TABLE IF EXISTS `bf_companies_meta`;
CREATE TABLE `bf_companies_meta` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned DEFAULT NULL,
  `img_path` text,
  `img_path_thumbs` text,
  `added_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_companies_meta
-- ----------------------------
INSERT INTO bf_companies_meta VALUES ('2', '2', 'logos/img-produce01-550x650.jpg', 'logos/thumbs/img-produce01-550x650.jpg', '2013-12-09 15:07:40');

-- ----------------------------
-- Table structure for `bf_counter`
-- ----------------------------
DROP TABLE IF EXISTS `bf_counter`;
CREATE TABLE `bf_counter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `description` text,
  `branch_id` int(11) DEFAULT NULL,
  `status` enum('serving','not_serving') DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_counter
-- ----------------------------
INSERT INTO bf_counter VALUES ('2', 'C-01', 'this is a description of counter one', '3', 'not_serving', '2014-01-02 04:24:59', '2013-12-24 17:38:49');
INSERT INTO bf_counter VALUES ('3', 'C-02', 'counter two descriptoin', '1', 'serving', '2014-01-05 03:54:47', '2014-01-05 03:54:47');
INSERT INTO bf_counter VALUES ('4', 'C-03', 'Counter no three', '1', 'not_serving', '2013-12-23 04:40:36', null);
INSERT INTO bf_counter VALUES ('5', 'A 1', 'counter', '1', 'not_serving', '2014-01-01 21:27:13', null);
INSERT INTO bf_counter VALUES ('6', 'C-01', 'description', '1', 'not_serving', '2014-01-02 04:32:00', null);

-- ----------------------------
-- Table structure for `bf_countries`
-- ----------------------------
DROP TABLE IF EXISTS `bf_countries`;
CREATE TABLE `bf_countries` (
  `iso` char(2) NOT NULL DEFAULT 'US',
  `name` varchar(80) NOT NULL,
  `printable_name` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`iso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_countries
-- ----------------------------
INSERT INTO bf_countries VALUES ('AD', 'ANDORRA', 'Andorra', 'AND', '20');
INSERT INTO bf_countries VALUES ('AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', '784');
INSERT INTO bf_countries VALUES ('AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', '4');
INSERT INTO bf_countries VALUES ('AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', '28');
INSERT INTO bf_countries VALUES ('AI', 'ANGUILLA', 'Anguilla', 'AIA', '660');
INSERT INTO bf_countries VALUES ('AL', 'ALBANIA', 'Albania', 'ALB', '8');
INSERT INTO bf_countries VALUES ('AM', 'ARMENIA', 'Armenia', 'ARM', '51');
INSERT INTO bf_countries VALUES ('AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', '530');
INSERT INTO bf_countries VALUES ('AO', 'ANGOLA', 'Angola', 'AGO', '24');
INSERT INTO bf_countries VALUES ('AQ', 'ANTARCTICA', 'Antarctica', null, null);
INSERT INTO bf_countries VALUES ('AR', 'ARGENTINA', 'Argentina', 'ARG', '32');
INSERT INTO bf_countries VALUES ('AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', '16');
INSERT INTO bf_countries VALUES ('AT', 'AUSTRIA', 'Austria', 'AUT', '40');
INSERT INTO bf_countries VALUES ('AU', 'AUSTRALIA', 'Australia', 'AUS', '36');
INSERT INTO bf_countries VALUES ('AW', 'ARUBA', 'Aruba', 'ABW', '533');
INSERT INTO bf_countries VALUES ('AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', '31');
INSERT INTO bf_countries VALUES ('BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', '70');
INSERT INTO bf_countries VALUES ('BB', 'BARBADOS', 'Barbados', 'BRB', '52');
INSERT INTO bf_countries VALUES ('BD', 'BANGLADESH', 'Bangladesh', 'BGD', '50');
INSERT INTO bf_countries VALUES ('BE', 'BELGIUM', 'Belgium', 'BEL', '56');
INSERT INTO bf_countries VALUES ('BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', '854');
INSERT INTO bf_countries VALUES ('BG', 'BULGARIA', 'Bulgaria', 'BGR', '100');
INSERT INTO bf_countries VALUES ('BH', 'BAHRAIN', 'Bahrain', 'BHR', '48');
INSERT INTO bf_countries VALUES ('BI', 'BURUNDI', 'Burundi', 'BDI', '108');
INSERT INTO bf_countries VALUES ('BJ', 'BENIN', 'Benin', 'BEN', '204');
INSERT INTO bf_countries VALUES ('BM', 'BERMUDA', 'Bermuda', 'BMU', '60');
INSERT INTO bf_countries VALUES ('BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', '96');
INSERT INTO bf_countries VALUES ('BO', 'BOLIVIA', 'Bolivia', 'BOL', '68');
INSERT INTO bf_countries VALUES ('BR', 'BRAZIL', 'Brazil', 'BRA', '76');
INSERT INTO bf_countries VALUES ('BS', 'BAHAMAS', 'Bahamas', 'BHS', '44');
INSERT INTO bf_countries VALUES ('BT', 'BHUTAN', 'Bhutan', 'BTN', '64');
INSERT INTO bf_countries VALUES ('BV', 'BOUVET ISLAND', 'Bouvet Island', null, null);
INSERT INTO bf_countries VALUES ('BW', 'BOTSWANA', 'Botswana', 'BWA', '72');
INSERT INTO bf_countries VALUES ('BY', 'BELARUS', 'Belarus', 'BLR', '112');
INSERT INTO bf_countries VALUES ('BZ', 'BELIZE', 'Belize', 'BLZ', '84');
INSERT INTO bf_countries VALUES ('CA', 'CANADA', 'Canada', 'CAN', '124');
INSERT INTO bf_countries VALUES ('CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', null, null);
INSERT INTO bf_countries VALUES ('CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', '180');
INSERT INTO bf_countries VALUES ('CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', '140');
INSERT INTO bf_countries VALUES ('CG', 'CONGO', 'Congo', 'COG', '178');
INSERT INTO bf_countries VALUES ('CH', 'SWITZERLAND', 'Switzerland', 'CHE', '756');
INSERT INTO bf_countries VALUES ('CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', '384');
INSERT INTO bf_countries VALUES ('CK', 'COOK ISLANDS', 'Cook Islands', 'COK', '184');
INSERT INTO bf_countries VALUES ('CL', 'CHILE', 'Chile', 'CHL', '152');
INSERT INTO bf_countries VALUES ('CM', 'CAMEROON', 'Cameroon', 'CMR', '120');
INSERT INTO bf_countries VALUES ('CN', 'CHINA', 'China', 'CHN', '156');
INSERT INTO bf_countries VALUES ('CO', 'COLOMBIA', 'Colombia', 'COL', '170');
INSERT INTO bf_countries VALUES ('CR', 'COSTA RICA', 'Costa Rica', 'CRI', '188');
INSERT INTO bf_countries VALUES ('CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', null, null);
INSERT INTO bf_countries VALUES ('CU', 'CUBA', 'Cuba', 'CUB', '192');
INSERT INTO bf_countries VALUES ('CV', 'CAPE VERDE', 'Cape Verde', 'CPV', '132');
INSERT INTO bf_countries VALUES ('CX', 'CHRISTMAS ISLAND', 'Christmas Island', null, null);
INSERT INTO bf_countries VALUES ('CY', 'CYPRUS', 'Cyprus', 'CYP', '196');
INSERT INTO bf_countries VALUES ('CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', '203');
INSERT INTO bf_countries VALUES ('DE', 'GERMANY', 'Germany', 'DEU', '276');
INSERT INTO bf_countries VALUES ('DJ', 'DJIBOUTI', 'Djibouti', 'DJI', '262');
INSERT INTO bf_countries VALUES ('DK', 'DENMARK', 'Denmark', 'DNK', '208');
INSERT INTO bf_countries VALUES ('DM', 'DOMINICA', 'Dominica', 'DMA', '212');
INSERT INTO bf_countries VALUES ('DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', '214');
INSERT INTO bf_countries VALUES ('DZ', 'ALGERIA', 'Algeria', 'DZA', '12');
INSERT INTO bf_countries VALUES ('EC', 'ECUADOR', 'Ecuador', 'ECU', '218');
INSERT INTO bf_countries VALUES ('EE', 'ESTONIA', 'Estonia', 'EST', '233');
INSERT INTO bf_countries VALUES ('EG', 'EGYPT', 'Egypt', 'EGY', '818');
INSERT INTO bf_countries VALUES ('EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', '732');
INSERT INTO bf_countries VALUES ('ER', 'ERITREA', 'Eritrea', 'ERI', '232');
INSERT INTO bf_countries VALUES ('ES', 'SPAIN', 'Spain', 'ESP', '724');
INSERT INTO bf_countries VALUES ('ET', 'ETHIOPIA', 'Ethiopia', 'ETH', '231');
INSERT INTO bf_countries VALUES ('FI', 'FINLAND', 'Finland', 'FIN', '246');
INSERT INTO bf_countries VALUES ('FJ', 'FIJI', 'Fiji', 'FJI', '242');
INSERT INTO bf_countries VALUES ('FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', '238');
INSERT INTO bf_countries VALUES ('FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', '583');
INSERT INTO bf_countries VALUES ('FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', '234');
INSERT INTO bf_countries VALUES ('FR', 'FRANCE', 'France', 'FRA', '250');
INSERT INTO bf_countries VALUES ('GA', 'GABON', 'Gabon', 'GAB', '266');
INSERT INTO bf_countries VALUES ('GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', '826');
INSERT INTO bf_countries VALUES ('GD', 'GRENADA', 'Grenada', 'GRD', '308');
INSERT INTO bf_countries VALUES ('GE', 'GEORGIA', 'Georgia', 'GEO', '268');
INSERT INTO bf_countries VALUES ('GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', '254');
INSERT INTO bf_countries VALUES ('GH', 'GHANA', 'Ghana', 'GHA', '288');
INSERT INTO bf_countries VALUES ('GI', 'GIBRALTAR', 'Gibraltar', 'GIB', '292');
INSERT INTO bf_countries VALUES ('GL', 'GREENLAND', 'Greenland', 'GRL', '304');
INSERT INTO bf_countries VALUES ('GM', 'GAMBIA', 'Gambia', 'GMB', '270');
INSERT INTO bf_countries VALUES ('GN', 'GUINEA', 'Guinea', 'GIN', '324');
INSERT INTO bf_countries VALUES ('GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', '312');
INSERT INTO bf_countries VALUES ('GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', '226');
INSERT INTO bf_countries VALUES ('GR', 'GREECE', 'Greece', 'GRC', '300');
INSERT INTO bf_countries VALUES ('GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', null, null);
INSERT INTO bf_countries VALUES ('GT', 'GUATEMALA', 'Guatemala', 'GTM', '320');
INSERT INTO bf_countries VALUES ('GU', 'GUAM', 'Guam', 'GUM', '316');
INSERT INTO bf_countries VALUES ('GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', '624');
INSERT INTO bf_countries VALUES ('GY', 'GUYANA', 'Guyana', 'GUY', '328');
INSERT INTO bf_countries VALUES ('HK', 'HONG KONG', 'Hong Kong', 'HKG', '344');
INSERT INTO bf_countries VALUES ('HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', null, null);
INSERT INTO bf_countries VALUES ('HN', 'HONDURAS', 'Honduras', 'HND', '340');
INSERT INTO bf_countries VALUES ('HR', 'CROATIA', 'Croatia', 'HRV', '191');
INSERT INTO bf_countries VALUES ('HT', 'HAITI', 'Haiti', 'HTI', '332');
INSERT INTO bf_countries VALUES ('HU', 'HUNGARY', 'Hungary', 'HUN', '348');
INSERT INTO bf_countries VALUES ('ID', 'INDONESIA', 'Indonesia', 'IDN', '360');
INSERT INTO bf_countries VALUES ('IE', 'IRELAND', 'Ireland', 'IRL', '372');
INSERT INTO bf_countries VALUES ('IL', 'ISRAEL', 'Israel', 'ISR', '376');
INSERT INTO bf_countries VALUES ('IN', 'INDIA', 'India', 'IND', '356');
INSERT INTO bf_countries VALUES ('IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', null, null);
INSERT INTO bf_countries VALUES ('IQ', 'IRAQ', 'Iraq', 'IRQ', '368');
INSERT INTO bf_countries VALUES ('IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', '364');
INSERT INTO bf_countries VALUES ('IS', 'ICELAND', 'Iceland', 'ISL', '352');
INSERT INTO bf_countries VALUES ('IT', 'ITALY', 'Italy', 'ITA', '380');
INSERT INTO bf_countries VALUES ('JM', 'JAMAICA', 'Jamaica', 'JAM', '388');
INSERT INTO bf_countries VALUES ('JO', 'JORDAN', 'Jordan', 'JOR', '400');
INSERT INTO bf_countries VALUES ('JP', 'JAPAN', 'Japan', 'JPN', '392');
INSERT INTO bf_countries VALUES ('KE', 'KENYA', 'Kenya', 'KEN', '404');
INSERT INTO bf_countries VALUES ('KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', '417');
INSERT INTO bf_countries VALUES ('KH', 'CAMBODIA', 'Cambodia', 'KHM', '116');
INSERT INTO bf_countries VALUES ('KI', 'KIRIBATI', 'Kiribati', 'KIR', '296');
INSERT INTO bf_countries VALUES ('KM', 'COMOROS', 'Comoros', 'COM', '174');
INSERT INTO bf_countries VALUES ('KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', '659');
INSERT INTO bf_countries VALUES ('KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', '408');
INSERT INTO bf_countries VALUES ('KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', '410');
INSERT INTO bf_countries VALUES ('KW', 'KUWAIT', 'Kuwait', 'KWT', '414');
INSERT INTO bf_countries VALUES ('KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', '136');
INSERT INTO bf_countries VALUES ('KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', '398');
INSERT INTO bf_countries VALUES ('LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', '418');
INSERT INTO bf_countries VALUES ('LB', 'LEBANON', 'Lebanon', 'LBN', '422');
INSERT INTO bf_countries VALUES ('LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', '662');
INSERT INTO bf_countries VALUES ('LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', '438');
INSERT INTO bf_countries VALUES ('LK', 'SRI LANKA', 'Sri Lanka', 'LKA', '144');
INSERT INTO bf_countries VALUES ('LR', 'LIBERIA', 'Liberia', 'LBR', '430');
INSERT INTO bf_countries VALUES ('LS', 'LESOTHO', 'Lesotho', 'LSO', '426');
INSERT INTO bf_countries VALUES ('LT', 'LITHUANIA', 'Lithuania', 'LTU', '440');
INSERT INTO bf_countries VALUES ('LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', '442');
INSERT INTO bf_countries VALUES ('LV', 'LATVIA', 'Latvia', 'LVA', '428');
INSERT INTO bf_countries VALUES ('LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', '434');
INSERT INTO bf_countries VALUES ('MA', 'MOROCCO', 'Morocco', 'MAR', '504');
INSERT INTO bf_countries VALUES ('MC', 'MONACO', 'Monaco', 'MCO', '492');
INSERT INTO bf_countries VALUES ('MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', '498');
INSERT INTO bf_countries VALUES ('MG', 'MADAGASCAR', 'Madagascar', 'MDG', '450');
INSERT INTO bf_countries VALUES ('MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', '584');
INSERT INTO bf_countries VALUES ('MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', '807');
INSERT INTO bf_countries VALUES ('ML', 'MALI', 'Mali', 'MLI', '466');
INSERT INTO bf_countries VALUES ('MM', 'MYANMAR', 'Myanmar', 'MMR', '104');
INSERT INTO bf_countries VALUES ('MN', 'MONGOLIA', 'Mongolia', 'MNG', '496');
INSERT INTO bf_countries VALUES ('MO', 'MACAO', 'Macao', 'MAC', '446');
INSERT INTO bf_countries VALUES ('MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', '580');
INSERT INTO bf_countries VALUES ('MQ', 'MARTINIQUE', 'Martinique', 'MTQ', '474');
INSERT INTO bf_countries VALUES ('MR', 'MAURITANIA', 'Mauritania', 'MRT', '478');
INSERT INTO bf_countries VALUES ('MS', 'MONTSERRAT', 'Montserrat', 'MSR', '500');
INSERT INTO bf_countries VALUES ('MT', 'MALTA', 'Malta', 'MLT', '470');
INSERT INTO bf_countries VALUES ('MU', 'MAURITIUS', 'Mauritius', 'MUS', '480');
INSERT INTO bf_countries VALUES ('MV', 'MALDIVES', 'Maldives', 'MDV', '462');
INSERT INTO bf_countries VALUES ('MW', 'MALAWI', 'Malawi', 'MWI', '454');
INSERT INTO bf_countries VALUES ('MX', 'MEXICO', 'Mexico', 'MEX', '484');
INSERT INTO bf_countries VALUES ('MY', 'MALAYSIA', 'Malaysia', 'MYS', '458');
INSERT INTO bf_countries VALUES ('MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', '508');
INSERT INTO bf_countries VALUES ('NA', 'NAMIBIA', 'Namibia', 'NAM', '516');
INSERT INTO bf_countries VALUES ('NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', '540');
INSERT INTO bf_countries VALUES ('NE', 'NIGER', 'Niger', 'NER', '562');
INSERT INTO bf_countries VALUES ('NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', '574');
INSERT INTO bf_countries VALUES ('NG', 'NIGERIA', 'Nigeria', 'NGA', '566');
INSERT INTO bf_countries VALUES ('NI', 'NICARAGUA', 'Nicaragua', 'NIC', '558');
INSERT INTO bf_countries VALUES ('NL', 'NETHERLANDS', 'Netherlands', 'NLD', '528');
INSERT INTO bf_countries VALUES ('NO', 'NORWAY', 'Norway', 'NOR', '578');
INSERT INTO bf_countries VALUES ('NP', 'NEPAL', 'Nepal', 'NPL', '524');
INSERT INTO bf_countries VALUES ('NR', 'NAURU', 'Nauru', 'NRU', '520');
INSERT INTO bf_countries VALUES ('NU', 'NIUE', 'Niue', 'NIU', '570');
INSERT INTO bf_countries VALUES ('NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', '554');
INSERT INTO bf_countries VALUES ('OM', 'OMAN', 'Oman', 'OMN', '512');
INSERT INTO bf_countries VALUES ('PA', 'PANAMA', 'Panama', 'PAN', '591');
INSERT INTO bf_countries VALUES ('PE', 'PERU', 'Peru', 'PER', '604');
INSERT INTO bf_countries VALUES ('PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', '258');
INSERT INTO bf_countries VALUES ('PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', '598');
INSERT INTO bf_countries VALUES ('PH', 'PHILIPPINES', 'Philippines', 'PHL', '608');
INSERT INTO bf_countries VALUES ('PK', 'PAKISTAN', 'Pakistan', 'PAK', '586');
INSERT INTO bf_countries VALUES ('PL', 'POLAND', 'Poland', 'POL', '616');
INSERT INTO bf_countries VALUES ('PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', '666');
INSERT INTO bf_countries VALUES ('PN', 'PITCAIRN', 'Pitcairn', 'PCN', '612');
INSERT INTO bf_countries VALUES ('PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', '630');
INSERT INTO bf_countries VALUES ('PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', null, null);
INSERT INTO bf_countries VALUES ('PT', 'PORTUGAL', 'Portugal', 'PRT', '620');
INSERT INTO bf_countries VALUES ('PW', 'PALAU', 'Palau', 'PLW', '585');
INSERT INTO bf_countries VALUES ('PY', 'PARAGUAY', 'Paraguay', 'PRY', '600');
INSERT INTO bf_countries VALUES ('QA', 'QATAR', 'Qatar', 'QAT', '634');
INSERT INTO bf_countries VALUES ('RE', 'REUNION', 'Reunion', 'REU', '638');
INSERT INTO bf_countries VALUES ('RO', 'ROMANIA', 'Romania', 'ROM', '642');
INSERT INTO bf_countries VALUES ('RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', '643');
INSERT INTO bf_countries VALUES ('RW', 'RWANDA', 'Rwanda', 'RWA', '646');
INSERT INTO bf_countries VALUES ('SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', '682');
INSERT INTO bf_countries VALUES ('SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', '90');
INSERT INTO bf_countries VALUES ('SC', 'SEYCHELLES', 'Seychelles', 'SYC', '690');
INSERT INTO bf_countries VALUES ('SD', 'SUDAN', 'Sudan', 'SDN', '736');
INSERT INTO bf_countries VALUES ('SE', 'SWEDEN', 'Sweden', 'SWE', '752');
INSERT INTO bf_countries VALUES ('SG', 'SINGAPORE', 'Singapore', 'SGP', '702');
INSERT INTO bf_countries VALUES ('SH', 'SAINT HELENA', 'Saint Helena', 'SHN', '654');
INSERT INTO bf_countries VALUES ('SI', 'SLOVENIA', 'Slovenia', 'SVN', '705');
INSERT INTO bf_countries VALUES ('SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', '744');
INSERT INTO bf_countries VALUES ('SK', 'SLOVAKIA', 'Slovakia', 'SVK', '703');
INSERT INTO bf_countries VALUES ('SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', '694');
INSERT INTO bf_countries VALUES ('SM', 'SAN MARINO', 'San Marino', 'SMR', '674');
INSERT INTO bf_countries VALUES ('SN', 'SENEGAL', 'Senegal', 'SEN', '686');
INSERT INTO bf_countries VALUES ('SO', 'SOMALIA', 'Somalia', 'SOM', '706');
INSERT INTO bf_countries VALUES ('SR', 'SURINAME', 'Suriname', 'SUR', '740');
INSERT INTO bf_countries VALUES ('ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', '678');
INSERT INTO bf_countries VALUES ('SV', 'EL SALVADOR', 'El Salvador', 'SLV', '222');
INSERT INTO bf_countries VALUES ('SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', '760');
INSERT INTO bf_countries VALUES ('SZ', 'SWAZILAND', 'Swaziland', 'SWZ', '748');
INSERT INTO bf_countries VALUES ('TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', '796');
INSERT INTO bf_countries VALUES ('TD', 'CHAD', 'Chad', 'TCD', '148');
INSERT INTO bf_countries VALUES ('TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', null, null);
INSERT INTO bf_countries VALUES ('TG', 'TOGO', 'Togo', 'TGO', '768');
INSERT INTO bf_countries VALUES ('TH', 'THAILAND', 'Thailand', 'THA', '764');
INSERT INTO bf_countries VALUES ('TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', '762');
INSERT INTO bf_countries VALUES ('TK', 'TOKELAU', 'Tokelau', 'TKL', '772');
INSERT INTO bf_countries VALUES ('TL', 'TIMOR-LESTE', 'Timor-Leste', null, null);
INSERT INTO bf_countries VALUES ('TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', '795');
INSERT INTO bf_countries VALUES ('TN', 'TUNISIA', 'Tunisia', 'TUN', '788');
INSERT INTO bf_countries VALUES ('TO', 'TONGA', 'Tonga', 'TON', '776');
INSERT INTO bf_countries VALUES ('TR', 'TURKEY', 'Turkey', 'TUR', '792');
INSERT INTO bf_countries VALUES ('TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', '780');
INSERT INTO bf_countries VALUES ('TV', 'TUVALU', 'Tuvalu', 'TUV', '798');
INSERT INTO bf_countries VALUES ('TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', '158');
INSERT INTO bf_countries VALUES ('TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', '834');
INSERT INTO bf_countries VALUES ('UA', 'UKRAINE', 'Ukraine', 'UKR', '804');
INSERT INTO bf_countries VALUES ('UG', 'UGANDA', 'Uganda', 'UGA', '800');
INSERT INTO bf_countries VALUES ('UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', null, null);
INSERT INTO bf_countries VALUES ('US', 'UNITED STATES', 'United States', 'USA', '840');
INSERT INTO bf_countries VALUES ('UY', 'URUGUAY', 'Uruguay', 'URY', '858');
INSERT INTO bf_countries VALUES ('UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', '860');
INSERT INTO bf_countries VALUES ('VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', '336');
INSERT INTO bf_countries VALUES ('VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', '670');
INSERT INTO bf_countries VALUES ('VE', 'VENEZUELA', 'Venezuela', 'VEN', '862');
INSERT INTO bf_countries VALUES ('VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', '92');
INSERT INTO bf_countries VALUES ('VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', '850');
INSERT INTO bf_countries VALUES ('VN', 'VIET NAM', 'Viet Nam', 'VNM', '704');
INSERT INTO bf_countries VALUES ('VU', 'VANUATU', 'Vanuatu', 'VUT', '548');
INSERT INTO bf_countries VALUES ('WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', '876');
INSERT INTO bf_countries VALUES ('WS', 'SAMOA', 'Samoa', 'WSM', '882');
INSERT INTO bf_countries VALUES ('YE', 'YEMEN', 'Yemen', 'YEM', '887');
INSERT INTO bf_countries VALUES ('YT', 'MAYOTTE', 'Mayotte', null, null);
INSERT INTO bf_countries VALUES ('ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', '710');
INSERT INTO bf_countries VALUES ('ZM', 'ZAMBIA', 'Zambia', 'ZMB', '894');
INSERT INTO bf_countries VALUES ('ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', '716');

-- ----------------------------
-- Table structure for `bf_email_queue`
-- ----------------------------
DROP TABLE IF EXISTS `bf_email_queue`;
CREATE TABLE `bf_email_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_email` varchar(128) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `alt_message` text,
  `max_attempts` int(11) NOT NULL DEFAULT '3',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  `date_published` datetime DEFAULT NULL,
  `last_attempt` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_email_queue
-- ----------------------------

-- ----------------------------
-- Table structure for `bf_login_attempts`
-- ----------------------------
DROP TABLE IF EXISTS `bf_login_attempts`;
CREATE TABLE `bf_login_attempts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) NOT NULL,
  `login` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for `bf_models`
-- ----------------------------
DROP TABLE IF EXISTS `bf_models`;
CREATE TABLE `bf_models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_models
-- ----------------------------
INSERT INTO bf_models VALUES ('1', 'Model One', 'Model One', '2013-12-23 16:36:23', '2013-12-12 11:36:03');
INSERT INTO bf_models VALUES ('2', 'Model Two', 'Model Two', '2013-12-23 16:36:34', null);
INSERT INTO bf_models VALUES ('3', 'Model Three', 'Model Three', '2013-12-23 16:36:47', null);
INSERT INTO bf_models VALUES ('4', 'Model Four', 'Model Four', '2013-12-23 16:36:55', null);
INSERT INTO bf_models VALUES ('5', 'Model Five', 'Model Five', '2013-12-23 16:37:04', null);
INSERT INTO bf_models VALUES ('6', 'test model', 'test', '2014-01-01 21:33:05', null);

-- ----------------------------
-- Table structure for `bf_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `bf_permissions`;
CREATE TABLE `bf_permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(100) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_permissions
-- ----------------------------
INSERT INTO bf_permissions VALUES ('2', 'Site.Content.View', 'Allow users to view the Content Context', 'active');
INSERT INTO bf_permissions VALUES ('3', 'Site.Reports.View', 'Allow users to view the Reports Context', 'active');
INSERT INTO bf_permissions VALUES ('4', 'Site.Settings.View', 'Allow users to view the Settings Context', 'active');
INSERT INTO bf_permissions VALUES ('5', 'Site.Developer.View', 'Allow users to view the Developer Context', 'active');
INSERT INTO bf_permissions VALUES ('6', 'Bonfire.Roles.Manage', 'Allow users to manage the user Roles', 'active');
INSERT INTO bf_permissions VALUES ('7', 'Bonfire.Users.Manage', 'Allow users to manage the site Users', 'active');
INSERT INTO bf_permissions VALUES ('8', 'Bonfire.Users.View', 'Allow users access to the User Settings', 'active');
INSERT INTO bf_permissions VALUES ('9', 'Bonfire.Users.Add', 'Allow users to add new Users', 'active');
INSERT INTO bf_permissions VALUES ('10', 'Bonfire.Database.Manage', 'Allow users to manage the Database settings', 'active');
INSERT INTO bf_permissions VALUES ('11', 'Bonfire.Emailer.Manage', 'Allow users to manage the Emailer settings', 'active');
INSERT INTO bf_permissions VALUES ('12', 'Bonfire.Logs.View', 'Allow users access to the Log details', 'active');
INSERT INTO bf_permissions VALUES ('13', 'Bonfire.Logs.Manage', 'Allow users to manage the Log files', 'active');
INSERT INTO bf_permissions VALUES ('14', 'Bonfire.Emailer.View', 'Allow users access to the Emailer settings', 'active');
INSERT INTO bf_permissions VALUES ('15', 'Site.Signin.Offline', 'Allow users to login to the site when the site is offline', 'active');
INSERT INTO bf_permissions VALUES ('16', 'Bonfire.Permissions.View', 'Allow access to view the Permissions menu unders Settings Context', 'active');
INSERT INTO bf_permissions VALUES ('17', 'Bonfire.Permissions.Manage', 'Allow access to manage the Permissions in the system', 'active');
INSERT INTO bf_permissions VALUES ('18', 'Bonfire.Roles.Delete', 'Allow users to delete user Roles', 'active');
INSERT INTO bf_permissions VALUES ('19', 'Bonfire.Modules.Add', 'Allow creation of modules with the builder.', 'active');
INSERT INTO bf_permissions VALUES ('20', 'Bonfire.Modules.Delete', 'Allow deletion of modules.', 'active');
INSERT INTO bf_permissions VALUES ('21', 'Permissions.Administrator.Manage', 'To manage the access control permissions for the Administrator role.', 'active');
INSERT INTO bf_permissions VALUES ('22', 'Permissions.Editor.Manage', 'To manage the access control permissions for the Editor role.', 'active');
INSERT INTO bf_permissions VALUES ('24', 'Permissions.User.Manage', 'To manage the access control permissions for the User role.', 'active');
INSERT INTO bf_permissions VALUES ('25', 'Permissions.Developer.Manage', 'To manage the access control permissions for the Developer role.', 'active');
INSERT INTO bf_permissions VALUES ('27', 'Activities.Own.View', 'To view the users own activity logs', 'active');
INSERT INTO bf_permissions VALUES ('28', 'Activities.Own.Delete', 'To delete the users own activity logs', 'active');
INSERT INTO bf_permissions VALUES ('29', 'Activities.User.View', 'To view the user activity logs', 'active');
INSERT INTO bf_permissions VALUES ('30', 'Activities.User.Delete', 'To delete the user activity logs, except own', 'active');
INSERT INTO bf_permissions VALUES ('31', 'Activities.Module.View', 'To view the module activity logs', 'active');
INSERT INTO bf_permissions VALUES ('32', 'Activities.Module.Delete', 'To delete the module activity logs', 'active');
INSERT INTO bf_permissions VALUES ('33', 'Activities.Date.View', 'To view the users own activity logs', 'active');
INSERT INTO bf_permissions VALUES ('34', 'Activities.Date.Delete', 'To delete the dated activity logs', 'active');
INSERT INTO bf_permissions VALUES ('35', 'Bonfire.UI.Manage', 'Manage the Bonfire UI settings', 'active');
INSERT INTO bf_permissions VALUES ('36', 'Bonfire.Settings.View', 'To view the site settings page.', 'active');
INSERT INTO bf_permissions VALUES ('37', 'Bonfire.Settings.Manage', 'To manage the site settings.', 'active');
INSERT INTO bf_permissions VALUES ('38', 'Bonfire.Activities.View', 'To view the Activities menu.', 'active');
INSERT INTO bf_permissions VALUES ('39', 'Bonfire.Database.View', 'To view the Database menu.', 'active');
INSERT INTO bf_permissions VALUES ('40', 'Bonfire.Migrations.View', 'To view the Migrations menu.', 'active');
INSERT INTO bf_permissions VALUES ('41', 'Bonfire.Builder.View', 'To view the Modulebuilder menu.', 'active');
INSERT INTO bf_permissions VALUES ('42', 'Bonfire.Roles.View', 'To view the Roles menu.', 'active');
INSERT INTO bf_permissions VALUES ('43', 'Bonfire.Sysinfo.View', 'To view the System Information page.', 'active');
INSERT INTO bf_permissions VALUES ('44', 'Bonfire.Translate.Manage', 'To manage the Language Translation.', 'active');
INSERT INTO bf_permissions VALUES ('45', 'Bonfire.Translate.View', 'To view the Language Translate menu.', 'active');
INSERT INTO bf_permissions VALUES ('46', 'Bonfire.UI.View', 'To view the UI/Keyboard Shortcut menu.', 'active');
INSERT INTO bf_permissions VALUES ('49', 'Bonfire.Profiler.View', 'To view the Console Profiler Bar.', 'active');
INSERT INTO bf_permissions VALUES ('50', 'Bonfire.Roles.Add', 'To add New Roles', 'active');
INSERT INTO bf_permissions VALUES ('51', 'Companies.Content.View', '', 'active');
INSERT INTO bf_permissions VALUES ('52', 'Companies.Content.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('53', 'Companies.Content.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('54', 'Companies.Content.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('55', 'Companies.Reports.View', '', 'active');
INSERT INTO bf_permissions VALUES ('56', 'Companies.Reports.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('57', 'Companies.Reports.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('58', 'Companies.Reports.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('59', 'Permissions.Executive.Manage', 'To manage the access control permissions for the Executive role.', 'active');
INSERT INTO bf_permissions VALUES ('76', 'Permissions.Manager.Manage', 'To manage the access control permissions for the Manager role.', 'active');
INSERT INTO bf_permissions VALUES ('77', 'Categories.Content.View', '', 'active');
INSERT INTO bf_permissions VALUES ('78', 'Categories.Content.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('79', 'Categories.Content.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('80', 'Categories.Content.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('81', 'Categories.Reports.View', '', 'active');
INSERT INTO bf_permissions VALUES ('82', 'Categories.Reports.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('83', 'Categories.Reports.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('84', 'Categories.Reports.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('85', 'Branches.Content.View', '', 'active');
INSERT INTO bf_permissions VALUES ('86', 'Branches.Content.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('87', 'Branches.Content.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('88', 'Branches.Content.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('89', 'Branches.Reports.View', '', 'active');
INSERT INTO bf_permissions VALUES ('90', 'Branches.Reports.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('91', 'Branches.Reports.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('92', 'Branches.Reports.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('93', 'Counters.Content.View', '', 'active');
INSERT INTO bf_permissions VALUES ('94', 'Counters.Content.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('95', 'Counters.Content.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('96', 'Counters.Content.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('97', 'Counters.Reports.View', '', 'active');
INSERT INTO bf_permissions VALUES ('98', 'Counters.Reports.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('99', 'Counters.Reports.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('100', 'Counters.Reports.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('101', 'Models.Content.View', '', 'active');
INSERT INTO bf_permissions VALUES ('102', 'Models.Content.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('103', 'Models.Content.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('104', 'Models.Content.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('105', 'Models.Reports.View', '', 'active');
INSERT INTO bf_permissions VALUES ('106', 'Models.Reports.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('107', 'Models.Reports.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('108', 'Models.Reports.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('117', 'Ticket.Content.View', '', 'active');
INSERT INTO bf_permissions VALUES ('118', 'Ticket.Content.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('119', 'Ticket.Content.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('120', 'Ticket.Content.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('121', 'Ticket.Reports.View', '', 'active');
INSERT INTO bf_permissions VALUES ('122', 'Ticket.Reports.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('123', 'Ticket.Reports.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('124', 'Ticket.Reports.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('125', 'Ticket_Meta.Content.View', '', 'active');
INSERT INTO bf_permissions VALUES ('126', 'Ticket_Meta.Content.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('127', 'Ticket_Meta.Content.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('128', 'Ticket_Meta.Content.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('129', 'Ticket_Meta.Reports.View', '', 'active');
INSERT INTO bf_permissions VALUES ('130', 'Ticket_Meta.Reports.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('131', 'Ticket_Meta.Reports.Edit', '', 'active');
INSERT INTO bf_permissions VALUES ('132', 'Ticket_Meta.Reports.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('133', 'Token.Content.View', '', 'active');
INSERT INTO bf_permissions VALUES ('134', 'Token.Content.Create', '', 'active');
INSERT INTO bf_permissions VALUES ('135', 'Token.Content.Delete', '', 'active');
INSERT INTO bf_permissions VALUES ('136', 'Permissions.Site Admin.Manage', 'To manage the access control permissions for the Site Admin role.', 'active');

-- ----------------------------
-- Table structure for `bf_roles`
-- ----------------------------
DROP TABLE IF EXISTS `bf_roles`;
CREATE TABLE `bf_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '1',
  `login_destination` varchar(255) NOT NULL DEFAULT '/',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `default_context` varchar(255) NOT NULL DEFAULT 'content',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_roles
-- ----------------------------
INSERT INTO bf_roles VALUES ('1', 'Administrator', 'Has full control over every aspect of the site.', '0', '0', '', '0', 'content');
INSERT INTO bf_roles VALUES ('2', 'Editor', 'Can handle day-to-day management, but does not have full power.', '0', '1', '', '0', 'content');
INSERT INTO bf_roles VALUES ('4', 'User', 'This is the default user with access to login.', '1', '0', '', '0', 'reports');
INSERT INTO bf_roles VALUES ('6', 'Developer', 'Developers typically are the only ones that can access the developer tools. Otherwise identical to Administrators, at least until the site is handed off.', '0', '1', '', '0', 'content');
INSERT INTO bf_roles VALUES ('7', 'executive', 'Serves customer from front desk', '0', '1', '', '0', 'reports');
INSERT INTO bf_roles VALUES ('8', 'Manager', 'Can manage whole system assigned to him or her', '0', '1', '', '0', 'reports');
INSERT INTO bf_roles VALUES ('9', 'Site Admin', 'Can access all data of symphony without any restriction', '0', '0', '', '0', 'reports');

-- ----------------------------
-- Table structure for `bf_role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `bf_role_permissions`;
CREATE TABLE `bf_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_role_permissions
-- ----------------------------
INSERT INTO bf_role_permissions VALUES ('1', '2');
INSERT INTO bf_role_permissions VALUES ('1', '3');
INSERT INTO bf_role_permissions VALUES ('1', '4');
INSERT INTO bf_role_permissions VALUES ('1', '5');
INSERT INTO bf_role_permissions VALUES ('1', '6');
INSERT INTO bf_role_permissions VALUES ('1', '7');
INSERT INTO bf_role_permissions VALUES ('1', '8');
INSERT INTO bf_role_permissions VALUES ('1', '9');
INSERT INTO bf_role_permissions VALUES ('1', '10');
INSERT INTO bf_role_permissions VALUES ('1', '11');
INSERT INTO bf_role_permissions VALUES ('1', '12');
INSERT INTO bf_role_permissions VALUES ('1', '13');
INSERT INTO bf_role_permissions VALUES ('1', '14');
INSERT INTO bf_role_permissions VALUES ('1', '15');
INSERT INTO bf_role_permissions VALUES ('1', '16');
INSERT INTO bf_role_permissions VALUES ('1', '17');
INSERT INTO bf_role_permissions VALUES ('1', '18');
INSERT INTO bf_role_permissions VALUES ('1', '19');
INSERT INTO bf_role_permissions VALUES ('1', '20');
INSERT INTO bf_role_permissions VALUES ('1', '21');
INSERT INTO bf_role_permissions VALUES ('1', '22');
INSERT INTO bf_role_permissions VALUES ('1', '24');
INSERT INTO bf_role_permissions VALUES ('1', '25');
INSERT INTO bf_role_permissions VALUES ('1', '27');
INSERT INTO bf_role_permissions VALUES ('1', '28');
INSERT INTO bf_role_permissions VALUES ('1', '29');
INSERT INTO bf_role_permissions VALUES ('1', '30');
INSERT INTO bf_role_permissions VALUES ('1', '31');
INSERT INTO bf_role_permissions VALUES ('1', '32');
INSERT INTO bf_role_permissions VALUES ('1', '33');
INSERT INTO bf_role_permissions VALUES ('1', '34');
INSERT INTO bf_role_permissions VALUES ('1', '35');
INSERT INTO bf_role_permissions VALUES ('1', '36');
INSERT INTO bf_role_permissions VALUES ('1', '37');
INSERT INTO bf_role_permissions VALUES ('1', '38');
INSERT INTO bf_role_permissions VALUES ('1', '39');
INSERT INTO bf_role_permissions VALUES ('1', '40');
INSERT INTO bf_role_permissions VALUES ('1', '41');
INSERT INTO bf_role_permissions VALUES ('1', '42');
INSERT INTO bf_role_permissions VALUES ('1', '43');
INSERT INTO bf_role_permissions VALUES ('1', '44');
INSERT INTO bf_role_permissions VALUES ('1', '45');
INSERT INTO bf_role_permissions VALUES ('1', '46');
INSERT INTO bf_role_permissions VALUES ('1', '49');
INSERT INTO bf_role_permissions VALUES ('1', '50');
INSERT INTO bf_role_permissions VALUES ('1', '51');
INSERT INTO bf_role_permissions VALUES ('1', '52');
INSERT INTO bf_role_permissions VALUES ('1', '53');
INSERT INTO bf_role_permissions VALUES ('1', '54');
INSERT INTO bf_role_permissions VALUES ('1', '55');
INSERT INTO bf_role_permissions VALUES ('1', '56');
INSERT INTO bf_role_permissions VALUES ('1', '57');
INSERT INTO bf_role_permissions VALUES ('1', '58');
INSERT INTO bf_role_permissions VALUES ('1', '59');
INSERT INTO bf_role_permissions VALUES ('1', '76');
INSERT INTO bf_role_permissions VALUES ('1', '77');
INSERT INTO bf_role_permissions VALUES ('1', '78');
INSERT INTO bf_role_permissions VALUES ('1', '79');
INSERT INTO bf_role_permissions VALUES ('1', '80');
INSERT INTO bf_role_permissions VALUES ('1', '81');
INSERT INTO bf_role_permissions VALUES ('1', '82');
INSERT INTO bf_role_permissions VALUES ('1', '83');
INSERT INTO bf_role_permissions VALUES ('1', '84');
INSERT INTO bf_role_permissions VALUES ('1', '85');
INSERT INTO bf_role_permissions VALUES ('1', '86');
INSERT INTO bf_role_permissions VALUES ('1', '87');
INSERT INTO bf_role_permissions VALUES ('1', '88');
INSERT INTO bf_role_permissions VALUES ('1', '89');
INSERT INTO bf_role_permissions VALUES ('1', '90');
INSERT INTO bf_role_permissions VALUES ('1', '91');
INSERT INTO bf_role_permissions VALUES ('1', '92');
INSERT INTO bf_role_permissions VALUES ('1', '93');
INSERT INTO bf_role_permissions VALUES ('1', '94');
INSERT INTO bf_role_permissions VALUES ('1', '95');
INSERT INTO bf_role_permissions VALUES ('1', '96');
INSERT INTO bf_role_permissions VALUES ('1', '97');
INSERT INTO bf_role_permissions VALUES ('1', '98');
INSERT INTO bf_role_permissions VALUES ('1', '99');
INSERT INTO bf_role_permissions VALUES ('1', '100');
INSERT INTO bf_role_permissions VALUES ('1', '101');
INSERT INTO bf_role_permissions VALUES ('1', '102');
INSERT INTO bf_role_permissions VALUES ('1', '103');
INSERT INTO bf_role_permissions VALUES ('1', '104');
INSERT INTO bf_role_permissions VALUES ('1', '105');
INSERT INTO bf_role_permissions VALUES ('1', '106');
INSERT INTO bf_role_permissions VALUES ('1', '107');
INSERT INTO bf_role_permissions VALUES ('1', '108');
INSERT INTO bf_role_permissions VALUES ('1', '117');
INSERT INTO bf_role_permissions VALUES ('1', '118');
INSERT INTO bf_role_permissions VALUES ('1', '119');
INSERT INTO bf_role_permissions VALUES ('1', '120');
INSERT INTO bf_role_permissions VALUES ('1', '121');
INSERT INTO bf_role_permissions VALUES ('1', '122');
INSERT INTO bf_role_permissions VALUES ('1', '123');
INSERT INTO bf_role_permissions VALUES ('1', '124');
INSERT INTO bf_role_permissions VALUES ('1', '125');
INSERT INTO bf_role_permissions VALUES ('1', '126');
INSERT INTO bf_role_permissions VALUES ('1', '127');
INSERT INTO bf_role_permissions VALUES ('1', '128');
INSERT INTO bf_role_permissions VALUES ('1', '129');
INSERT INTO bf_role_permissions VALUES ('1', '130');
INSERT INTO bf_role_permissions VALUES ('1', '131');
INSERT INTO bf_role_permissions VALUES ('1', '132');
INSERT INTO bf_role_permissions VALUES ('1', '133');
INSERT INTO bf_role_permissions VALUES ('1', '134');
INSERT INTO bf_role_permissions VALUES ('1', '135');
INSERT INTO bf_role_permissions VALUES ('1', '136');
INSERT INTO bf_role_permissions VALUES ('2', '2');
INSERT INTO bf_role_permissions VALUES ('2', '3');
INSERT INTO bf_role_permissions VALUES ('4', '2');
INSERT INTO bf_role_permissions VALUES ('4', '51');
INSERT INTO bf_role_permissions VALUES ('4', '77');
INSERT INTO bf_role_permissions VALUES ('4', '85');
INSERT INTO bf_role_permissions VALUES ('4', '93');
INSERT INTO bf_role_permissions VALUES ('4', '101');
INSERT INTO bf_role_permissions VALUES ('4', '117');
INSERT INTO bf_role_permissions VALUES ('6', '2');
INSERT INTO bf_role_permissions VALUES ('6', '3');
INSERT INTO bf_role_permissions VALUES ('6', '4');
INSERT INTO bf_role_permissions VALUES ('6', '5');
INSERT INTO bf_role_permissions VALUES ('6', '6');
INSERT INTO bf_role_permissions VALUES ('6', '7');
INSERT INTO bf_role_permissions VALUES ('6', '8');
INSERT INTO bf_role_permissions VALUES ('6', '9');
INSERT INTO bf_role_permissions VALUES ('6', '10');
INSERT INTO bf_role_permissions VALUES ('6', '11');
INSERT INTO bf_role_permissions VALUES ('6', '12');
INSERT INTO bf_role_permissions VALUES ('6', '13');
INSERT INTO bf_role_permissions VALUES ('6', '49');
INSERT INTO bf_role_permissions VALUES ('7', '2');
INSERT INTO bf_role_permissions VALUES ('7', '3');
INSERT INTO bf_role_permissions VALUES ('7', '51');
INSERT INTO bf_role_permissions VALUES ('7', '77');
INSERT INTO bf_role_permissions VALUES ('7', '85');
INSERT INTO bf_role_permissions VALUES ('7', '93');
INSERT INTO bf_role_permissions VALUES ('7', '101');
INSERT INTO bf_role_permissions VALUES ('7', '117');
INSERT INTO bf_role_permissions VALUES ('7', '121');
INSERT INTO bf_role_permissions VALUES ('8', '2');
INSERT INTO bf_role_permissions VALUES ('8', '3');
INSERT INTO bf_role_permissions VALUES ('8', '51');
INSERT INTO bf_role_permissions VALUES ('8', '52');
INSERT INTO bf_role_permissions VALUES ('8', '55');
INSERT INTO bf_role_permissions VALUES ('8', '77');
INSERT INTO bf_role_permissions VALUES ('8', '78');
INSERT INTO bf_role_permissions VALUES ('8', '80');
INSERT INTO bf_role_permissions VALUES ('8', '85');
INSERT INTO bf_role_permissions VALUES ('8', '93');
INSERT INTO bf_role_permissions VALUES ('8', '94');
INSERT INTO bf_role_permissions VALUES ('8', '96');
INSERT INTO bf_role_permissions VALUES ('8', '101');
INSERT INTO bf_role_permissions VALUES ('8', '102');
INSERT INTO bf_role_permissions VALUES ('8', '104');
INSERT INTO bf_role_permissions VALUES ('8', '117');
INSERT INTO bf_role_permissions VALUES ('8', '120');
INSERT INTO bf_role_permissions VALUES ('8', '121');
INSERT INTO bf_role_permissions VALUES ('9', '2');
INSERT INTO bf_role_permissions VALUES ('9', '3');
INSERT INTO bf_role_permissions VALUES ('9', '24');
INSERT INTO bf_role_permissions VALUES ('9', '51');
INSERT INTO bf_role_permissions VALUES ('9', '52');
INSERT INTO bf_role_permissions VALUES ('9', '53');
INSERT INTO bf_role_permissions VALUES ('9', '54');
INSERT INTO bf_role_permissions VALUES ('9', '59');
INSERT INTO bf_role_permissions VALUES ('9', '76');
INSERT INTO bf_role_permissions VALUES ('9', '77');
INSERT INTO bf_role_permissions VALUES ('9', '78');
INSERT INTO bf_role_permissions VALUES ('9', '79');
INSERT INTO bf_role_permissions VALUES ('9', '80');
INSERT INTO bf_role_permissions VALUES ('9', '85');
INSERT INTO bf_role_permissions VALUES ('9', '86');
INSERT INTO bf_role_permissions VALUES ('9', '87');
INSERT INTO bf_role_permissions VALUES ('9', '88');
INSERT INTO bf_role_permissions VALUES ('9', '89');
INSERT INTO bf_role_permissions VALUES ('9', '93');
INSERT INTO bf_role_permissions VALUES ('9', '94');
INSERT INTO bf_role_permissions VALUES ('9', '95');
INSERT INTO bf_role_permissions VALUES ('9', '96');
INSERT INTO bf_role_permissions VALUES ('9', '97');
INSERT INTO bf_role_permissions VALUES ('9', '101');
INSERT INTO bf_role_permissions VALUES ('9', '102');
INSERT INTO bf_role_permissions VALUES ('9', '103');
INSERT INTO bf_role_permissions VALUES ('9', '104');
INSERT INTO bf_role_permissions VALUES ('9', '117');
INSERT INTO bf_role_permissions VALUES ('9', '118');
INSERT INTO bf_role_permissions VALUES ('9', '119');
INSERT INTO bf_role_permissions VALUES ('9', '121');

-- ----------------------------
-- Table structure for `bf_schema_version`
-- ----------------------------
DROP TABLE IF EXISTS `bf_schema_version`;
CREATE TABLE `bf_schema_version` (
  `type` varchar(40) NOT NULL,
  `version` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_schema_version
-- ----------------------------
INSERT INTO bf_schema_version VALUES ('api_', '1');
INSERT INTO bf_schema_version VALUES ('branches_', '1');
INSERT INTO bf_schema_version VALUES ('categories_', '1');
INSERT INTO bf_schema_version VALUES ('companies_', '1');
INSERT INTO bf_schema_version VALUES ('core', '37');
INSERT INTO bf_schema_version VALUES ('counters_', '1');
INSERT INTO bf_schema_version VALUES ('mainsite_', '1');
INSERT INTO bf_schema_version VALUES ('models_', '1');
INSERT INTO bf_schema_version VALUES ('ticket_', '1');
INSERT INTO bf_schema_version VALUES ('ticket_meta_', '1');
INSERT INTO bf_schema_version VALUES ('token_', '1');

-- ----------------------------
-- Table structure for `bf_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `bf_sessions`;
CREATE TABLE `bf_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_sessions
-- ----------------------------

-- ----------------------------
-- Table structure for `bf_settings`
-- ----------------------------
DROP TABLE IF EXISTS `bf_settings`;
CREATE TABLE `bf_settings` (
  `name` varchar(30) NOT NULL,
  `module` varchar(50) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_settings
-- ----------------------------
INSERT INTO bf_settings VALUES ('auth.allow_name_change', 'core', '1');
INSERT INTO bf_settings VALUES ('auth.allow_register', 'core', '1');
INSERT INTO bf_settings VALUES ('auth.allow_remember', 'core', '1');
INSERT INTO bf_settings VALUES ('auth.do_login_redirect', 'core', '1');
INSERT INTO bf_settings VALUES ('auth.login_type', 'core', 'both');
INSERT INTO bf_settings VALUES ('auth.name_change_frequency', 'core', '1');
INSERT INTO bf_settings VALUES ('auth.name_change_limit', 'core', '1');
INSERT INTO bf_settings VALUES ('auth.password_force_mixed_case', 'core', '0');
INSERT INTO bf_settings VALUES ('auth.password_force_numbers', 'core', '0');
INSERT INTO bf_settings VALUES ('auth.password_force_symbols', 'core', '0');
INSERT INTO bf_settings VALUES ('auth.password_min_length', 'core', '8');
INSERT INTO bf_settings VALUES ('auth.password_show_labels', 'core', '0');
INSERT INTO bf_settings VALUES ('auth.remember_length', 'core', '1209600');
INSERT INTO bf_settings VALUES ('auth.user_activation_method', 'core', '0');
INSERT INTO bf_settings VALUES ('auth.use_extended_profile', 'core', '0');
INSERT INTO bf_settings VALUES ('auth.use_usernames', 'core', '1');
INSERT INTO bf_settings VALUES ('ext.country', 'core', 'BD');
INSERT INTO bf_settings VALUES ('ext.state', 'core', 'CA');
INSERT INTO bf_settings VALUES ('ext.street_name', 'core', 'Dhaka');
INSERT INTO bf_settings VALUES ('ext.type', 'core', 'small');
INSERT INTO bf_settings VALUES ('form_save', 'core.ui', 'ctrl+s/⌘+s');
INSERT INTO bf_settings VALUES ('goto_content', 'core.ui', 'alt+c');
INSERT INTO bf_settings VALUES ('mailpath', 'email', '/usr/sbin/sendmail');
INSERT INTO bf_settings VALUES ('mailtype', 'email', 'text');
INSERT INTO bf_settings VALUES ('password_iterations', 'users', '2');
INSERT INTO bf_settings VALUES ('protocol', 'email', 'mail');
INSERT INTO bf_settings VALUES ('sender_email', 'email', '');
INSERT INTO bf_settings VALUES ('site.languages', 'core', 'a:1:{i:0;s:7:\"english\";}');
INSERT INTO bf_settings VALUES ('site.list_limit', 'core', '25');
INSERT INTO bf_settings VALUES ('site.show_front_profiler', 'core', '0');
INSERT INTO bf_settings VALUES ('site.show_profiler', 'core', '0');
INSERT INTO bf_settings VALUES ('site.status', 'core', '1');
INSERT INTO bf_settings VALUES ('site.system_email', 'core', 'admin@progmaatic.com');
INSERT INTO bf_settings VALUES ('site.title', 'core', 'Symphony Queue Management System');
INSERT INTO bf_settings VALUES ('smtp_host', 'email', '');
INSERT INTO bf_settings VALUES ('smtp_pass', 'email', '');
INSERT INTO bf_settings VALUES ('smtp_port', 'email', '');
INSERT INTO bf_settings VALUES ('smtp_timeout', 'email', '');
INSERT INTO bf_settings VALUES ('smtp_user', 'email', '');
INSERT INTO bf_settings VALUES ('updates.bleeding_edge', 'core', '0');
INSERT INTO bf_settings VALUES ('updates.do_check', 'core', '0');

-- ----------------------------
-- Table structure for `bf_states`
-- ----------------------------
DROP TABLE IF EXISTS `bf_states`;
CREATE TABLE `bf_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL,
  `abbrev` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_states
-- ----------------------------
INSERT INTO bf_states VALUES ('1', 'Alaska', 'AK');
INSERT INTO bf_states VALUES ('2', 'Alabama', 'AL');
INSERT INTO bf_states VALUES ('3', 'American Samoa', 'AS');
INSERT INTO bf_states VALUES ('4', 'Arizona', 'AZ');
INSERT INTO bf_states VALUES ('5', 'Arkansas', 'AR');
INSERT INTO bf_states VALUES ('6', 'California', 'CA');
INSERT INTO bf_states VALUES ('7', 'Colorado', 'CO');
INSERT INTO bf_states VALUES ('8', 'Connecticut', 'CT');
INSERT INTO bf_states VALUES ('9', 'Delaware', 'DE');
INSERT INTO bf_states VALUES ('10', 'District of Columbia', 'DC');
INSERT INTO bf_states VALUES ('11', 'Florida', 'FL');
INSERT INTO bf_states VALUES ('12', 'Georgia', 'GA');
INSERT INTO bf_states VALUES ('13', 'Guam', 'GU');
INSERT INTO bf_states VALUES ('14', 'Hawaii', 'HI');
INSERT INTO bf_states VALUES ('15', 'Idaho', 'ID');
INSERT INTO bf_states VALUES ('16', 'Illinois', 'IL');
INSERT INTO bf_states VALUES ('17', 'Indiana', 'IN');
INSERT INTO bf_states VALUES ('18', 'Iowa', 'IA');
INSERT INTO bf_states VALUES ('19', 'Kansas', 'KS');
INSERT INTO bf_states VALUES ('20', 'Kentucky', 'KY');
INSERT INTO bf_states VALUES ('21', 'Louisiana', 'LA');
INSERT INTO bf_states VALUES ('22', 'Maine', 'ME');
INSERT INTO bf_states VALUES ('23', 'Marshall Islands', 'MH');
INSERT INTO bf_states VALUES ('24', 'Maryland', 'MD');
INSERT INTO bf_states VALUES ('25', 'Massachusetts', 'MA');
INSERT INTO bf_states VALUES ('26', 'Michigan', 'MI');
INSERT INTO bf_states VALUES ('27', 'Minnesota', 'MN');
INSERT INTO bf_states VALUES ('28', 'Mississippi', 'MS');
INSERT INTO bf_states VALUES ('29', 'Missouri', 'MO');
INSERT INTO bf_states VALUES ('30', 'Montana', 'MT');
INSERT INTO bf_states VALUES ('31', 'Nebraska', 'NE');
INSERT INTO bf_states VALUES ('32', 'Nevada', 'NV');
INSERT INTO bf_states VALUES ('33', 'New Hampshire', 'NH');
INSERT INTO bf_states VALUES ('34', 'New Jersey', 'NJ');
INSERT INTO bf_states VALUES ('35', 'New Mexico', 'NM');
INSERT INTO bf_states VALUES ('36', 'New York', 'NY');
INSERT INTO bf_states VALUES ('37', 'North Carolina', 'NC');
INSERT INTO bf_states VALUES ('38', 'North Dakota', 'ND');
INSERT INTO bf_states VALUES ('39', 'Northern Mariana Islands', 'MP');
INSERT INTO bf_states VALUES ('40', 'Ohio', 'OH');
INSERT INTO bf_states VALUES ('41', 'Oklahoma', 'OK');
INSERT INTO bf_states VALUES ('42', 'Oregon', 'OR');
INSERT INTO bf_states VALUES ('43', 'Palau', 'PW');
INSERT INTO bf_states VALUES ('44', 'Pennsylvania', 'PA');
INSERT INTO bf_states VALUES ('45', 'Puerto Rico', 'PR');
INSERT INTO bf_states VALUES ('46', 'Rhode Island', 'RI');
INSERT INTO bf_states VALUES ('47', 'South Carolina', 'SC');
INSERT INTO bf_states VALUES ('48', 'South Dakota', 'SD');
INSERT INTO bf_states VALUES ('49', 'Tennessee', 'TN');
INSERT INTO bf_states VALUES ('50', 'Texas', 'TX');
INSERT INTO bf_states VALUES ('51', 'Utah', 'UT');
INSERT INTO bf_states VALUES ('52', 'Vermont', 'VT');
INSERT INTO bf_states VALUES ('53', 'Virgin Islands', 'VI');
INSERT INTO bf_states VALUES ('54', 'Virginia', 'VA');
INSERT INTO bf_states VALUES ('55', 'Washington', 'WA');
INSERT INTO bf_states VALUES ('56', 'West Virginia', 'WV');
INSERT INTO bf_states VALUES ('57', 'Wisconsin', 'WI');
INSERT INTO bf_states VALUES ('58', 'Wyoming', 'WY');
INSERT INTO bf_states VALUES ('59', 'Armed Forces Africa', 'AE');
INSERT INTO bf_states VALUES ('60', 'Armed Forces Canada', 'AE');
INSERT INTO bf_states VALUES ('61', 'Armed Forces Europe', 'AE');
INSERT INTO bf_states VALUES ('62', 'Armed Forces Middle East', 'AE');
INSERT INTO bf_states VALUES ('63', 'Armed Forces Pacific', 'AP');

-- ----------------------------
-- Table structure for `bf_ticket`
-- ----------------------------
DROP TABLE IF EXISTS `bf_ticket`;
CREATE TABLE `bf_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lso` varchar(150) DEFAULT NULL,
  `customer_name` varchar(50) DEFAULT NULL,
  `customer_address` text,
  `model_id` int(11) DEFAULT NULL,
  `service_type` varchar(20) DEFAULT NULL,
  `counter_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` enum('pending','serving','finished') DEFAULT 'pending',
  `comments` text,
  `seen` tinyint(4) DEFAULT '0',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_on` datetime DEFAULT '0000-00-00 00:00:00',
  `finished_on` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_ticket
-- ----------------------------
INSERT INTO bf_ticket VALUES ('7', '1', 'eeee', 'eeee', '4', 'eeee', '2', '1', '5', 'finished', 'This is a comment', '1', '2013-12-23 15:36:44', '2013-12-23 16:59:58', '2013-12-23 17:00:25');
INSERT INTO bf_ticket VALUES ('8', '1', 'aaaa', 'aaaa', '5', 'aaaa', '2', '1', '5', 'finished', 'dasdasd', '1', '2013-12-23 15:39:22', '2013-12-23 17:27:30', '2013-12-23 17:27:36');
INSERT INTO bf_ticket VALUES ('9', '1', 'aaaa', 'aaaa', '5', 'aaaa', '3', '1', '5', 'finished', 'This is a comment', '1', '2013-12-23 15:39:24', '2013-12-23 17:29:24', '2013-12-23 17:31:23');
INSERT INTO bf_ticket VALUES ('11', '1', 'aaaa', 'aaaa', '5', 'aaaa', '3', '1', '5', 'finished', 'dfgfgdfg f', '1', '2013-12-23 15:39:26', '2013-12-23 17:31:53', '2013-12-23 17:35:04');
INSERT INTO bf_ticket VALUES ('12', '1', 'aaaa', 'aaaa', '5', 'aaaa', '5', '1', '5', 'pending', null, '1', '2013-12-23 15:39:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('13', '1', 'aaaa', 'aaaa', '5', 'aaaa', '3', '1', '5', 'finished', 'sdkjhdskjf kj kjdfh s', '1', '2013-12-23 15:39:26', '2013-12-23 17:35:31', '2013-12-30 11:02:37');
INSERT INTO bf_ticket VALUES ('14', '1', 'aaaa', 'aaaa', '5', 'aaaa', '3', '1', '5', 'serving', null, '1', '2013-12-23 15:39:26', '2014-01-02 04:04:28', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('15', '99', 'aaaa', 'aaaa', '5', 'aaaa', '3', '1', '5', 'finished', 'sd fdsfdss dfds', '1', '2013-12-23 16:02:48', '2013-12-30 11:02:47', '2013-12-30 06:21:21');
INSERT INTO bf_ticket VALUES ('16', '12212', 'aaaa', 'aaaa', '2', 'aaaa', '5', '1', '2', 'serving', null, '1', '2013-12-23 16:57:53', '2013-12-30 06:21:12', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('17', '1', 'aaaa', 'aaaa', '1', 'aaaa', '2', '1', '2', 'finished', 'hghj ghjghjhg h ', '1', '2013-12-24 11:33:10', '2013-12-24 11:38:07', '2013-12-24 11:39:29');
INSERT INTO bf_ticket VALUES ('18', '2', 'aaaa', 'aaaa', '1', 'aaaa', '3', '1', '2', 'finished', 'asdas ds', '1', '2013-12-24 11:33:13', '2013-12-24 11:34:02', '2013-12-24 11:34:11');
INSERT INTO bf_ticket VALUES ('19', '2', 'bbaaaa', 'aaaa', '1', 'aaaa', '2', '1', '2', 'finished', 'hjghg hj yj j jj j hjghj ghj', '1', '2013-12-24 11:33:19', '2013-12-24 11:39:49', '2013-12-24 11:40:27');
INSERT INTO bf_ticket VALUES ('20', '2', 'ccaaaa', 'aaaa', '1', 'aaaa', '3', '1', '2', 'finished', 'asd asdasdasda ', '1', '2013-12-24 11:33:23', '2013-12-24 11:34:06', '2013-12-24 11:34:18');
INSERT INTO bf_ticket VALUES ('21', '2', 'ddaaaa', 'aaaa', '1', 'aaaa', '2', '1', '2', 'finished', 's dadsadasd asd', '1', '2013-12-24 11:33:26', '2013-12-24 17:09:18', '2013-12-24 17:36:47');
INSERT INTO bf_ticket VALUES ('22', '2', 'eeaaa', 'aaaa', '1', 'aaaa', '3', '1', '2', 'finished', 'asds ad asdasdasda', '1', '2013-12-24 11:33:29', '2013-12-24 11:34:08', '2013-12-24 11:34:24');
INSERT INTO bf_ticket VALUES ('25', 'lso', 'Customer name', 'Dhaka, Bangladesh', '5', 'service', '2', '3', '4', 'pending', null, '1', '2013-12-26 10:46:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('26', 'lso', 'Customer name', 'Dhaka, Bangladesh', '5', '0', '3', '1', '4', 'pending', null, '1', '2013-12-26 11:05:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('27', 'lso', 'Customer name', 'Dhaka, Bangladesh', '5', '', '2', '1', '4', 'pending', null, '1', '2013-12-26 11:16:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('28', '12', 'sda dasd ', 'asd sa', '1', '', '3', '1', '2', 'pending', null, '1', '2013-12-26 11:26:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('29', '30000', 'Customer name', 'Customer Address', '1', null, '2', '1', '1', 'pending', null, '1', '2013-12-26 16:47:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('30', '30000', 'Customer name', 'Customer Address', '2', null, '3', '1', '2', 'pending', null, '1', '2013-12-26 16:47:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('31', '30000', 'Customer name', 'Customer Address', '3', null, '2', '1', '3', 'pending', null, '1', '2013-12-26 16:47:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('32', '30000', 'Customer name', 'Customer Address', '4', null, '3', '1', '4', 'pending', null, '1', '2013-12-26 16:47:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('33', '30000', 'Customer name', 'Customer Address', '5', null, '2', '1', '5', 'pending', null, '1', '2013-12-26 16:47:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('34', '30000', 'Customer name', 'Customer Address', '5', null, '3', '1', '4', 'pending', null, '1', '2013-12-26 16:47:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('35', '30000', 'Customer name', 'Customer Address', '5', null, '2', '1', '3', 'pending', null, '1', '2013-12-26 16:47:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('36', '30000', 'Customer name', 'Customer Address', '5', null, '3', '1', '2', 'pending', null, '1', '2013-12-26 16:47:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('37', '30000', 'Customer name', 'Customer Address', '5', null, '2', '1', '1', 'pending', null, '1', '2013-12-26 16:47:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('38', '30000', 'Customer name', 'Customer Address', '2', null, '3', '1', '1', 'finished', 'as dasd asdasda', '1', '2013-12-26 16:47:56', '2014-01-02 02:48:10', '2014-01-02 04:03:51');
INSERT INTO bf_ticket VALUES ('39', '30000', 'Customer name', 'Customer Address', '3', null, '2', '1', '1', 'pending', null, '1', '2013-12-26 16:47:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('40', '30000', 'Customer name', 'Customer Address', '4', null, '3', '1', '1', 'pending', null, '1', '2013-12-26 16:48:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('41', '30000', 'Customer name', 'Customer Address', '5', null, '2', '1', '1', 'pending', null, '1', '2013-12-26 16:48:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('42', '30000', 'Customer name', 'Customer Address', '5', null, '3', '1', '1', 'pending', null, '1', '2013-12-26 16:48:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('43', '30000', 'Customer name', 'Customer Address', '5', null, '2', '1', '1', 'pending', null, '1', '2013-12-26 16:48:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('44', '30000', 'Customer name', 'Customer Address', '4', null, '3', '1', '1', 'pending', null, '1', '2013-12-26 16:48:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('45', '30000', 'Customer name', 'Customer Address', '3', null, '2', '1', '1', 'pending', null, '1', '2013-12-26 16:48:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('46', '30000', 'Customer name', 'Customer Address', '2', null, '3', '1', '1', 'pending', null, '1', '2013-12-26 16:48:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('47', '30000', 'Customer name', 'Customer Address', '2', null, '2', '1', '2', 'pending', null, '1', '2013-12-26 16:48:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('48', '30000', 'Customer name', 'Customer Address', '3', null, '3', '1', '2', 'pending', null, '1', '2013-12-26 16:48:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('49', '30000', 'Customer name', 'Customer Address', '4', null, '2', '1', '2', 'pending', null, '1', '2013-12-26 16:48:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('50', '30000', 'Customer name', 'Customer Address', '5', null, '3', '1', '2', 'pending', null, '1', '2013-12-26 16:48:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('51', '30000', 'Customer name', 'Customer Address', '5', null, '2', '1', '3', 'pending', null, '1', '2013-12-26 16:48:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('52', '30000', 'Customer name', 'Customer Address', '2', null, '3', '1', '3', 'pending', null, '1', '2013-12-26 16:48:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('53', '30000', 'Customer name', 'Customer Address', '3', null, '2', '1', '3', 'pending', null, '1', '2013-12-26 16:48:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('54', '30000', 'Customer name', 'Customer Address', '4', null, '3', '1', '3', 'pending', null, '1', '2013-12-26 16:48:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('55', '30000', 'Customer name', 'Customer Address', '5', null, '2', '1', '3', 'pending', null, '1', '2013-12-26 16:48:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('56', '30000', 'Customer name', 'Customer Address', '1', null, '3', '1', '4', 'pending', null, '1', '2013-12-26 16:49:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('57', '30000', 'Customer name', 'Customer Address', '2', null, '2', '1', '4', 'pending', null, '1', '2013-12-26 16:49:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('58', '30000', 'Customer name', 'Customer Address', '3', null, '3', '1', '4', 'pending', null, '1', '2013-12-26 16:49:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('59', '30000', 'Customer name', 'Customer Address', '4', null, '2', '1', '4', 'pending', null, '1', '2013-12-26 16:49:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('60', '30000', 'Customer name', 'Customer Address', '5', null, '3', '1', '4', 'pending', null, '1', '2013-12-26 16:49:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('61', '30000', 'Customer name', 'Customer Address', '2', null, '2', '1', '5', 'pending', null, '1', '2013-12-26 16:49:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('62', '30000', 'Customer name', 'Customer Address', '3', null, '3', '1', '5', 'pending', null, '1', '2013-12-26 16:49:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('63', '30000', 'Customer name', 'Customer Address', '4', null, '2', '1', '5', 'pending', null, '1', '2013-12-26 16:49:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('64', '30000', 'Customer name', 'Customer Address', '5', null, '3', '1', '5', 'pending', null, '1', '2013-12-26 16:49:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('65', '30000', 'Customer name', 'Customer Address', '5', null, '2', '1', '5', 'pending', null, '1', '2013-12-26 16:49:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('66', '30000', 'Customer name', 'Customer Address', '5', null, '3', '1', '5', 'pending', null, '1', '2013-12-26 16:49:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('67', '30000', 'Customer name', 'Customer Address', '5', null, '2', '1', '5', 'pending', null, '1', '2013-12-26 16:49:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('68', '30000', 'Customer name a', 'Customer Address a', '5', null, '3', '1', '1', 'pending', null, '1', '2013-12-26 16:49:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('69', '30000', 'Customer name a', 'Customer Address a', '2', null, '2', '1', '1', 'pending', null, '1', '2013-12-26 16:49:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('70', '30000', 'Customer name a', 'Customer Address a', '3', null, '3', '1', '5', 'pending', null, '1', '2013-12-26 16:49:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('71', '30000', 'Customer name a', 'Customer Address a', '3', null, '2', '1', '1', 'pending', null, '1', '2013-12-26 16:50:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('72', '30000', 'Customer name a', 'Customer Address a', '3', null, '3', '1', '2', 'pending', null, '1', '2013-12-26 16:50:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('73', '1', 'sumon', '12512412', '1', null, '2', '1', '3', 'pending', null, '1', '2013-12-30 06:11:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('74', '1', 'sumon', '12512412', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 06:12:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('75', '1', 'sumon', '12512412', '1', null, '2', '1', '3', 'pending', null, '1', '2013-12-30 06:12:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('76', '1', 'sumon', '12512412', '1', null, '3', '1', '4', 'pending', null, '1', '2013-12-30 06:14:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('77', '1', 'sumon', '12512412', '1', null, '2', '1', '5', 'pending', null, '1', '2013-12-30 06:15:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('78', '1', 'sumon', '12512412', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 06:15:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('79', '1', 'sumon', '12512412', '1', null, '2', '1', '3', 'pending', null, '1', '2013-12-30 06:15:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('80', '1', 'sumon', '12512412', '1', null, '3', '1', '4', 'pending', null, '1', '2013-12-30 06:35:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('81', '1', 'sumon', '12512412', '1', null, '2', '1', '5', 'pending', null, '1', '2013-12-30 07:00:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('82', '1', 'sumon', '12512412', '1', null, '3', '1', '3', 'pending', null, '1', '2013-12-30 07:02:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('83', '1', 'sumon', '12512412', '1', null, '2', '1', '3', 'pending', null, '1', '2013-12-30 07:02:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('84', '1', 'sumon', '12512412', '1', null, '3', '1', '4', 'pending', null, '1', '2013-12-30 08:17:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('85', '', '', '', '1', null, '2', '1', '3', 'pending', null, '1', '2013-12-30 09:05:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('86', '', '', '', '1', null, '3', '1', '4', 'pending', null, '1', '2013-12-30 09:15:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('87', '', '', '', '1', null, '2', '1', '4', 'pending', null, '1', '2013-12-30 09:21:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('88', '', '', '', '1', null, '3', '1', '3', 'pending', null, '1', '2013-12-30 11:00:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('89', '', '', '', '1', null, '2', '1', '1', 'pending', null, '1', '2013-12-30 11:02:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('90', '', '', '', '1', null, '3', '1', '1', 'pending', null, '1', '2013-12-30 11:08:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('91', '', '', '', '1', null, '2', '1', '5', 'pending', null, '1', '2013-12-30 11:10:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('92', '', '', '', '1', null, '3', '1', '2', 'pending', null, '1', '2013-12-30 11:24:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('93', '', '', '', '1', null, '2', '1', '4', 'pending', null, '1', '2013-12-30 11:28:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('94', '', '', '', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 11:32:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('95', '555454588280955', 'hfttgjdsrthdtbhhgv', '7554854585075874566', '1', null, '2', '1', '4', 'pending', null, '1', '2013-12-30 11:33:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('96', '', '', '', '1', null, '2', '1', '5', 'pending', null, '1', '2013-12-30 11:34:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('97', '155', 'jhgrjf', '56559', '1', null, '3', '1', '4', 'pending', null, '1', '2013-12-30 11:35:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('98', '', '', '', '1', null, '2', '1', '4', 'pending', null, '1', '2013-12-30 11:35:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('99', '', '', '', '1', null, '3', '1', '4', 'pending', null, '1', '2013-12-30 11:35:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('100', '', '', '', '1', null, '2', '1', '4', 'pending', null, '1', '2013-12-30 11:36:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('101', '', '', '', '1', null, '3', '1', '2', 'pending', null, '1', '2013-12-30 11:37:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('102', '', '', '', '1', null, '2', '1', '3', 'pending', null, '1', '2013-12-30 11:39:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('103', '', '', '', '1', null, '3', '1', '3', 'pending', null, '1', '2013-12-30 11:46:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('104', '', '', '', '1', null, '2', '1', '2', 'pending', null, '1', '2013-12-30 11:46:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('105', '', '', '', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 12:30:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('106', '', '', '', '1', null, '2', '1', '5', 'pending', null, '1', '2013-12-30 12:30:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('107', '', '', '', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 12:30:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('108', '', '', '', '1', null, '2', '1', '3', 'pending', null, '1', '2013-12-30 12:31:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('109', '', '', '', '1', null, '3', '1', '3', 'pending', null, '1', '2013-12-30 12:31:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('110', '', '', '', '1', null, '2', '1', '2', 'pending', null, '1', '2013-12-30 12:34:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('111', '', '', '', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 12:37:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('112', '', '', '', '1', null, '2', '1', '2', 'pending', null, '1', '2013-12-30 12:43:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('113', '', '', '', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 12:45:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('114', '', '', '', '1', null, '2', '1', '3', 'pending', null, '1', '2013-12-30 13:02:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('115', '257', 'sumon', '89248', '1', null, '3', '1', '3', 'pending', null, '1', '2013-12-30 13:03:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('116', '548', 'fsgf', '55', '1', null, '2', '1', '5', 'pending', null, '1', '2013-12-30 13:04:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('117', '', '', '', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 13:04:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('118', '', '', '', '1', null, '2', '1', '4', 'pending', null, '1', '2013-12-30 13:10:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('119', '', '', '', '1', null, '2', '1', '2', 'pending', null, '1', '2013-12-30 13:12:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('120', '', '', '', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 16:06:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('121', '', '', '', '1', null, '2', '1', '4', 'pending', null, '1', '2013-12-30 16:11:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('122', '', '', '', '1', null, '3', '1', '4', 'pending', null, '1', '2013-12-30 16:11:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('123', '', '', '', '1', null, '2', '1', '5', 'pending', null, '1', '2013-12-30 16:12:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('124', '', '', '', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-30 16:12:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('125', '', '', '', '1', null, '2', '1', '1', 'pending', null, '1', '2013-12-30 20:41:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('126', '', '', '', '1', null, '2', '1', '5', 'pending', null, '1', '2013-12-31 00:27:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('127', '', '', '', '1', null, '3', '1', '5', 'pending', null, '1', '2013-12-31 00:49:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('128', '', '', '', '1', null, '2', '1', '5', 'pending', null, '1', '2013-12-31 00:49:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('129', '123', 'test', 'test', '11', '32ewwew', '11', '11', '11', 'pending', null, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('130', '123', 'test', 'test', '11', '32ewwew', '11', '11', '11', 'pending', null, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('131', '1234', 'test name', 'test address', '11', 'information', '11', '11', '11', 'pending', null, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('132', '', '', '', '1', null, '2', '1', '2', 'pending', null, '1', '2014-01-02 02:46:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('133', '', '', '', '1', null, '3', '1', '2', 'finished', 's Sas ASa', '0', '2014-01-02 02:46:33', '2014-01-02 05:14:11', '2014-01-02 05:15:19');
INSERT INTO bf_ticket VALUES ('134', '', '', '', '1', null, '2', '1', '2', 'pending', null, '1', '2014-01-02 02:51:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('135', '', '', '', '1', null, '3', '1', '2', 'pending', null, '1', '2014-01-02 02:53:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('136', '', '', '', '1', null, '2', '1', '2', 'pending', null, '1', '2014-01-02 02:53:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('137', '', '', '', '2', null, '3', '1', '2', 'pending', null, '0', '2014-01-02 05:12:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('138', '', '', '', '4', null, '3', '1', '2', 'finished', 'df dsf sdf dsf s', '0', '2014-01-04 22:03:35', '2014-01-04 23:46:48', '2014-01-04 23:47:05');
INSERT INTO bf_ticket VALUES ('139', '', '', '', '4', null, '3', '1', '2', 'pending', null, '0', '2014-01-04 22:03:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('140', '', '', '', '1', null, '3', '1', '2', 'finished', null, '0', '2014-01-05 03:52:57', '2014-01-05 04:12:02', '2014-01-05 04:12:39');
INSERT INTO bf_ticket VALUES ('141', '', '', '', '1', null, '3', '1', '2', 'pending', null, '0', '2014-01-05 03:55:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO bf_ticket VALUES ('142', '', '', '', '1', null, '3', '1', '1', 'serving', null, '0', '2014-01-05 04:13:12', '2014-01-05 04:13:31', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `bf_ticket_meta`
-- ----------------------------
DROP TABLE IF EXISTS `bf_ticket_meta`;
CREATE TABLE `bf_ticket_meta` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `counter_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `status` enum('open','progress','resolved') DEFAULT NULL,
  `seen` tinyint(4) DEFAULT '0',
  `created_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `progressed_on` datetime DEFAULT NULL,
  `finished_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_ticket_meta
-- ----------------------------
INSERT INTO bf_ticket_meta VALUES ('1', '6', '2', '3', '1', 'progress', '0', '2013-12-23 13:04:26', '2013-12-23 13:04:26', null);
INSERT INTO bf_ticket_meta VALUES ('2', '6', '2', '3', '1', 'resolved', '0', '2013-12-23 13:06:47', null, '2013-12-23 13:06:47');
INSERT INTO bf_ticket_meta VALUES ('3', '6', '2', '3', '1', 'progress', '0', '2013-12-23 13:07:12', '2013-12-23 13:07:12', null);
INSERT INTO bf_ticket_meta VALUES ('4', '9', '1', '2', '1', 'progress', '0', '2013-12-23 13:08:41', '2013-12-23 13:08:41', null);
INSERT INTO bf_ticket_meta VALUES ('5', '9', '1', '2', '1', 'resolved', '0', '2013-12-23 13:08:48', null, '2013-12-23 13:08:48');
INSERT INTO bf_ticket_meta VALUES ('6', '9', '3', '2', '1', 'progress', '0', '2013-12-23 13:23:22', '2013-12-23 13:23:22', null);
INSERT INTO bf_ticket_meta VALUES ('7', '9', '3', '2', '1', 'resolved', '0', '2013-12-23 13:23:28', null, '2013-12-23 13:23:28');
INSERT INTO bf_ticket_meta VALUES ('8', '9', '4', '2', '1', 'progress', '0', '2013-12-23 13:23:45', '2013-12-23 13:23:45', null);
INSERT INTO bf_ticket_meta VALUES ('9', '6', '2', '3', '1', 'progress', '0', '2013-12-23 13:26:04', '2013-12-23 13:26:04', null);
INSERT INTO bf_ticket_meta VALUES ('10', '6', '2', '3', '1', 'resolved', '0', '2013-12-23 13:27:33', null, '2013-12-23 13:27:33');
INSERT INTO bf_ticket_meta VALUES ('11', '6', '5', '3', '1', 'progress', '0', '2013-12-23 13:32:42', '2013-12-23 13:32:42', null);
INSERT INTO bf_ticket_meta VALUES ('12', '9', '3', '2', '1', 'progress', '0', '2013-12-23 13:34:47', '2013-12-23 13:34:47', null);
INSERT INTO bf_ticket_meta VALUES ('13', '9', '4', '2', '1', 'progress', '0', '2013-12-23 13:37:42', '2013-12-23 13:37:42', null);
INSERT INTO bf_ticket_meta VALUES ('14', '9', '6', '2', '1', 'progress', '0', '2013-12-23 13:37:54', '2013-12-23 13:37:54', null);
INSERT INTO bf_ticket_meta VALUES ('15', '9', '3', '2', '1', 'progress', '0', '2013-12-23 13:39:00', '2013-12-23 13:39:00', null);
INSERT INTO bf_ticket_meta VALUES ('16', '9', '4', '2', '1', 'progress', '0', '2013-12-23 13:39:18', '2013-12-23 13:39:18', null);
INSERT INTO bf_ticket_meta VALUES ('17', '9', '6', '2', '1', 'progress', '0', '2013-12-23 13:43:13', '2013-12-23 13:43:13', null);
INSERT INTO bf_ticket_meta VALUES ('18', '6', '2', '3', '1', 'progress', '0', '2013-12-23 13:43:29', '2013-12-23 13:43:29', null);
INSERT INTO bf_ticket_meta VALUES ('19', '6', '5', '3', '1', 'progress', '0', '2013-12-23 13:43:31', '2013-12-23 13:43:31', null);
INSERT INTO bf_ticket_meta VALUES ('20', '6', '2', '3', '1', 'progress', '0', '2013-12-23 15:02:50', '2013-12-23 15:02:50', null);
INSERT INTO bf_ticket_meta VALUES ('21', '6', '2', '3', '1', 'resolved', '0', '2013-12-23 15:02:57', null, '2013-12-23 15:02:57');
INSERT INTO bf_ticket_meta VALUES ('22', '6', '5', '3', '1', 'progress', '0', '2013-12-23 15:11:34', '2013-12-23 15:11:34', null);
INSERT INTO bf_ticket_meta VALUES ('23', '6', '5', '3', '1', 'resolved', '0', '2013-12-23 15:16:03', null, '2013-12-23 15:16:03');
INSERT INTO bf_ticket_meta VALUES ('24', '9', '3', '2', '1', 'progress', '0', '2013-12-23 15:19:21', '2013-12-23 15:19:21', null);
INSERT INTO bf_ticket_meta VALUES ('25', '9', '3', '2', '1', 'resolved', '0', '2013-12-23 15:19:48', null, '2013-12-23 15:19:48');
INSERT INTO bf_ticket_meta VALUES ('26', '9', '3', '2', '1', 'progress', '0', '2013-12-23 15:21:41', '2013-12-23 15:21:41', null);
INSERT INTO bf_ticket_meta VALUES ('27', '9', '3', '2', '1', 'resolved', '0', '2013-12-23 15:22:43', null, '2013-12-23 15:22:43');
INSERT INTO bf_ticket_meta VALUES ('28', '6', '2', '3', '1', 'progress', '0', '2013-12-23 15:24:08', '2013-12-23 15:24:08', null);
INSERT INTO bf_ticket_meta VALUES ('29', '6', '2', '3', '1', 'resolved', '0', '2013-12-23 15:24:10', null, '2013-12-23 15:24:10');
INSERT INTO bf_ticket_meta VALUES ('30', '6', '2', '3', '1', 'progress', '0', '2013-12-23 15:25:28', '2013-12-23 15:25:28', null);
INSERT INTO bf_ticket_meta VALUES ('31', '6', '5', '3', '1', 'progress', '0', '2013-12-23 15:28:00', '2013-12-23 15:28:00', null);
INSERT INTO bf_ticket_meta VALUES ('32', '6', '2', '3', '1', 'resolved', '0', '2013-12-23 15:29:43', null, '2013-12-23 15:29:43');
INSERT INTO bf_ticket_meta VALUES ('33', '6', '5', '3', '1', 'resolved', '0', '2013-12-23 15:29:49', null, '2013-12-23 15:29:49');
INSERT INTO bf_ticket_meta VALUES ('34', '6', '2', '3', '1', 'progress', '0', '2013-12-23 15:30:35', '2013-12-23 15:30:35', null);
INSERT INTO bf_ticket_meta VALUES ('35', '6', '5', '3', '1', 'progress', '0', '2013-12-23 15:31:00', '2013-12-23 15:31:00', null);
INSERT INTO bf_ticket_meta VALUES ('36', '6', '2', '3', '1', 'progress', '0', '2013-12-23 15:31:45', '2013-12-23 15:31:45', null);
INSERT INTO bf_ticket_meta VALUES ('37', '9', '7', '2', '1', 'progress', '0', '2013-12-23 15:36:53', '2013-12-23 15:36:53', null);
INSERT INTO bf_ticket_meta VALUES ('38', '9', '7', '2', '1', 'progress', '0', '2013-12-23 15:38:08', '2013-12-23 15:38:08', null);
INSERT INTO bf_ticket_meta VALUES ('39', '9', '7', '2', '1', 'resolved', '0', '2013-12-23 15:38:14', null, '2013-12-23 15:38:14');
INSERT INTO bf_ticket_meta VALUES ('40', '6', '9', '3', '1', 'progress', '0', '2013-12-23 15:39:45', '2013-12-23 15:39:45', null);
INSERT INTO bf_ticket_meta VALUES ('41', '6', '11', '3', '1', 'progress', '0', '2013-12-23 15:40:15', '2013-12-23 15:40:15', null);
INSERT INTO bf_ticket_meta VALUES ('42', '6', '13', '3', '1', 'progress', '0', '2013-12-23 15:42:14', '2013-12-23 15:42:14', null);
INSERT INTO bf_ticket_meta VALUES ('43', '9', '8', '2', '1', 'progress', '0', '2013-12-23 15:45:09', '2013-12-23 15:45:09', null);
INSERT INTO bf_ticket_meta VALUES ('44', '9', '10', '2', '1', 'progress', '0', '2013-12-23 15:57:38', '2013-12-23 15:57:38', null);
INSERT INTO bf_ticket_meta VALUES ('45', '6', '9', '3', '1', 'progress', '0', '2013-12-23 16:00:51', '2013-12-23 16:00:51', null);
INSERT INTO bf_ticket_meta VALUES ('46', '6', '11', '3', '1', 'progress', '0', '2013-12-23 16:01:16', '2013-12-23 16:01:16', null);
INSERT INTO bf_ticket_meta VALUES ('47', '6', '13', '3', '1', 'progress', '0', '2013-12-23 16:01:22', '2013-12-23 16:01:22', null);
INSERT INTO bf_ticket_meta VALUES ('48', '9', '12', '2', '1', 'progress', '0', '2013-12-23 16:01:33', '2013-12-23 16:01:33', null);
INSERT INTO bf_ticket_meta VALUES ('49', '9', '7', '2', '1', 'progress', '0', '2013-12-23 16:01:39', '2013-12-23 16:01:39', null);
INSERT INTO bf_ticket_meta VALUES ('50', '9', '8', '2', '1', 'progress', '0', '2013-12-23 16:01:40', '2013-12-23 16:01:40', null);
INSERT INTO bf_ticket_meta VALUES ('51', '9', '10', '2', '1', 'progress', '0', '2013-12-23 16:01:41', '2013-12-23 16:01:41', null);
INSERT INTO bf_ticket_meta VALUES ('52', '9', '14', '2', '1', 'progress', '0', '2013-12-23 16:01:43', '2013-12-23 16:01:43', null);
INSERT INTO bf_ticket_meta VALUES ('53', '9', '7', '2', '1', 'resolved', '0', '2013-12-23 16:01:55', null, '2013-12-23 16:01:55');
INSERT INTO bf_ticket_meta VALUES ('54', '9', '8', '2', '1', 'resolved', '0', '2013-12-23 16:02:04', null, '2013-12-23 16:02:04');
INSERT INTO bf_ticket_meta VALUES ('55', '9', '10', '2', '1', 'resolved', '0', '2013-12-23 16:02:09', null, '2013-12-23 16:02:09');
INSERT INTO bf_ticket_meta VALUES ('56', '9', '12', '2', '1', 'resolved', '0', '2013-12-23 16:02:13', null, '2013-12-23 16:02:13');
INSERT INTO bf_ticket_meta VALUES ('57', '9', '14', '2', '1', 'resolved', '0', '2013-12-23 16:02:17', null, '2013-12-23 16:02:17');
INSERT INTO bf_ticket_meta VALUES ('58', '6', '15', '3', '1', 'progress', '0', '2013-12-23 16:03:05', '2013-12-23 16:03:05', null);
INSERT INTO bf_ticket_meta VALUES ('59', '6', '9', '3', '1', 'progress', '0', '2013-12-23 16:10:12', '2013-12-23 16:10:12', null);
INSERT INTO bf_ticket_meta VALUES ('60', '6', '11', '3', '1', 'progress', '0', '2013-12-23 16:23:43', '2013-12-23 16:23:43', null);
INSERT INTO bf_ticket_meta VALUES ('61', '6', '9', '3', '1', 'resolved', '0', '2013-12-23 16:29:09', null, '2013-12-23 16:29:09');
INSERT INTO bf_ticket_meta VALUES ('62', '6', '13', '3', '1', 'progress', '0', '2013-12-23 16:29:27', '2013-12-23 16:29:27', null);
INSERT INTO bf_ticket_meta VALUES ('63', '6', '11', '3', '1', 'resolved', '0', '2013-12-23 16:29:32', null, '2013-12-23 16:29:32');
INSERT INTO bf_ticket_meta VALUES ('64', '6', '15', '3', '1', 'progress', '0', '2013-12-23 16:32:23', '2013-12-23 16:32:23', null);
INSERT INTO bf_ticket_meta VALUES ('65', '9', '7', '2', '1', 'progress', '0', '2013-12-23 16:33:42', '2013-12-23 16:33:42', null);
INSERT INTO bf_ticket_meta VALUES ('66', '9', '8', '2', '1', 'progress', '0', '2013-12-23 16:33:56', '2013-12-23 16:33:56', null);
INSERT INTO bf_ticket_meta VALUES ('67', '9', '10', '2', '1', 'progress', '0', '2013-12-23 16:33:58', '2013-12-23 16:33:58', null);
INSERT INTO bf_ticket_meta VALUES ('68', '9', '7', '2', '1', 'resolved', '0', '2013-12-23 16:34:00', null, '2013-12-23 16:34:00');
INSERT INTO bf_ticket_meta VALUES ('69', '6', '9', '3', '1', 'progress', '0', '2013-12-23 16:47:32', '2013-12-23 16:47:32', null);
INSERT INTO bf_ticket_meta VALUES ('70', '6', '11', '3', '1', 'progress', '0', '2013-12-23 16:48:20', '2013-12-23 16:48:20', null);
INSERT INTO bf_ticket_meta VALUES ('71', '6', '13', '3', '1', 'progress', '0', '2013-12-23 16:48:25', '2013-12-23 16:48:25', null);
INSERT INTO bf_ticket_meta VALUES ('72', '6', '15', '3', '1', 'progress', '0', '2013-12-23 16:48:29', '2013-12-23 16:48:29', null);
INSERT INTO bf_ticket_meta VALUES ('73', '6', '9', '3', '1', 'resolved', '0', '2013-12-23 16:48:37', null, '2013-12-23 16:48:37');
INSERT INTO bf_ticket_meta VALUES ('74', '6', '11', '3', '1', 'resolved', '0', '2013-12-23 16:48:51', null, '2013-12-23 16:48:51');
INSERT INTO bf_ticket_meta VALUES ('75', '9', '10', '2', '1', 'progress', '0', '2013-12-23 16:50:10', '2013-12-23 16:50:10', null);
INSERT INTO bf_ticket_meta VALUES ('76', '9', '7', '2', '1', 'progress', '0', '2013-12-23 16:51:00', '2013-12-23 16:51:00', null);
INSERT INTO bf_ticket_meta VALUES ('77', '6', '9', '3', '1', 'progress', '0', '2013-12-23 16:53:51', '2013-12-23 16:53:51', null);
INSERT INTO bf_ticket_meta VALUES ('78', '6', '9', '3', '1', 'resolved', '0', '2013-12-23 16:58:23', null, '2013-12-23 16:58:23');
INSERT INTO bf_ticket_meta VALUES ('79', '6', '11', '3', '1', 'progress', '0', '2013-12-23 16:58:50', '2013-12-23 16:58:50', null);
INSERT INTO bf_ticket_meta VALUES ('80', '6', '11', '3', '1', 'resolved', '0', '2013-12-23 16:58:52', null, '2013-12-23 16:58:52');
INSERT INTO bf_ticket_meta VALUES ('81', '9', '7', '2', '1', 'progress', '0', '2013-12-23 16:59:57', '2013-12-23 16:59:57', null);
INSERT INTO bf_ticket_meta VALUES ('82', '9', '7', '2', '1', 'resolved', '0', '2013-12-23 17:00:25', null, '2013-12-23 17:00:25');
INSERT INTO bf_ticket_meta VALUES ('83', '9', '8', '2', '1', 'progress', '0', '2013-12-23 17:27:30', '2013-12-23 17:27:30', null);
INSERT INTO bf_ticket_meta VALUES ('84', '9', '8', '2', '1', 'resolved', '0', '2013-12-23 17:27:36', null, '2013-12-23 17:27:36');
INSERT INTO bf_ticket_meta VALUES ('85', '9', '10', '2', '1', 'progress', '0', '2013-12-23 17:27:49', '2013-12-23 17:27:49', null);
INSERT INTO bf_ticket_meta VALUES ('86', '6', '9', '3', '1', 'progress', '0', '2013-12-23 17:29:24', '2013-12-23 17:29:24', null);
INSERT INTO bf_ticket_meta VALUES ('87', '6', '9', '3', '1', 'resolved', '0', '2013-12-23 17:31:23', null, '2013-12-23 17:31:23');
INSERT INTO bf_ticket_meta VALUES ('88', '6', '11', '3', '1', 'progress', '0', '2013-12-23 17:31:53', '2013-12-23 17:31:53', null);
INSERT INTO bf_ticket_meta VALUES ('89', '6', '11', '3', '1', 'resolved', '0', '2013-12-23 17:35:04', null, '2013-12-23 17:35:04');
INSERT INTO bf_ticket_meta VALUES ('90', '6', '13', '3', '1', 'progress', '0', '2013-12-23 17:35:31', '2013-12-23 17:35:31', null);
INSERT INTO bf_ticket_meta VALUES ('91', '6', '18', '3', '1', 'progress', '0', '2013-12-24 11:34:02', '2013-12-24 11:34:02', null);
INSERT INTO bf_ticket_meta VALUES ('92', '6', '20', '3', '1', 'progress', '0', '2013-12-24 11:34:06', '2013-12-24 11:34:06', null);
INSERT INTO bf_ticket_meta VALUES ('93', '6', '22', '3', '1', 'progress', '0', '2013-12-24 11:34:08', '2013-12-24 11:34:08', null);
INSERT INTO bf_ticket_meta VALUES ('94', '6', '24', '3', '1', 'progress', '0', '2013-12-24 11:34:10', '2013-12-24 11:34:10', null);
INSERT INTO bf_ticket_meta VALUES ('95', '6', '18', '3', '1', 'resolved', '0', '2013-12-24 11:34:11', null, '2013-12-24 11:34:11');
INSERT INTO bf_ticket_meta VALUES ('96', '6', '20', '3', '1', 'resolved', '0', '2013-12-24 11:34:18', null, '2013-12-24 11:34:18');
INSERT INTO bf_ticket_meta VALUES ('97', '6', '22', '3', '1', 'resolved', '0', '2013-12-24 11:34:24', null, '2013-12-24 11:34:24');
INSERT INTO bf_ticket_meta VALUES ('98', '6', '24', '3', '1', 'resolved', '0', '2013-12-24 11:34:29', null, '2013-12-24 11:34:29');
INSERT INTO bf_ticket_meta VALUES ('99', '9', '17', '2', '1', 'progress', '0', '2013-12-24 11:38:07', '2013-12-24 11:38:07', null);
INSERT INTO bf_ticket_meta VALUES ('100', '9', '17', '2', '1', 'resolved', '0', '2013-12-24 11:39:29', null, '2013-12-24 11:39:29');
INSERT INTO bf_ticket_meta VALUES ('101', '9', '19', '2', '1', 'progress', '0', '2013-12-24 11:39:49', '2013-12-24 11:39:49', null);
INSERT INTO bf_ticket_meta VALUES ('102', '9', '19', '2', '1', 'resolved', '0', '2013-12-24 11:40:27', null, '2013-12-24 11:40:27');
INSERT INTO bf_ticket_meta VALUES ('103', '9', '21', '2', '1', 'progress', '0', '2013-12-24 17:09:18', '2013-12-24 17:09:18', null);
INSERT INTO bf_ticket_meta VALUES ('104', '9', '21', '2', '1', 'resolved', '0', '2013-12-24 17:36:47', null, '2013-12-24 17:36:47');
INSERT INTO bf_ticket_meta VALUES ('105', '6', '13', '3', '1', 'resolved', '0', '2013-12-30 11:02:37', null, '2013-12-30 11:02:37');
INSERT INTO bf_ticket_meta VALUES ('106', '6', '15', '3', '1', 'progress', '0', '2013-12-30 11:02:47', '2013-12-30 11:02:47', null);
INSERT INTO bf_ticket_meta VALUES ('107', '6', '16', '3', '1', 'progress', '0', '2013-12-30 06:21:12', '2013-12-30 06:21:12', null);
INSERT INTO bf_ticket_meta VALUES ('108', '6', '15', '3', '1', 'resolved', '0', '2013-12-30 06:21:21', null, '2013-12-30 06:21:21');
INSERT INTO bf_ticket_meta VALUES ('109', '6', '38', '3', '1', 'progress', '0', '2014-01-02 02:48:10', '2014-01-02 02:48:10', null);
INSERT INTO bf_ticket_meta VALUES ('110', '6', '38', '3', '1', 'resolved', '0', '2014-01-02 04:03:51', null, '2014-01-02 04:03:51');
INSERT INTO bf_ticket_meta VALUES ('111', '6', '14', '3', '1', 'progress', '0', '2014-01-02 04:04:28', '2014-01-02 04:04:28', null);
INSERT INTO bf_ticket_meta VALUES ('112', '6', '133', '3', '1', 'progress', '0', '2014-01-02 05:14:11', '2014-01-02 05:14:11', null);
INSERT INTO bf_ticket_meta VALUES ('113', '6', '133', '3', '1', 'resolved', '0', '2014-01-02 05:15:19', null, '2014-01-02 05:15:19');
INSERT INTO bf_ticket_meta VALUES ('114', '6', '138', '3', '1', 'progress', '0', '2014-01-04 23:46:48', '2014-01-04 23:46:48', null);
INSERT INTO bf_ticket_meta VALUES ('115', '6', '138', '3', '1', 'resolved', '0', '2014-01-04 23:47:05', null, '2014-01-04 23:47:05');
INSERT INTO bf_ticket_meta VALUES ('116', '6', '140', '3', '1', 'progress', '0', '2014-01-05 04:12:02', '2014-01-05 04:12:02', null);
INSERT INTO bf_ticket_meta VALUES ('117', '6', '140', '3', '1', 'resolved', '0', '2014-01-05 04:12:39', null, '2014-01-05 04:12:39');
INSERT INTO bf_ticket_meta VALUES ('118', '6', '142', '3', '1', 'progress', '0', '2014-01-05 04:13:31', '2014-01-05 04:13:31', null);

-- ----------------------------
-- Table structure for `bf_token`
-- ----------------------------
DROP TABLE IF EXISTS `bf_token`;
CREATE TABLE `bf_token` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(32) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `expire_on` datetime DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_token
-- ----------------------------
INSERT INTO bf_token VALUES ('1', '70a8a24e80d046726a0e354f04cffc8b', '1', '2013-12-17 12:47:29', null, '2013-12-17 14:47:29', '1');
INSERT INTO bf_token VALUES ('2', '6fdc2a385f5ab12b2e433a863851d1a8', '1', '2013-12-17 12:48:12', null, '2013-12-17 14:48:12', '1');
INSERT INTO bf_token VALUES ('3', '721bb142a21808948be8f25c10a175cc', '1', '2013-12-17 12:52:05', null, '2013-12-17 14:52:05', '1');
INSERT INTO bf_token VALUES ('4', '6c81890d9ec47da81fadb2c9d0fe6292', '1', '2013-12-17 12:54:22', null, '2013-12-17 14:54:22', '1');
INSERT INTO bf_token VALUES ('5', '5df4a5cfc55689084fc9edf6ca288a6f', '1', '2013-12-17 12:56:38', null, '2013-12-17 14:56:38', '1');
INSERT INTO bf_token VALUES ('6', '00a6a1876ac4ca5554c656cb69e2f566', '1', '2013-12-17 13:01:34', null, '2013-12-17 15:01:34', '1');
INSERT INTO bf_token VALUES ('7', '399e0f09953753252a1b4c1adf94d4dc', '1', '2013-12-17 13:01:53', null, '2013-12-17 15:01:53', '1');
INSERT INTO bf_token VALUES ('8', 'e032c6c2bb2d8696fb6f4fa79dc419cd', '1', '2013-12-17 13:02:58', null, '2013-12-17 15:02:58', '1');
INSERT INTO bf_token VALUES ('9', '784e7351c81a166b3020ccaf003a4e20', '1', '2013-12-17 13:23:45', null, '2013-12-17 15:23:45', '1');
INSERT INTO bf_token VALUES ('10', 'eb287d28fcb459a61501eea21d86d989', '1', '2013-12-17 14:24:34', null, '2013-12-17 16:24:34', '1');
INSERT INTO bf_token VALUES ('11', 'd4d479745cd692e73c160790f08a522e', '1', '2013-12-17 15:13:15', '2013-12-18 12:31:28', '2013-12-17 17:13:15', '0');
INSERT INTO bf_token VALUES ('12', '452b863fd7688ae02684ef3b1c9e6d3c', '1', '2013-12-18 10:23:59', null, '2013-12-18 12:23:59', '1');
INSERT INTO bf_token VALUES ('13', '503cabc21755ef2d73caa71eb8c5da54', '1', '2013-12-18 12:31:39', '2013-12-18 14:31:39', '2013-12-18 14:31:39', '0');
INSERT INTO bf_token VALUES ('14', '1276b27e909e117a6a841991ecf1e24f', '1', '2013-12-18 14:32:25', null, '2013-12-18 16:32:25', '1');
INSERT INTO bf_token VALUES ('15', '732ed0e4e3bb2c46a962155d4762bbb9', '1', '2013-12-18 15:26:58', '2013-12-18 17:27:26', '2013-12-18 17:26:58', '0');
INSERT INTO bf_token VALUES ('16', 'fa6645023a9498f792158c6db78dff72', '1', '2013-12-18 17:28:07', null, '2013-12-18 19:28:07', '1');
INSERT INTO bf_token VALUES ('17', 'ac3af6da78dfe28c4190138466763ef8', '1', '2013-12-18 20:36:54', null, '2013-12-18 22:36:54', '1');
INSERT INTO bf_token VALUES ('18', 'be78a4065c10f58bc30cd9e85b9d9d30', '1', '2013-12-19 10:26:33', null, '2013-12-19 12:26:33', '1');
INSERT INTO bf_token VALUES ('19', 'e2d19f9c95cc60fde1e72c6c792f6b65', '1', '2013-12-19 11:35:08', null, '2013-12-19 13:35:08', '1');
INSERT INTO bf_token VALUES ('20', '01c4f1bf58e405168fa6764e04c7820f', '1', '2013-12-19 11:36:06', null, '2013-12-19 13:36:06', '1');
INSERT INTO bf_token VALUES ('21', '7a4b2556ff9a468dcf134472e723a318', '1', '2013-12-19 11:39:28', '2013-12-19 17:17:53', '2013-12-19 13:39:28', '0');

-- ----------------------------
-- Table structure for `bf_users`
-- ----------------------------
DROP TABLE IF EXISTS `bf_users`;
CREATE TABLE `bf_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `counter_id` int(11) DEFAULT NULL,
  `email` varchar(120) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password_hash` char(60) NOT NULL,
  `reset_hash` varchar(40) DEFAULT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_ip` varchar(40) NOT NULL DEFAULT '',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `reset_by` int(10) DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_message` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT '',
  `display_name_changed` date DEFAULT NULL,
  `timezone` char(4) NOT NULL DEFAULT 'UM6',
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activate_hash` varchar(40) NOT NULL DEFAULT '',
  `password_iterations` int(4) NOT NULL,
  `force_password_reset` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_users
-- ----------------------------
INSERT INTO bf_users VALUES ('1', '1', '1', '1', '0', 'admin@progmaatic.com', 'admin', '$2a$08$Md5uga8isnCDPs6sTsmj/.pJOoZKhg88.djAMsBOfayFFslCkTo0S', null, '2014-01-01 21:54:54', '119.148.16.250', '2013-12-09 07:33:07', '0', null, '0', null, 'admin', null, 'UP6', 'english', '1', '', '2', '0');
INSERT INTO bf_users VALUES ('2', '9', '1', '1', '0', 'symphony@gmail.com', 'symphony', '$2a$08$uj1I7wJCWJ7pKlNy/.TTde8qNbJ1fCJWnyiO6AoZbG8x9hd6EvD5W', null, '2014-01-01 23:08:15', '119.148.16.250', '2013-12-19 07:16:46', '0', null, '0', null, 'symphony', null, 'UP6', 'english', '1', '', '2', '0');
INSERT INTO bf_users VALUES ('6', '7', '1', '1', '3', 'executive@gmail.com', 'executive', '$2a$08$QqAOmQzqn7K9i4P1tkjqH.EfyqBof.roWGOsaaVSyLjFzZk4Dw7uS', null, '2013-12-30 06:20:22', '119.148.16.250', '2013-12-22 15:29:15', '0', null, '0', null, 'Executive', null, 'UP6', 'english', '1', '', '2', '0');
INSERT INTO bf_users VALUES ('7', '8', '1', '1', '0', 'manager@gmail.com', 'manager', '$2a$08$4M.KJe266EQSUfRqXDYzZ.t1B/GaFF..ihF5phHGYzgTPQMa0HI1G', null, '2014-01-02 02:09:07', '119.148.16.250', '2013-12-22 15:31:07', '0', null, '0', null, 'Manager', null, 'UP6', 'english', '1', '', '2', '0');
INSERT INTO bf_users VALUES ('8', '7', '1', '1', '4', 'executive2@gmail.com', 'executive2', '$2a$08$dlm/z8dWKoJpkJ45UBNI0e2JpJ8wLuGYCLkRmNkW/3cQa8Fr2.sfW', null, '2013-12-24 17:10:02', '127.0.0.1', '2013-12-23 10:58:07', '0', null, '0', null, 'executive2', null, 'UP6', 'english', '1', '', '2', '0');
INSERT INTO bf_users VALUES ('9', '7', '1', '1', '2', 'executive3@gmail.com', 'executive3', '$2a$08$S.5GXPcSNgJBCosoGQYRPOi/qjkrwJFBz4u0HGh5ciYyadlu1X.vS', null, '2013-12-24 17:07:20', '127.0.0.1', '2013-12-23 11:01:47', '0', null, '0', null, 'executive3', null, 'UP6', 'english', '1', '', '2', '0');
INSERT INTO bf_users VALUES ('12', '8', '1', '1', '2', 'pd@gmail.com', 'manager3', '$2a$08$vCCOC9//pX/1FQ3SgkH5TOtkTT1hk.FGkAluCggMPCOnetV6X35cS', null, '0000-00-00 00:00:00', '', '2014-01-02 16:10:29', '0', null, '0', null, 'manager new', null, 'UP6', 'english', '1', '', '2', '0');
INSERT INTO bf_users VALUES ('13', '7', '1', '1', '3', 'poo@foo.com', 'executive4', '$2a$08$uSG.Fjm/wkfNpSgguLpIZuicClbrGus0O1nzwj2i8Zk4xY47Vxt/C', null, '0000-00-00 00:00:00', '', '2014-01-02 16:11:54', '0', null, '0', null, 'Exe', null, 'UP6', 'english', '1', '', '2', '0');

-- ----------------------------
-- Table structure for `bf_user_cookies`
-- ----------------------------
DROP TABLE IF EXISTS `bf_user_cookies`;
CREATE TABLE `bf_user_cookies` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(128) NOT NULL,
  `created_on` datetime NOT NULL,
  KEY `token` (`token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_user_cookies
-- ----------------------------
INSERT INTO bf_user_cookies VALUES ('6', 'xy25m1374XZaQP5NGI4xcKi4T5etUb7DoWxHpISQjsBmomttvWGnR6kYPV20ZcSjj8VylIKyWyFszWMgTttOGEPemMcPL164Cu6Ghly3BUetWFARhcYoYTqQV6RsexhV', '2014-01-02 04:03:36');

-- ----------------------------
-- Table structure for `bf_user_meta`
-- ----------------------------
DROP TABLE IF EXISTS `bf_user_meta`;
CREATE TABLE `bf_user_meta` (
  `meta_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) NOT NULL DEFAULT '',
  `meta_value` text,
  PRIMARY KEY (`meta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bf_user_meta
-- ----------------------------
INSERT INTO bf_user_meta VALUES ('1', '1', 'street_name', 'Dhaka');
INSERT INTO bf_user_meta VALUES ('2', '1', 'state', 'SC');
INSERT INTO bf_user_meta VALUES ('3', '1', 'country', 'BD');
INSERT INTO bf_user_meta VALUES ('4', '1', 'type', 'small');
INSERT INTO bf_user_meta VALUES ('5', '2', 'street_name', 'Dhaka');
INSERT INTO bf_user_meta VALUES ('6', '2', 'state', '');
INSERT INTO bf_user_meta VALUES ('7', '2', 'country', 'BD');
INSERT INTO bf_user_meta VALUES ('11', '4', 'street_name', 'Dhaka');
INSERT INTO bf_user_meta VALUES ('12', '4', 'state', '');
INSERT INTO bf_user_meta VALUES ('13', '4', 'country', 'BD');
INSERT INTO bf_user_meta VALUES ('14', '2', 'type', 'small');
INSERT INTO bf_user_meta VALUES ('15', '5', 'street_name', '');
INSERT INTO bf_user_meta VALUES ('16', '5', 'state', 'SC');
INSERT INTO bf_user_meta VALUES ('17', '5', 'country', 'US');
INSERT INTO bf_user_meta VALUES ('18', '6', 'street_name', 'Dhaka');
INSERT INTO bf_user_meta VALUES ('19', '6', 'state', '');
INSERT INTO bf_user_meta VALUES ('20', '6', 'country', 'BD');
INSERT INTO bf_user_meta VALUES ('21', '7', 'street_name', 'Dhaka');
INSERT INTO bf_user_meta VALUES ('22', '7', 'state', '');
INSERT INTO bf_user_meta VALUES ('23', '7', 'country', 'BD');
INSERT INTO bf_user_meta VALUES ('24', '8', 'street_name', 'Dhaka');
INSERT INTO bf_user_meta VALUES ('25', '8', 'state', '');
INSERT INTO bf_user_meta VALUES ('26', '8', 'country', 'BD');
INSERT INTO bf_user_meta VALUES ('27', '9', 'street_name', 'Dhaka');
INSERT INTO bf_user_meta VALUES ('28', '9', 'state', '');
INSERT INTO bf_user_meta VALUES ('29', '9', 'country', 'BD');
INSERT INTO bf_user_meta VALUES ('36', '12', 'street_name', 'gduywgdwu');
INSERT INTO bf_user_meta VALUES ('37', '12', 'state', 'SC');
INSERT INTO bf_user_meta VALUES ('38', '12', 'country', 'US');
INSERT INTO bf_user_meta VALUES ('39', '13', 'street_name', 'ggyughjg');
INSERT INTO bf_user_meta VALUES ('40', '13', 'state', 'SC');
INSERT INTO bf_user_meta VALUES ('41', '13', 'country', 'US');
