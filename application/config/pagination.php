<?php

$config['paginationContent'] = array(
    'per_page' => 10,
    'uri_segment' => 5,
    'full_tag_open' => '<div id="pagination" class="pagination pagination-centered pagination-mini"><ul>',
    'full_tag_close' => '</ul></div>',
    'cur_tag_open' => '<li class="active"><span>',
    'cur_tag_close' => '</span></li>',
    'num_tag_open' => '<li class="disabled"><span>',
    'num_tag_close' => '</span></li>',
    'next_link' => 'Next',
    'next_tag_open' => '<li class="disabled"><span>',
    'next_tag_close' => '</span></li>',
    'prev_link' => 'Prev',
    'prev_tag_open' => '<li class="disabled"><span>',
    'prev_tag_close' => '</span></li>',
    'last_link' => 'Last',
    'last_tag_open' => '<li class="disabled"><span>',
    'last_tag_close' => '</span></li>',
    'first_link' => 'First',
    'first_tag_open' => '<li class="disabled"><span>',
    'first_tag_close' => '</span></li>'
);
?>
