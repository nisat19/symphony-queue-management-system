<!--	<footer class="container-fluid footer">
                <p class="pull-right">
                        Executed in {elapsed_time} seconds, using {memory_usage}.
                        <br/>
                        Powered by <a href="http://cibonfire.com" target="_blank"><i class="icon-fire">&nbsp;</i>&nbsp;Bonfire</a> <?php echo BONFIRE_VERSION ?>
                </p>
        </footer>-->
</div>
</div>
<div id="debug"><!-- Stores the Profiler Results --></div>
<div id="developed_by" style="position: relative;bottom: -10px;float:right; padding:10px 10px 10px 10px; text-align: right; background: #F1F1F1;">Developed by: <a href="http://www.progmaatic.com">PROGmaatic</a></div>

<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo js_path(); ?>jquery-1.7.2.min.js"><\/script>')</script>
<?php echo Assets::js(); ?>
<script type="text/javascript">
    $(function() {
        var width = $('body').innerWidth();
        var height = $('body').innerHeight();
        $('#developed_by').css('width', width - 20);
        $('#developed_by').css('top', height - 10);
    });
</script>
</body>
</html>
